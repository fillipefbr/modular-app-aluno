angular.module('app.codesCtrl', [])

//****************************_CODES CONTROLLER_******************************//

.controller('codesCtrl', function($scope, $rootScope, $document, $timeout, categoryService, $ionicLoading, $state, $cordovaNetwork, ionicToast, ConnectivityMonitor, $ionicPopup, agendamentoService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const dbRefCadastroPermanente = dbRefRoot.child('cadastro_permanente');
  const dbRefCodesPromocionais = dbRefRoot.child('codes_promocionais');
  const dbRefContatoModular = dbRefRoot.child('contato_modular');
  const auth = firebase.auth();

  $scope.$on("$ionicView.beforeEnter", function(){

    //$scope.codes = [];
    $scope.nenhumCP = false;
    $scope.data.spincodes = true;
    var cds = [];
    var user = firebase.auth().currentUser;

    if (user) {
      user = user.uid;
      var dbRefA = dbRefAlunosPrivate.child(user);
      var dbRefC = dbRefA.child("Códigos_Promocionais");

      dbRefC.once('value', function(snap) {
        if(snap.val()){
          snap.forEach(function(child) {
            var cd = {"code" : child.val().Código, "desconto" : child.val().Desconto, "key" : child.key};
            cds.push(cd);
          });
          $scope.codes = cds;
          $scope.data.spincodes = false;
        }
        else{
          $scope.data.spincodes = false;
          $scope.nenhumCP = true;
        }
      });
    }

  });

//----------------------------------------------------------------------------//
$scope.addCodePromo = function(code) {

  var user = firebase.auth().currentUser;
  var hasCode = false;
  var notFound = false;
  $ionicLoading.show({
    template: "<ion-spinner icon='ios-small'></ion-spinner>"
  });

  if (user) {
    user = user.uid;
    dbRefAlunosPrivate.child(user).child("Códigos_Promocionais").on('child_added', function(snap) {
      if(code === snap.val().Código){
        hasCode = true;
        $ionicLoading.hide();
        ionicToast.show('Você já possui esse código promocional', 'middle', false, 2500);
      }
    });
    dbRefAlunosPrivate.child(user).child("Códigos_Utilizados").on('child_added', function(snap) {
      if(code === snap.val().Código && !hasCode){
        hasCode = true;
        $ionicLoading.hide();
        ionicToast.show('Você já utilizou esse código promocional', 'middle', false, 2500);
      }
    });
    dbRefCodesPromocionais.on('child_added', function(snop) {
      if(code === snop.val().Código){
        notFound = false;
      }
      else{
        notFound = true;
      }
    });

    $timeout(function() {
      if(!hasCode && !notFound){
        dbRefCodesPromocionais.on('child_added', function(snapshot) {
          if(code === snapshot.val().Código){
            if(snapshot.val().Restantes){
              dbRefAlunosPrivate.child(user).child("Códigos_Promocionais").child(snapshot.key).set({"Código" : code, "Desconto" : snapshot.val().Desconto});
              dbRefAlunosPrivate.child(user).child("Códigos_Utilizados").child(snapshot.key).set({"Código" : code, "Desconto" : snapshot.val().Desconto});
              dbRefCodesPromocionais.child(snapshot.key).update({"Restantes" : snapshot.val().Restantes - 1});
              $ionicLoading.hide();
              $scope.nenhumCP = false;
              ionicToast.show('Código adicionado', 'middle', false, 2500);
            }
            else{
              dbRefAlunosPrivate.child(user).child("Códigos_Promocionais").child(snapshot.key).set({"Código" : code, "Desconto" : snapshot.val().Desconto});
              dbRefAlunosPrivate.child(user).child("Códigos_Utilizados").child(snapshot.key).set({"Código" : code, "Desconto" : snapshot.val().Desconto});
              $ionicLoading.hide();
              $scope.nenhumCP = false;
              ionicToast.show('Código adicionado', 'middle', false, 2500);
            }
          }
        });
      }
      else if(notFound && !hasCode){
        $ionicLoading.hide();
        ionicToast.show('Código inválido', 'middle', false, 2500);
      }
      else{
        $ionicLoading.hide();
      }
    }, 2000);
  }

};

//----------------------------------------------------------------------------//
$scope.escolheuMeuCode = function(cod, des, kk) {
  var alertPopup = $ionicPopup.alert({
   title: 'Código',
   subTitle: "<p class='custom'>" + cod + "</p>",
   template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-pricetag-outline icon-pop'></i></div></div></div><p class='custom'>Com esse código promocional você ganha <strong>" + des + "%</strong> de desconto na aula.</p></div></div>",
   cssClass: 'popcustom',
   okType: 'customok'
 });
};
//----------------------------------------------------------------------------//

})
