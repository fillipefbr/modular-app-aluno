angular.module('app.tourCtrl', [])

//****************************_TOUR CONTROLLER********************************//

.controller('tourCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, ionicToast) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const dbRefTours = dbRefRoot.child('tours');
  const dbRefAlunosTour = dbRefRoot.child('alunos_tour');
  const auth = firebase.auth();
  $scope.spintour = true;

  $scope.$on("$ionicView.enter", function(){
     // handle event
     $scope.tours = [];
     $scope.spintour = true;
     $scope.nenhumTour = false;
     /*$ionicLoading.show({
       template: '<ion-spinner></ion-spinner> <br/> Carregando'
     });*/

     dbRefTours.on('child_added', function(snap) {
       $timeout(function() {
         var dia = snap.val().Dia;
         dia = parseInt(dia, 10);
         if(dia < 10){
           dia = "0" + dia;
         }
         var mes = snap.val().Mês;
         mes = parseInt(mes, 10);
         if(mes < 10){
           mes = "0" + mes;
         }
         var tour = {"data" : dia + '/' + mes + '/' + snap.val().Ano, "hora" : snap.val().Hora, "dia" : snap.val().Dia, "mes" : snap.val().Mês, "ano" : snap.val().Ano, "key" : snap.key};
         $scope.tours.push(tour);
       });
     });
     $scope.spintour = false;
     $timeout(function() {
       if($scope.tours.length === 0){
         $scope.nenhumTour = true;
       }
     }, 2000);
     //$ionicLoading.hide();

  });

//----------------------------------------------------------------------------//
  $scope.escolheuTour = function(dia, mes, ano, hora, data, key) {

    var currentUser = firebase.auth().currentUser;
    var confirmado = false;

    if (currentUser) {
      // User is signed in.
      var user = currentUser.uid;

      dbRefAulasAlunos.child(user).on('child_added', function(snap) {
        if(snap.val().Disciplina === "Tour pela UnB" && snap.val().Dia === dia && snap.val().Mês === mes && snap.val().Ano === ano){
          confirmado = true;
        }
      });

      if(confirmado){

        ionicToast.show('Você já está confirmado para este tour', 'middle', false, 2500);

      }

      else{

        function onConfirm(buttonIndex) {
          if(buttonIndex === 2){

            $ionicLoading.show({
              template: '<ion-spinner></ion-spinner> <br/>Confirmando'
            });

            dbRefAulasAlunos.child(user).push({"Dia" : dia, "Mês" : mes, "Ano" : ano, "Hora" : hora, "Disciplina" : "Tour pela UnB", "Professor" : "Modular"});
            dbRefAlunosTour.child(key).child(user).set({"Nome" : currentUser.displayName});

            $ionicLoading.hide();

            function alertDismissed() {
                // do something
                $state.go('tabsController.aulas');
            }

            navigator.notification.alert(
                'Você está incluído na lista.',  // message
                alertDismissed,         // callback
                'Confirmado!',            // title
                'Ok'                  // buttonName
            );

          }
          else{

          }
        }

        navigator.notification.confirm(
            'Confirmar participação no Tour pela UnB do dia ' + data + ' às ' + hora + " horas ?", // message
             onConfirm,            // callback to invoke with index of button pressed
            'Confirmar',           // title
            ['Cancelar','Sim']     // buttonLabels
        );


      }


    }

  };

})
