angular.module('app.aulasCtrl', [])

//****************************_AJUSTES CONTROLLER******************************//

.controller('aulasCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, categoryService, ionicToast, $ionicPopup) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessores = dbRefRoot.child('professores');
  const dbRefContatoModular = dbRefRoot.child('contato_modular');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');

  $ionicModal.fromTemplateUrl('aulas/modalcontato.html', {
    scope: $scope
  }).then(function(modalcontato) {
    $scope.modalcontato = modalcontato;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.modalcontato != 'undefined'){
      $scope.modalcontato.remove();
    }
  });

//----------------------------------------------------------------------------//
  $scope.$on("$ionicView.beforeEnter", function(){
    
    var user = firebase.auth().currentUser;
    if (user) {
      $scope.alunoaulasid = user.displayName;
      var contador = 0;
      var dbRefA = dbRefAulasAlunos.child(user.uid);
      var span = $document[0].getElementById('spanaulas');
      dbRefA.once('value', function(snap) {
        if(snap.val()){
          snap.forEach(function(child) {
            contador++;
          });
          if(span != null){
            span.innerText = contador.toString();
          }
        }
        else{
          if(span != null){
            span.innerText = contador.toString();
          }
        }
      });
    }
    
  });

//----------------------------------------------------------------------------//
  $scope.checkSpan = function() {
    var user = firebase.auth().currentUser;
    if (user) {
      user = user.uid;
      var contador = 0;
      var dbRefA = dbRefAulasAlunos.child(user);
      var span = $document[0].getElementById('spanaulas');
      dbRefA.once('value', function(snap) {
        if(snap.val()){
          snap.forEach(function(child) {
            contador++;
          });
          if(span != null){
            span.innerText = contador.toString();
          }
        }
        else{
          if(span != null){
            span.innerText = contador.toString();
          }
        }
      });
    }
  };




})
