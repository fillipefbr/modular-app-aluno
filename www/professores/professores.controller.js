angular.module('app.teachersCtrl', [])

//**************************_TEACHER'S CONTROLLER_****************************//

.controller('teachersCtrl', function($scope, $rootScope, $document, $ionicPopup, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, ConnectivityMonitor, ionicToast, agendamentoService, $ImageCacheFactory, dataService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

  var professores = [];
  var disciplinas = [];
  var horarios_professores = [];

//----------------------------------------------------------------------------//
  $ionicModal.fromTemplateUrl('professores/profDetalhes.html', {
    scope: $scope
  }).then(function(model) {
    $scope.model = model;
  });
//----------------------------------------------------------------------------//
  $scope.$on('$destroy', function() {
    if(typeof $scope.model != 'undefined'){
      $scope.model.remove();
    }
  });
//----------------------------------------------------------------------------//
  $scope.$on("$ionicView.beforeEnter", function(){
    professores = dataService.getProfessores();
    disciplinas = dataService.getDisciplinas();
    horarios_professores = dataService.getHorariosProfessores();
    if(typeof $scope.lehrer === 'undefined'){
      $scope.lehrer = [];
      $scope.data.spinprofs = true;
      $scope.data.hideprofsmsg = true;
      $scope.qtdlehrer = 0;
    }
  });
//----------------------------------------------------------------------------//
  $scope.$on("$ionicView.enter", function(){

    var e = agendamentoService.getDis();
    var p = categoryService.getCat();
    var l = agendamentoService.getLoc();
    var ntemprof = true;
    var dis;
    var loc;
    var cat;
    var disci = [];
    var locai = [];
    var categ = [];
    var localbool = false;
    var disciplinabool = false;

    for(var i = 0; i < professores.length; i++){
       disci = [];
       locai = [];
       categ = [];
       localbool = false;
       disciplinabool = false;
      for(var j = 0; j < horarios_professores.length; j++){
        if(professores[i].$id === horarios_professores[j].$id && professores[i].Autorizado === true){
          dis = professores[i].Disciplinas;
          loc = professores[i].Locais;

          for(var item in dis){
            disci.push(dis[item]);
          }
          for(var item in loc){
            locai.push(loc[item]);
          }

          disci.forEach(function(child) {
            if(e === child.Nome && p === child.Categoria){
              disciplinabool = true;
            }
          });

          locai.forEach(function(child) {
            if(l === child.Local){
              localbool = true;
            }
          });

          if(localbool && disciplinabool){
            var ava = professores[i].Avaliações;
            var valorAva = professores[i].Avaliações_Valor;
            ava = parseInt(ava, 10);
            valorAva = parseInt(valorAva, 10);
            if(ava != 0){
              var resultado = valorAva / ava;
              resultado = resultado.toFixed(1);
              rat = resultado;
            }
            else{
              rat = 0;
            }

           if(professores[i].Cadastro){
               var cds = professores[i].Cadastro.substring(0, 5);
             }
             if(typeof cds === 'undefined'){
               cds = "17/03";
             }
             var pp = {
               "nome" : professores[i].Nome, 
               "curso" : professores[i].Curso, 
               "avatar" : professores[i].Avatar,  
               "descricao" : professores[i].Descrição, 
               "avaliacoes" : professores[i].Avaliações, 
               "rating" : rat,
               "key" : professores[i].$id, 
               "cadastro" : cds, 
               "profpush" : professores[i].Push_ID,
               "merchId" : professores[i].merchId
              };
             $scope.lehrer.push(pp);
             $scope.data.spinprofs = false;
             $scope.data.hideprofsmsg = true;
             ntemprof = false;
          }

        }
      }
    }


    $timeout(function() {
      if(ntemprof && $scope.data.spinprofs === true){
        $scope.data.spinprofs = false;
        $scope.data.hideprofsmsg = false;
        var alertPopup = $ionicPopup.alert({
         title: 'Professores',
         template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-search-strong icon-pop'></i></div></div></div><p class='custom'>Infelizmente não há professores disponíveis de <strong>" + agendamentoService.getDis() + "</strong> para <strong>" + agendamentoService.getLoc() + "</strong> no aplicativo. Por favor, escolha outro endereço ou entre em contato conosco para agendar sua aula.<br> <strong>" + $rootScope.modular1numero + "</strong><br>ou<br><strong>" + $rootScope.modular2numero + "</strong></p></div></div>",
         cssClass: 'popcustom',
         okType: 'customok'
       });
      }
      else if(ConnectivityMonitor.isOffline()){
        ionicToast.show('Sem conexão com a internet', 'middle', false, 2500);
        $scope.data.spinprofs = false;
      }
    }, 8000);
  });
//----------------------------------------------------------------------------//
$scope.refreshProfessores = function() {

  if(ConnectivityMonitor.isOnline()){

    horarios_professores = dataService.getHorariosProfessores();
    var e = agendamentoService.getDis();
    var p = categoryService.getCat();
    var l = agendamentoService.getLoc();
    var ntemprof = true;
    var dis;
    var loc;
    var cat;
    var disci = [];
    var locai = [];
    var categ = [];
    var localbool = false;
    var disciplinabool = false;

    for(var i = 0; i < professores.length; i++){
       disci = [];
       locai = [];
       categ = [];
       localbool = false;
       disciplinabool = false;
      for(var j = 0; j < horarios_professores.length; j++){
        if(professores[i].key === horarios_professores[j].key){
          dis = professores[i].Disciplinas;
          loc = professores[i].Locais;
          for(var item in dis){
            disci.push(dis[item]);
          }
          for(var item in loc){
            locai.push(loc[item]);
          }

          disci.forEach(function(child) {
            if(e === child.Nome && p === child.Categoria){
              disciplinabool = true;
            }
          });

          locai.forEach(function(child) {
            if(l === child.Local){
              localbool = true;
            }
          });

          if(localbool && disciplinabool){
            var ava = professores[i].Avaliações;
            var valorAva = professores[i].Avaliações_Valor;
            ava = parseInt(ava, 10);
            valorAva = parseInt(valorAva, 10);
            if(ava != 0){
              var resultado = valorAva / ava;
              resultado = resultado.toFixed(1);
              rat = resultado;
            }
            else{
              rat = 0;
            }

           if(professores[i].Cadastro){
               var cds = professores[i].Cadastro.substring(0, 5);
             }
             if(typeof cds === 'undefined'){
               cds = "17/03";
             }
             var pp = {
               "nome" : professores[i].Nome, 
               "curso" : professores[i].Curso, 
               "avatar" : professores[i].Avatar,  
               "descricao" : professores[i].Descrição, 
               "avaliacoes" : professores[i].Avaliações, 
               "rating" : rat,
               "key" : professores[i].key, 
               "cadastro" : cds,
               "profpush" : professores[i].Push_ID,
               "merchId" : professores[i].merchId
              };
             $scope.lehrer.push(pp);
             $scope.data.spinprofs = false;
             $scope.data.hideprofsmsg = true;
             ntemprof = false;
          }

        }
      }
    }

  }

  else{
    ionicToast.show('Sem conexão com a internet', 'middle', false, 2500);
  }

  $scope.$broadcast('scroll.refreshComplete');

};

//----------------------------------------------------------------------------//
  $scope.escolheuProf = function(x,y,w,z,a,r,cds,k,p,m) {

    $scope.profNome = x;
    $scope.profPro = "Estudante";
    $scope.profCurso = y;

    $scope.profrating = r;
    $rootScope.starrating = r;

    $scope.nomeperfilprof = x;
    teacherService.setProf(x);
    teacherService.setProfKey(k);
    teacherService.setAvatar(w);
    teacherService.setProfPush(p);
    teacherService.setMerchId(m);
    $scope.cursoperfilprof = y;
    $scope.descricaoperfilprof = z;
    $scope.imgfullprof = w;
    $scope.avaliacoesperfilprof = a;
    $scope.cadastroperfilprof = cds;

    $scope.model.show();

  };

//----------------------------------------------------------------------------//
  $scope.agendar = function() {

    var prof = teacherService.getProf();

    $scope.profCalendar = prof;
    $scope.model.hide();

        /*$ionicLoading.show({
          template: '<ion-spinner></ion-spinner> <br/>',
          duration: '1000'
        });*/

    $state.go('tabsController.horarios');

  };


})
