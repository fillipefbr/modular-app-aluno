angular.module('app.disciplinasCtrl', [])

//**************************_SUBJECT'S CONTROLLER_****************************//

.controller('disciplinasCtrl', function($scope, $rootScope, $document, $timeout, categoryService, $ionicLoading, $state, $cordovaNetwork, ionicToast, ConnectivityMonitor, agendamentoService, $ImageCacheFactory, dataService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

//----------------------------------------------------------------------------//

  /*firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      var cat = categoryService.getCat();
      categoryService.setCurrentCat(cat);
    }
  });*/

  $scope.$on("$ionicView.beforeEnter", function(){

    var disciplinas = dataService.getDisciplinas();
    var cat = categoryService.getCat();
    categoryService.setCurrentCat(cat);

    if(typeof $scope.facher != 'undefined' && categoryService.hasCatChanged()){
  
      $scope.facher = [];
      if(typeof cat != 'undefined'){
        for(var i = 0; i < disciplinas.length; i++){
          if(cat === disciplinas[i].Categoria){
            var fach = {"nome" : disciplinas[i].Nome, "icon" : disciplinas[i].Icon};
            $scope.facher.push(fach);
          }
        }
        categoryService.changedCatFalse();
      }
    }

    else if(typeof $scope.facher != 'undefined'){
      
    }

    else{
      
      $scope.facher = [];

      if(typeof cat != 'undefined'){
        for(var i = 0; i < disciplinas.length; i++){
          if(cat === disciplinas[i].Categoria){
            var fach = {"nome" : disciplinas[i].Nome, "icon" : disciplinas[i].Icon};
            $scope.facher.push(fach);
          }
        }

      }
    }

  });


//----------------------------------------------------------------------------//
  $scope.refreshDisciplinas = function() {

    var user = firebase.auth().currentUser;

    if(user && ConnectivityMonitor.isOnline() && categoryService.getCat() != categoryService.getCurrentCat()){

        user = user.uid;

          $scope.facher = [];
          var cat = categoryService.getCat();
          categoryService.setCurrentCat(cat);

          if(typeof cat != 'undefined'){


            dbRefDisciplinas.on('child_added', function(snap) {

              if(cat === snap.val().Categoria){

                var fach = {"nome" : snap.val().Nome, "icon" : snap.val().Icon};
                $scope.facher.push(fach);

              }

            });

          }
    }

    else if(ConnectivityMonitor.isOffline()){
      ionicToast.show('Sem conexão com a internet', 'middle', true, 2500);
    }


    $scope.$broadcast('scroll.refreshComplete');

  };

//----------------------------------------------------------------------------//
  $scope.mudaPage = function(nome){

    if(nome === "Tour pela UnB"){
      $state.go('tabsController.tour');
    }

    else{

      $scope.data.disciplina = nome;
      $scope.disciplina = nome;
      agendamentoService.setDis(nome);
      $state.go('tabsController.agendamento');
      /*$timeout(function() {

        var e = $document[0].getElementById('divider1');
        e.innerText = nome;

      }, 500);*/

    }


  };


})
