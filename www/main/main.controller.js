angular.module('app.mainCtrl', [])

//**************************_MAIN CONTROLLER_*********************************//

.controller('mainCtrl', function($scope, $rootScope, $document, $ionicPopup, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicActionSheet, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, ionicToast, ConnectivityMonitor, notificationsService, agendamentoService, $ImageCacheFactory) {

    $scope.data = {};

    /*$gn.ready(function(checkout) {
        $rootScope.checkout = checkout;
    });*/

    /*$ionicPlatform.ready(function() {
      window.plugins.OneSignal.getIds(function(ids) {
       $rootScope.pushID = ids.userId;
     });
   });*/

   //ConnectivityMonitor.startWatching();

    
    $scope.qtdAlunos = 1;
    $scope.selected = "Visa";


//----------------------------------------------------------------------------//

    const dbRefRoot = firebase.database().ref();
    const dbRefAlunos = dbRefRoot.child('alunos');
    const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
    const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
    const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
    const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
    const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
    const dbRefDisciplinas = dbRefRoot.child('disciplinas');
    const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
    const dbRefProfessores = dbRefRoot.child('professores');
    const dbRefCadastroPermanente = dbRefRoot.child('cadastro_permanente');
    const dbRefCodesPromocionais = dbRefRoot.child('codes_promocionais');
    const dbRefContatoModular = dbRefRoot.child('contato_modular');
    const dbRefAvaliacoes = dbRefRoot.child('avaliacoes_aulas');
    const auth = firebase.auth();


//----------------------------------------------------------------------------//

    var weekday = new Array(7);
    weekday[0]=  "Domingo";
    weekday[1] = "Segunda-Feira";
    weekday[2] = "Terça-Feira";
    weekday[3] = "Quarta-Feira";
    weekday[4] = "Quinta-Feira";
    weekday[5] = "Sexta-Feira";
    weekday[6] = "Sábado";

    var monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
      "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
    ];

//----------------------------------------------------------------------------//
$scope.checkAcc = function() {
  var user = firebase.auth().currentUser;
  if (user && user.emailVerified) {
    $state.go('tabsController.disciplinas');
  }
  else if(!user.emailVerified){
    var alertPopup = $ionicPopup.alert({
     title: 'Confirmar E-mail',
     template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Você ainda não clicou no link de verificação que enviamos para o seu e-mail. Por favor, verifique seu e-mail antes de continuar.</p></div></div>",
     cssClass: 'popcustom',
     okType: 'customok'
   });
  }
  else{
    $state.go('tabsController.disciplinas');
  }
};

 //$scope.$broadcast('scroll.refreshComplete');

 //---------------------------------------------------------------------------//
 $scope.help = function() {

   var alertPopup = $ionicPopup.alert({
    title: 'Aulas',
    template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-time-outline icon-pop'></i></div></div></div><p class='custom'>Os horários disponíveis do(s) professor(es) para o dia escolhido aparecerão aqui. Todas as aulas têm duração de uma hora e quarenta minutos.</p>",
    cssClass: 'popcustom',
    okType: 'customok'
  });


 };

 $scope.help2 = function() {

   var alertPopup = $ionicPopup.alert({
    title: 'Aulas',
    template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-calendar-outline icon-pop'></i></div></div></div><p class='custom'>Este é o seu calendário de aulas. As aulas marcadas por você aparecerão aqui, de modo que os dias nos quais você possui aulas agendadas estarão em destaque na cor indicada. Todas as aulas têm duração de uma hora e quarenta minutos.</p>",
    cssClass: 'popcustom',
    okType: 'customok'
  });


 };

 $scope.help3 = function() {

   var alertPopup = $ionicPopup.alert({
    title: 'Códigos',
    template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-pricetag-outline icon-pop'></i></div></div></div><p class='custom'>Os códigos promocionais que você possui aparecerão aqui, use-os para ter descontos exclusivos nas aulas. Cada código será utilizado somente uma única vez.</p>",
    cssClass: 'popcustom',
    okType: 'customok'
  });

 };

 $scope.ajuda = function() {

   var alertPopup = $ionicPopup.alert({
    title: 'Modular',
    template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-help icon-pop'></i></div></div></div><p class='custom'>A Modular é uma startup de aulas particulares, cuja missão é facilitar o contato entre professores e alunos. Para saber mais, cadastre-se e entre em contato conosco!</p>",
    cssClass: 'popcustom',
    okType: 'customok'
  });


 };

//----------------------------------------------------------------------------//
$scope.changeSenha = function(antiga, nova) {

  var user = firebase.auth().currentUser;

  if (user) {
    user = user.uid;

    var us = $document[0].getElementById('usersenha');
    var x = "";

    var senha;

    dbRefAlunos.on('child_added', function(snap) {

      if(user === snap.key){
        senha = snap.val().Senha;
      }

    });


    if(antiga === senha){
      dbRefAlunos.child(user).update({"Senha": nova});

      for(i = 0; i < nova.length; i++) {
        x += "*";
      }
      us.innerText = x;


      function alertDismissed() {
          // do something
      }

      navigator.notification.alert(
          'Sua senha foi alterada com sucesso.',  // message
          alertDismissed,         // callback
          'Concluído!',            // title
          'OK'                  // buttonName
      );

      $scope.modal.hide();
    }

    else{

      function alertDismissed2() {
          // do something
      }

      navigator.notification.alert(
          'Senha atual incorreta. Por favor, tente novamente.',  // message
          alertDismissed2,         // callback
          'Erro',            // title
          'OK'                  // buttonName
      );
    }

  }


};


//----------------------------------------------------------------------------//



});
