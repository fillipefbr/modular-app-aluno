angular.module('app.boletoCtrl', [])

//**************************_BOLETO CONTROLLER_********************************//

.controller('boletoCtrl', function($scope, $rootScope, $document, $ionicPopup, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, ionicToast, ConnectivityMonitor, notificationsService, dataService, agendamentoService, $firebaseObject) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');

  $scope.data = {};

  $scope.$on("$ionicView.beforeEnter", function(){

    $scope.data.boletoestado = "DF";

    var user = firebase.auth().currentUser;

    if (user) {

      user = user.uid;
      $timeout(function() {
        var dbRefA = dbRefAlunosPrivate.child(user);
        //$scope.data.shippingAddress = $firebaseObject(dbRefA.child("shippingAddress"));
        dbRefA.once('value', function(snap) {
          $scope.data.name = snap.val().Nome;
          if(snap.val().CPF != "Não informado"){
            $scope.data.cpf = parseInt(snap.val().CPF, 10);
          }
          $scope.data.email = snap.val().Email;
          if(snap.val().Aniversário != "Não informado"){
            var stringniver = snap.val().Aniversário;
            var arraydata = stringniver.split("-");
            var ano = parseInt(arraydata[0], 10);
            var mes = parseInt(arraydata[1], 10);
            var dia = parseInt(arraydata[2], 10);
            $scope.data.birth = new Date(ano, mes - 1, dia);
          }
          $scope.data.phone_number = snap.val().Telefone;
          if(snap.val().shippingAddress) {
            $scope.data.shippingAddress = snap.val().shippingAddress;
          }
        });

      }, 0);
    }

  });

//----------------------------------------------------------------------------//
$scope.confirmarAulaBoleto = function(data) {

  data.name = data.name.toString();
  data.name = data.name.replace(/[0-9]/g,'');
  if(/^[ ]*(.+[ ]+)+.+[ ]*$/.test(data.name) === false){
    ionicToast.show('Nome inválido', 'middle', false, 3000);
    return;
  }
  data.cpf = data.cpf.toString();
  data.cpf = data.cpf.replace(/\D/g,'');
  if((data.cpf).length != 11){
    ionicToast.show('CPF inválido', 'middle', false, 2500);
    return;
  }
  data.phone_number = data.phone_number.toString();
  data.phone_number = data.phone_number.replace(/\D/g,'');
  if(/^[1-9]{2}9?[0-9]{8}$/.test(data.phone_number) === false){
    ionicToast.show('Telefone inválido', 'middle', false, 2500);
    return;
  }

  data.shippingAddress.zipCode = (data.shippingAddress.zipCode).toString();
  data.shippingAddress.zipCode = (data.shippingAddress.zipCode).replace(/\D/g,'');
  data.shippingAddress.state = (data.shippingAddress.state).toUpperCase();

  //data.phone.areaCode = (data.phone_number).substring(0, 2);
  //data.phone.number = (data.phone_number).substring(2);
  var phoneareaCode = (data.phone_number).substring(0, 2);
  var phonenumber = (data.phone_number).substring(2);

  var yearbirth = data.birth.getFullYear();
  var monthbirth = data.birth.getMonth() + 1;
  if(monthbirth < 10){
    monthbirth = "0" + monthbirth;
  }
  var datebirth = data.birth.getDate();
  if(datebirth < 10){
    datebirth = "0" + datebirth;
  }
  //data.birth = yearbirth + "-" + monthbirth + "-" + datebirth;
  var birthDate = yearbirth + "-" + monthbirth + "-" + datebirth;

  //var user = agendamentoService.getAlunoKey();
  var user = firebase.auth().currentUser;
  data.ownId = user.uid;

  if (user) {

    $ionicLoading.show({
      template: "<ion-spinner></ion-spinner><br><p class='custom'>Carregando...</p>"
    });

    var datas = [];
    var child = agendamentoService.getArrHors();

    if(typeof $rootScope.pushID === 'undefined'){
      $ionicPlatform.ready(function() {
         window.plugins.OneSignal.getIds(function(ids) {
           $rootScope.pushID = ids.userId;
         });
       });
    }

    if(typeof $rootScope.pushID != 'undefined'){
      child.pushid = $rootScope.pushID;
    }

    datas.push(child);
    var authToken = dataService.getAuthToken();
    var cusId = paymentService.getCusId();
    
    var dbRefAlu = dbRefAlunos.child(user.uid);
    var nomeAluno = agendamentoService.getAlunoNome();
    var charge_id;

    dbRefAlunosPrivate.child(user.uid).update({"CPF" : data.cpf});
    dbRefAlunosPrivate.child(user.uid).update({"Aniversário" : birthDate});
    dbRefAlunosPrivate.child(user.uid).update({"shippingAddress" : data.shippingAddress}); //ADDED

    var presumovalor2 = agendamentoService.getPresumovalor2();
    presumovalor2 = parseInt(presumovalor2, 10);
    //var aula = "Modular Aulas";

    if(paymentService.hasCusId()) {
      
      agendamentoService.scheduleClassWithBankBillet(datas, authToken).then(function successCallback(response) {

        var resp = response.data;
        resp.forEach(function(child) {

          var aula = {
            "ownId": child.KeyPush,
            "price": child.Valor,
            "subject": child.Subject,
            "cusId": cusId,
            "merchId": teacherService.getMerchId(),
            "merchAmount": child.ValorProf
          }
    
          paymentService.createOrder(aula, authToken).then(function(respo) {
            charge_id = respo.data.id;
            dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"Charge_ID" : charge_id});
            dbRefAulasAlunos.child(child.AlunoID).child(child.KeyPush).update({"Charge_ID" : charge_id});
          });
    
          notificationsService.remindTeacherNotification(child.ProfPush, "Você tem uma aula hoje às " + child.HoraPush + " horas", child.DataPush).then(function(res) {
            notificationprofessorID = res.data.id;
            dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"NotificationProfessor_ID" : notificationprofessorID});
          }, function(error) {
            //ionicToast.show('Erro ao enviar notificação.', 'middle', true, 2500);
          });
          notificationsService.newClassNotification(child.ProfPush, "Você tem uma nova aula marcada para o dia " + child.Data + " horas").then(function(re) {
            
          },
          function(e) {
            //ionicToast.show('Erro ao mandar notificação para o professor.', 'middle', true, 2500);
          });
    
        });
    
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go("tabsController.aulas");
    
        $timeout(function() {
           var spun = $document[0].getElementById('spanaulas');
           if(spun != null){
             var spaan = spun.innerText;
             spaan = parseInt(spaan, 10);
             spaan = spaan + 1;
             spaan = spaan.toString();
             spun.innerText = spaan;
           }
           
           var alertPopup = $ionicPopup.alert({
             title: 'Concluído',
             template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Aula marcada!</p></div></div>",
             cssClass: 'popcustom',
             okType: 'customok'
           });
          $ionicHistory.clearCache();
          $ionicLoading.hide();
    
        }, 1000);
    
      }, function errorCallback(error) {
        var alertPopup = $ionicPopup.alert({
           title: 'Ooops!',
           template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>Ocorreu um erro! Por favor, tente novamente.</p></div></div>",
           cssClass: 'popcustom',
           okType: 'customok'
         });
        $ionicLoading.hide();
    
      });

    }
    else{
      var cus = {
        "ownId": user.uid,
        "fullname": nomeAluno,
        "email": user.email,
        "birthDate": birthDate,
        "cpf" : data.cpf,
        "phone": {
          "areaCode": phoneareaCode,
          "number": phonenumber
        },
        "shippingAddress": {
          "city": data.shippingAddress.city,
          "complement": data.shippingAddress.complement,
          "district": data.shippingAddress.district,
          "street": data.shippingAddress.street,
          "streetNumber": data.shippingAddress.streetNumber,
          "zipCode": data.shippingAddress.zipCode,
          "state": data.shippingAddress.state
        }
      }
      paymentService.createCustomer(cus, authToken).then(function successCallback(response) {
        dbRefAlunosPrivate.child(user.uid).update({"customerId" : response.data.id});
        paymentService.setCusId(response.data.id); //ADDED
        
        agendamentoService.scheduleClassWithBankBillet(datas, authToken).then(function successCallback(response) {

          var resp = response.data;
          resp.forEach(function(child) {

            var aula = {
              "ownId": child.KeyPush,
              "price": child.Valor,
              "subject": child.Subject,
              "cusId": cusId,
              "merchId": teacherService.getMerchId(),
              "merchAmount": child.ValorProf
            }
      
            paymentService.createOrder(aula, authToken).then(function(respo) {
              charge_id = respo.data.id;
              dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"Charge_ID" : charge_id});
              dbRefAulasAlunos.child(child.AlunoID).child(child.KeyPush).update({"Charge_ID" : charge_id});
            });
      
            notificationsService.remindTeacherNotification(child.ProfPush, "Você tem uma aula hoje às " + child.HoraPush + " horas", child.DataPush).then(function(res) {
              notificationprofessorID = res.data.id;
              dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"NotificationProfessor_ID" : notificationprofessorID});
            }, function(error) {
              //ionicToast.show('Erro ao enviar notificação.', 'middle', true, 2500);
            });
            notificationsService.newClassNotification(child.ProfPush, "Você tem uma nova aula marcada para o dia " + child.Data + " horas").then(function(re) {
              
            },
            function(e) {
              //ionicToast.show('Erro ao mandar notificação para o professor.', 'middle', true, 2500);
            });
      
          });
      
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go("tabsController.aulas");
      
          $timeout(function() {
             var spun = $document[0].getElementById('spanaulas');
             if(spun != null){
               var spaan = spun.innerText;
               spaan = parseInt(spaan, 10);
               spaan = spaan + 1;
               spaan = spaan.toString();
               spun.innerText = spaan;
             }
             
             var alertPopup = $ionicPopup.alert({
               title: 'Concluído',
               template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Aula marcada!</p></div></div>",
               cssClass: 'popcustom',
               okType: 'customok'
             });
            $ionicHistory.clearCache();
            $ionicLoading.hide();
      
          }, 1000);
      
        }, function errorCallback(error) {
          var alertPopup = $ionicPopup.alert({
             title: 'Ooops!',
             template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>Ocorreu um erro! Por favor, tente novamente.</p></div></div>",
             cssClass: 'popcustom',
             okType: 'customok'
           });
          $ionicLoading.hide();
      
        });

      }, function errorCallback(response) {
        $ionicLoading.hide();
      });
    }

  }

  else{
    $ionicLoading.hide();
    ionicToast.show('É necessário verificar seu e-mail antes de marcar uma aula.', 'middle', true, 2500);
    return;
  }

};



})
