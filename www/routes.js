angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('tabsController.aulas', {
    url: '/page2',
    views: {
      'tab1': {
        templateUrl: 'aulas/aulas.html',
        controller: 'aulasCtrl'
      }
    }
  })

  .state('tabsController.histRico', {
    url: '/page3',
    views: {
      'tab2': {
        templateUrl: 'historico/histRico.html',
        controller: 'historicoCtrl'
      }
    }
  })

  .state('tabsController.configuraEs', {
    url: '/page4',
    views: {
      'tab3': {
        templateUrl: 'ajustes/configuraEs.html',
        controller: 'ajustesCtrl'
      }
    }
  })

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'main/tabsController.html',
    abstract:true
  })

  .state('tabsController.disciplinas', {
    url: '/page6',
    views: {
      'tab1': {
        templateUrl: 'disciplinas/disciplinas.html',
        controller: 'disciplinasCtrl'
      }
    }
  })

  .state('inicio', {
    url: '/inicio',
    templateUrl: 'main/inicio.html',
    controller: 'mainCtrl'
  })

  .state('loginAluno', {
    url: '/loginAluno',
    templateUrl: 'login/loginAluno.html',
    controller: 'loginCtrl'
  })

  .state('cadastro', {
    url: '/cadastro',
    templateUrl: 'cadastro/cadastro.html',
    controller: 'registerCtrl'
  })

  .state('tabsController.agendamento', {
    url: '/page7',
    views: {
      'tab1': {
        templateUrl: 'agendamento/agendamento.html',
        controller: 'agendamentoCtrl'
      }
    }
  })

  .state('tabsController.horarios', {
    cache: true,
    url: '/page13',
    views: {
      'tab1': {
        templateUrl: 'calendarprof/horarios.html',
        controller: 'calendarCtrl'
      }
    }
  })

  .state('tabsController.resumo', {
    url: '/page14',
    views: {
      'tab1': {
        templateUrl: 'resumo/resumo.html',
        controller: 'resumoCtrl'
      }
    }
  })

  .state('tabsController.marcadas', {
    url: '/page16',
    views: {
      'tab1': {
        templateUrl: 'marcadas/marcadas.html',
        controller: 'marcadasCtrl'
      }
    }
  })

  .state('tabsController.calendarioPessoal', {
    url: '/page22',
    views: {
      'tab1': {
        templateUrl: 'calendariopessoal/calendarioPessoal.html',
        controller: 'calendarioPessoalCtrl'
      }
    }
  })

  .state('tabsController.todosHorarios', {
    cache: false,
    url: '/page23',
    views: {
      'tab1': {
        templateUrl: 'calendarall/todosHorarios.html',
        controller: 'calendarAllCtrl'
      }
    }
  })


  .state('tabsController.financeiro', {
    url: '/page25',
    views: {
      'tab2': {
        templateUrl: 'financeiro/financeiro.html',
        controller: 'financeiroCtrl'
      }
    }
  })

  .state('tabsController.addressMine', {
    url: '/page27',
    views: {
      'tab1': {
        templateUrl: 'enderecos/addressMine.html',
        controller: 'addressMineCtrl'
      }
    }
  })

  .state('tabsController.aulasSemanais', {
    url: '/page29',
    views: {
      'tab1': {
        templateUrl: 'pacotes/aulasSemanais.html',
        controller: 'pacotesCtrl'
      }
    }
  })

  .state('tabsController.minhaCategoria', {
    url: '/page30',
    views: {
      'tab3': {
        templateUrl: 'categorias/minhaCategoria.html',
        controller: 'categoriasCtrl'
      }
    }
  })

  .state('tabsController.codigoPromocional', {
    url: '/codigoPromocional',
    views: {
      'tab1': {
        templateUrl: 'codes/codigoPromocional.html',
        controller: 'codigoPromocionalCtrl'
      }
    }
  })

  .state('tabsController.meusCartoes', {
    url: '/meusCartoes',
    views: {
      'tab1': {
        templateUrl: 'cards/meusCartoes.html',
        controller: 'cardsCtrl'
      }
    }
  })

  .state('tabsController.addCartao', {
    url: '/addCartao',
    views: {
      'tab1': {
        templateUrl: 'cards/addCartao.html',
        controller: 'cardsCtrl'
      }
    }
  })

  .state('tabsController.addCartaoConfig', {
    url: '/addCartaoConfig',
    views: {
      'tab3': {
        templateUrl: 'cards/addCartaoConfig.html',
        controller: 'cardsCtrl'
      }
    }
  })

  .state('tabsController.addCartaoAdress', {
    url: '/addCartaoAdress',
    views: {
      'tab1': {
        templateUrl: 'cards/addCartaoAdress.html',
        controller: 'cardsCtrl'
      }
    }
  })

  .state('tabsController.addCartaoAdressConfig', {
    url: '/addCartaoAdressConfig',
    views: {
      'tab3': {
        templateUrl: 'cards/addCartaoAdressConfig.html',
        controller: 'cardsCtrl'
      }
    }
  })

  .state('tabsController.boleto', {
    url: '/boleto',
    views: {
      'tab1': {
        templateUrl: 'boleto/boleto.html',
        controller: 'boletoCtrl'
      }
    }
  })

  .state('tabsController.changePass', {
    url: '/changePass',
    views: {
      'tab3': {
        templateUrl: 'main/changePass.html',
        controller: 'mainCtrl'
      }
    }
  })

  .state('tabsController.tour', {
    url: '/tour',
    views: {
      'tab1': {
        templateUrl: 'tour/tour.html',
        controller: 'tourCtrl'
      }
    }
  })

  .state('tabsController.tourdias', {
    url: '/tourdias',
    views: {
      'tab1': {
        templateUrl: 'tour/tourdias.html',
        controller: 'tourCtrl'
      }
    }
  })

  .state('tabsController.favoritos', {
    cache: true,
    url: '/favoritos',
    views: {
      'tab4': {
        templateUrl: 'favoritos/favoritos.html',
        controller: 'favoritosCtrl'
      }
    }
  })

  .state('tabsController.profsfav', {
    url: '/profsfav',
    views: {
      'tab4': {
        templateUrl: 'favoritos/profsfav.html',
        controller: 'addfavoritosCtrl'
      }
    }
  })

  .state('tabsController.calendariofav', {
    url: '/calendariofav',
    views: {
      'tab4': {
        templateUrl: 'favoritos/calendariofav.html',
        controller: 'calendariofavoritosCtrl'
      }
    }
  })

  .state('tabsController.agendamentofav', {
    url: '/agendamentofav',
    views: {
      'tab4': {
        templateUrl: 'favoritos/agendamentofav.html',
        controller: 'agefavCtrl'
      }
    }
  })

  .state('tabsController.meuscodes', {
    url: '/meusCodes',
    views: {
      'tab3': {
        templateUrl: 'codes/meuscodes.html',
        controller: 'codesCtrl'
      }
    }
  })

  .state('tabsController.meusenderecos', {
    url: '/meusenderecos',
    views: {
      'tab3': {
        templateUrl: 'enderecos/meusenderecos.html',
        controller: 'addressCtrl'
      }
    }
  })

  .state('tabsController.meuscartoesconfig', {
    url: '/meusCartoesConfig',
    views: {
      'tab3': {
        templateUrl: 'cards/cardsConfig.html',
        controller: 'cardsCtrl'
      }
    }
  })

  .state('tabsController.resumofav', {
    url: '/resumofav',
    views: {
      'tab4': {
        templateUrl: 'favoritos/resumofav.html',
        controller: 'resumoFavCtrl'
      }
    }
  })

  .state('tabsController.escola', {
    url: '/escola',
    views: {
      'tab3': {
        templateUrl: 'escola/escola.html',
        controller: 'escolaCtrl'
      }
    }
  })

  .state('tabsController.boletofav', {
    url: '/boletofav',
    views: {
      'tab4': {
        templateUrl: 'favoritos/boletofav.html',
        controller: 'boletoFavCtrl'
      }
    }
  })

  .state('tabsController.transfer', {
    url: '/transfer',
    views: {
      'tab1': {
        templateUrl: 'transfer/transfer.html',
        controller: 'transferCtrl'
      }
    }
  })

  .state('tabsController.transferfav', {
    url: '/transferfav',
    views: {
      'tab4': {
        templateUrl: 'favoritos/transferfav.html',
        controller: 'transferFavCtrl'
      }
    }
  })

  .state('tabsController.professores', {
    cache: true,
    url: '/page10',
    views: {
      'tab1': {
        templateUrl: 'professores/professores.html',
        controller: 'teachersCtrl'
      }
    }
  })

//$urlRouterProvider.otherwise('/page1/page2')
//$urlRouterProvider.otherwise('inicio')



});
