angular.module('app.agendamentoCtrl', [])

//*************************_AGENDAMENTO CONTROLLER_***************************//

.controller('agendamentoCtrl', function($scope, $rootScope, $document, $timeout, categoryService, $ionicLoading, $state, $cordovaNetwork, ionicToast, ConnectivityMonitor, agendamentoService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

  $scope.data = {};

  $scope.$on("$ionicView.loaded", function(){
    $scope.ageact(1);
  });

//----------------------------------------------------------------------------//

  $scope.$on("$ionicView.loaded", function(){
    //$scope.ageact(1);
    var user = firebase.auth().currentUser;
    if (user) {
      $scope.disnome = agendamentoService.getDis();
      dbRefAlunosPrivate.child(user.uid).child("Endereços").on('child_added', function(snap) {
        if(snap.val().Padrão === true){
          var adresstxt = $document[0].getElementById('adresstxt');
          var locale = $document[0].getElementById('localselect');
          adresstxt.innerText = snap.val().Endereço;
          locale.innerText = snap.val().Local;
        }
      });
    }

  });

//----------------------------------------------------------------------------//
$scope.ageact = function(x) {
  switch (x) {
    case 1:
      agendamentoService.setAlunos(1);
      $scope.ageact1 = true;
      $scope.ageact2 = false;
      $scope.ageact3 = false;
      $scope.ageact4 = false;
      $scope.ageact5 = false;
      break;
    case 2:
      agendamentoService.setAlunos(2);
      $scope.ageact1 = false;
      $scope.ageact2 = true;
      $scope.ageact3 = false;
      $scope.ageact4 = false;
      $scope.ageact5 = false;
      break;
    case 3:
      agendamentoService.setAlunos(3);
      $scope.ageact1 = false;
      $scope.ageact2 = false;
      $scope.ageact3 = true;
      $scope.ageact4 = false;
      $scope.ageact5 = false;
      break;
    case 4:
      agendamentoService.setAlunos(4);
      $scope.ageact1 = false;
      $scope.ageact2 = false;
      $scope.ageact3 = false;
      $scope.ageact4 = true;
      $scope.ageact5 = false;
      break;
    case 5:
      agendamentoService.setAlunos(5);
      $scope.ageact1 = false;
      $scope.ageact2 = false;
      $scope.ageact3 = false;
      $scope.ageact4 = false;
      $scope.ageact5 = true;
      break;
    default:
      agendamentoService.setAlunos(1);
      $scope.ageact1 = true;
      $scope.ageact2 = false;
      $scope.ageact3 = false;
      $scope.ageact4 = false;
      $scope.ageact5 = false;
      break;
  }
};

//----------------------------------------------------------------------------//
$scope.goModal2 = function() {
  $state.go('tabsController.addressMine');
};

//----------------------------------------------------------------------------//
$scope.moveProfs = function(cnt) {
  var adresstxt = $document[0].getElementById('adresstxt');
  var loc = $document[0].getElementById('localselect');
  if(adresstxt.innerText === "Toque para escolher"){
    ionicToast.show('Por favor, selecione um endereço', 'middle', false, 2500);
    return;
  }
  if(typeof cnt === 'undefined' || cnt.length === 0){
    ionicToast.show('Por favor, informe o conteúdo da aula', 'middle', false, 2500);
    return;
  }
  else{
    agendamentoService.setLoc(loc.innerText);
    $state.go('tabsController.professores');
  }
};


})
