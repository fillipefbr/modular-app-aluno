angular.module('app.addressCtrl', [])

//**************************_ADRESSES CONTROLLER_*****************************//

.controller('addressCtrl', function($scope, $rootScope, $document, $ionicPopup, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicActionSheet, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, ionicToast, ConnectivityMonitor, notificationsService, agendamentoService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const dbRefCadastroPermanente = dbRefRoot.child('cadastro_permanente');
  const dbRefCodesPromocionais = dbRefRoot.child('codes_promocionais');
  const dbRefContatoModular = dbRefRoot.child('contato_modular');
  const auth = firebase.auth();

  $scope.upadress = false;
  $scope.textoadress = "Editar";

  $ionicModal.fromTemplateUrl('enderecos/addMeuAddress.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.modal != 'undefined'){
      $scope.modal.remove();
    }
  });


  $scope.$on("$ionicView.beforeEnter", function(){

    $scope.ads = [];
    $scope.data.spinads = true;
    $scope.data.nenhumadress = false;
    var mensagem;
    var key;
    var novo = true;
    var local;

    var user = firebase.auth().currentUser;

    if (user) {

      user = user.uid;
      var dbRefA = dbRefAlunosPrivate.child(user);
      var dbRefE = dbRefA.child("Endereços");
      var vai;

      dbRefE.on('child_added', function(snap) {

        mensagem = snap.val().Endereço;
        local = snap.val().Local;
        key = snap.key;

        if(snap.val().Padrão === true){
          var ad = {"adress" : mensagem, "key" : key, "local" : local, "class" : "ion-ios-location"};
          $scope.ads.push(ad);
        }
        else{
          var ad = {"adress" : mensagem, "key" : key, "local" : local, "class" : "ion-ios-location-outline"};
          $scope.ads.push(ad);
        }
        novo = false;
        $scope.data.spinads = false;

      });

      $timeout(function() {

        if(novo){
          $scope.data.spinads = false;
          $scope.data.nenhumadress = true;
        }

      }, 2000);

    }

  });

//----------------------------------------------------------------------------//
$scope.escolheuMeuAdress = function(ad, loc) {
  var alertPopup = $ionicPopup.alert({
   title: loc,
   template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-location icon-pop'></i></div></div></div><p class='custom'>" + ad + "</p></div></div>",
   cssClass: 'popcustom',
   okType: 'customok'
 });
};
//----------------------------------------------------------------------------//
$scope.checkAdressLimit = function() {

  var user = firebase.auth().currentUser;
  var counter = 0;
  var limit = 5;

  if (user) {
    user = user.uid;
    var dbRefE = dbRefAlunosPrivate.child(user).child("Endereços");
    dbRefE.on('child_added', function(snap) {
      counter += 1;
    });
    $timeout(function() {
      if(counter < limit){
        $scope.modal.show();
      }
      else{
        var alertPopup = $ionicPopup.alert({
         title: 'Aviso',
         template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-information icon-pop'></i></div></div></div><p class='custom'>O número máximo de endereços que você pode adicionar é igual a 5.</p></div></div>",
         cssClass: 'popcustom',
         okType: 'customok'
       });
      }
    }, 500);
  }

};
//----------------------------------------------------------------------------//
$scope.adressDelete = function(key){

  var user = firebase.auth().currentUser;

  if (user) {
    user = user.uid;

    $ionicLoading.show({
      template: "<ion-spinner icon='ios-small'></ion-spinner><br><p class='custom'>Excluindo</p>",
      duration: '2000'
    });

    var index = -1;
    var dbRefA = dbRefAlunosPrivate.child(user);
    var dbRefE = dbRefA.child("Endereços");
    dbRefE.child(key).remove();

    var val = key;
    var filteredObj = $scope.ads.find(function(item, i){
      if(item.key === val){
        index = i;
      }
    });

    if (index > -1) {
      $scope.ads.splice(index, 1);
    }

  }

};

//----------------------------------------------------------------------------//
$scope.adressStd = function(key, ad) {
  var myPopup = $ionicPopup.show({
  template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-home icon-pop'></i></div></div></div><p class='custom'>Você gostaria de definir <strong>" + ad + "</strong> como seu endereço padrão? (Seu endereço será preenchido automaticamente).</p></div></div>",
  title: "Endereço Padrão",
  cssClass: 'popcustom',
  scope: $scope,
  buttons: [
    { text: 'Cancelar', type: 'customconfirm' },
    {
      text: '<b>Sim</b>',
      type: 'customconfirm',
      onTap: function(e) {
        var user = firebase.auth().currentUser;
        if(user){
          user = user.uid;
          dbRefAlunosPrivate.child(user).child("Endereços").once('child_added', function(snap) {
            if(snap.val().Padrão === true){
              dbRefAlunosPrivate.child(user).child("Endereços").child(snap.key).update({"Padrão" : false});
              dbRefAlunosPrivate.child(user).child("Endereços").child(key).child("Padrão").set(true);
              ionicToast.show('Endereço padrão atualizado', 'middle', false, 2500);
              $ionicListDelegate.closeOptionButtons();
            }
            else{
              dbRefAlunosPrivate.child(user).child("Endereços").child(key).child("Padrão").set(true);
              ionicToast.show('Endereço padrão atualizado', 'middle', false, 2500);
              $ionicListDelegate.closeOptionButtons();
            }
          });
        }
      }
    }
  ]
  });
};
//----------------------------------------------------------------------------//
function addAdress(local) {

  var user = firebase.auth().currentUser;

  if (user) {
    user = user.uid;
    var resultado;
    var dbRefA;
    var dbRefE;
    function onPrompt(results) {
      var userText = results.input1.replace(/^\s*/, '').replace(/\s*$/, '');

      if(results.buttonIndex === 2 && userText != ''){
        resultado = userText;
        var myPopup = $ionicPopup.show({
        template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-home icon-pop'></i></div></div></div><p class='custom'>Você gostaria de definir <strong>" + resultado + "</strong> como seu endereço padrão? (Seu endereço será preenchido automaticamente).</p></div></div>",
        title: "Endereço Padrão",
        cssClass: 'popcustom',
        scope: $scope,
        buttons: [
          { text: 'Não',
            type: 'customconfirm',
            onTap: function(e) {
              $ionicLoading.show({
                template: "<ion-spinner icon='ios-small'></ion-spinner><br/><p class='custom'>Adicionando</p>",
                duration: '2000'
              });
              dbRefAlunosPrivate.child(user).child("Endereços").push({"Endereço" : resultado, "Local" : local});
              $scope.data.nenhumadress = false;
            }
          },
          {
            text: '<b>Sim</b>',
            type: 'customconfirm',
            onTap: function(e) {
              $ionicLoading.show({
                template: "<ion-spinner icon='ios-small'></ion-spinner><br/><p class='custom'>Adicionando</p>",
                duration: '2000'
              });
              dbRefAlunosPrivate.child(user).child("Endereços").once('value', function(snap) {
                if(snap.val() && snap.val().Padrão === true){
                  dbRefAlunosPrivate.child(user).child("Endereços").child(snap.key).update({"Padrão" : false});
                  dbRefAlunosPrivate.child(user).child("Endereços").push({"Endereço" : resultado, "Local" : local, "Padrão" : true});
                  ionicToast.show('Endereço padrão atualizado', 'middle', false, 2500);
                  $scope.data.nenhumadress = false;
                }
                else{
                  dbRefAlunosPrivate.child(user).child("Endereços").push({"Endereço" : resultado, "Local" : local, "Padrão" : true});
                  ionicToast.show('Endereço padrão atualizado', 'middle', false, 2500);
                  $scope.data.nenhumadress = false;
                }
              });
            }
          }
        ]
        });
      }
      else if(results.buttonIndex === 1){

      }
      else{
        var alertPopup = $ionicPopup.alert({
         title: 'Aviso',
         template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Por favor, informe um local válido para a aula.</p></div></div>",
         cssClass: 'popcustom',
         okType: 'customok'
       });
      }
    }

    navigator.notification.prompt(
        'Digite o endereço do local da aula no campo abaixo:',  // message
        onPrompt,                  // callback to invoke
        'Local da Aula',            // title
        ['Cancelar','Ok']             // buttonLabels
    );

      $scope.upadress = false;
      $scope.textoadress = "Editar";

  }

};
//----------------------------------------------------------------------------//
$scope.localSelect = function(local) {

    addAdress(local);
    $scope.modal.hide();

};
//----------------------------------------------------------------------------//
$scope.editaAdress = function() {
  if($scope.upadress === false){
    $scope.upadress = true;
    $scope.textoadress= "Ok";
  }

  else{
    $scope.upadress = false;
    $scope.textoadress= "Editar";
  }
};

//----------------------------------------------------------------------------//

})
