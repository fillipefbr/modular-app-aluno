angular.module('app.transferCtrl', [])

//****************************_TRANSFER CONTROLLER******************************//

.controller('transferCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, categoryService, ionicToast, $ionicPopup, agendamentoService, dataService, notificationsService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

//----------------------------------------------------------------------------//
  function confirmarAulaTrans() {

    var user = agendamentoService.getAlunoKey();
    var datas = [];
    var child = agendamentoService.getArrHors();

    if(typeof $rootScope.pushID === 'undefined'){
      $ionicPlatform.ready(function() {
         window.plugins.OneSignal.getIds(function(ids) {
           $rootScope.pushID = ids.userId;
         });
       });
    }

    if(typeof $rootScope.pushID != 'undefined'){
      child.pushid = $rootScope.pushID;
    }
    
    datas.push(child);

    if (user && datas) {

      $ionicLoading.show({
        template: "<ion-spinner icon='ios-small'></ion-spinner><br><p class='custom'>Marcando<br>aula</p>"
      });

      var dbRefAlu = dbRefAlunos.child(user);
      var nomeAluno = agendamentoService.getAlunoNome();
      var charge_id;
      var notificationalunoID;
      var notificationprofessorID;

      agendamentoService.marcarAulaTrans(datas).then(function successCallback(response) {

        var resp = response.data;
        resp.forEach(function(child) {
          notificationsService.remindTeacherNotification(child.ProfPush, "Você tem uma aula hoje às " + child.HoraPush + " horas", child.DataPush).then(function(res) {
            notificationprofessorID = res.data.id;
            dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"NotificationProfessor_ID" : notificationprofessorID});
          }, function(error) {
            //ionicToast.show('Erro ao enviar notificação.', 'middle', true, 2500);
          });
          notificationsService.newClassNotification(child.ProfPush, "Você tem uma nova aula marcada para o dia " + child.Data + " horas").then(function(re) {
            
          },
          function(e) {
            //ionicToast.show('Erro ao mandar notificação para o professor.', 'middle', true, 2500);
          });
        });
        
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go("tabsController.aulas");

        $timeout(function() {
           var spun = $document[0].getElementById('spanaulas');
           if(spun != null){
             var spaan = spun.innerText;
             spaan = parseInt(spaan, 10);
             spaan = spaan + 1;
             spaan = spaan.toString();
             spun.innerText = spaan;
           }
           
           var alertPopup = $ionicPopup.alert({
             title: 'Concluído',
             template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Aula marcada!</p></div></div>",
             cssClass: 'popcustom',
             okType: 'customok'
           });
           $ionicHistory.clearCache();
           $ionicLoading.hide();

        }, 1000);

      }, function errorCallback(error) {
        var alertPopup = $ionicPopup.alert({
           title: 'Ooops!',
           template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>Ocorreu um erro! Por favor, tente novamente.</p></div></div>",
           cssClass: 'popcustom',
           okType: 'customok'
         });
        $ionicLoading.hide();

      });

    }

  };


//----------------------------------------------------------------------------//
  $scope.check4email = function() {

    var user = firebase.auth().currentUser;
    var ferboden = false;
    var now = new Date();
    var dia = now.getDate();
    var mes = now.getMonth() + 1;
    var ano = now.getFullYear();
    var diaaula;
    var mesaula;
    var anoaula;

    if (user) {

      dbRefAulasAlunos.child(user.uid).on('child_added', function(snap) {
        if(typeof snap.val().Professor_Finalizou != 'undefined'){
          diaaula = parseInt(snap.val().Dia, 10);
          mesaula = parseInt(snap.val().Mês, 10);
          anoaula = parseInt(snap.val().Ano, 10);
          if(snap.val().Professor_Finalizou === "Não" && diaaula < dia && mesaula <= mes && anoaula <= ano || mesaula < mes && anoaula <= ano){
            
            var alertPopup = $ionicPopup.alert({
             title: 'Aviso',
             template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Constatamos que você ainda não finalizou uma aula que já ocorreu. Por favor, encerre-a na sua agenda antes de marcar outra aula.</p></div></div>",
             cssClass: 'popcustom',
             okType: 'customok'
           });

            ferboden = true;
          }
        }
      });

      $timeout(function() {
        if (user.emailVerified) {
          confirmarAulaTrans();
        }
        else if(!user.emailVerified){
          ionicToast.show('É necessário verificar seu e-mail antes de marcar uma aula. Por favor, confirme seu e-mail e faça login novamente.', 'middle', true, 2500);
        }
      }, 0);
    }
  };


})
