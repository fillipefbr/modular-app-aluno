angular.module('app.financeiroCtrl', [])

//****************************_FINANCEIRO CONTROLLER******************************//

.controller('financeiroCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, categoryService, ionicToast, $ionicPopup) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessores = dbRefRoot.child('professores');

  $ionicModal.fromTemplateUrl('financeiro/financeiroDetalhes.html', {
    scope: $scope
  }).then(function(modil) {
    $scope.modil = modil;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.modil != 'undefined'){
      $scope.modil.remove();
    }
  });

  // Constants declarations
  var weekday = new Array(7);
  weekday[0]=  "Domingo";
  weekday[1] = "Segunda-Feira";
  weekday[2] = "Terça-Feira";
  weekday[3] = "Quarta-Feira";
  weekday[4] = "Quinta-Feira";
  weekday[5] = "Sexta-Feira";
  weekday[6] = "Sábado";

  var monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
    "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
  ];

//----------------------------------------------------------------------------//
  $scope.$on("$ionicView.beforeEnter", function(){
    
    var user = firebase.auth().currentUser;
    $scope.nenhumhistfin = false;
    $scope.loadhistfin = true;

    if (user) {

      user = user.uid;
      $scope.classes = [];

      var data;
      var datares;
      var mesInt;
      var loc;
      var val;
      var nenhumhistfin = true;

      var dbRefA = dbRefAlunosPrivate.child(user);
      var dbRefF = dbRefA.child("Financeiro");

        dbRefF.on('child_added', function(snap) {

          mesInt = snap.val().Mês;
          mesInt = parseInt(mesInt, 10);
          mesInt -= 1;
          data = monthNames[mesInt].substring(0, 3) + " " + snap.val().Dia + ", " + snap.val().Ano;
          datares = snap.val().Dia + '/' + snap.val().Mês;
          loc = snap.val().Local;
          val = snap.val().Valor;
          val = val.toString();
          val = val.replace(/\D/g,'');
          val = parseInt(val, 10);
          val = val / 100;
          val = val.toFixed(2);
          val = val.toString().replace(".", ",");
          var classe = {"nome" : snap.val().Data + " - " + snap.val().Hora, "disciplina" : snap.val().Disciplina, "data" : data, "datares" : datares, "hora" : snap.val().Hora, "valor" : val, "local" : loc};
          $scope.loadhistfin = false;
          $scope.classes.push(classe);
          nenhumhistfin = false;
        });
        $timeout(function() {
          if(nenhumhistfin === true){
            $scope.loadhistfin = false;
            $scope.nenhumhistfin = true;
          }
        }, 2000);
    }
    
  });

//----------------------------------------------------------------------------//
$scope.finitem = function(nome) {
  var user = firebase.auth().currentUser;
  if (user) {
    user = user.uid;
    var dbRefA = dbRefAlunosPrivate.child(user);
    var dbRefF = dbRefA.child("Financeiro");

    dbRefF.on('child_added', function(snap) {
      if(nome === (snap.val().Data + " - " + snap.val().Hora)){
        $scope.findata = snap.val().Data;
        $scope.finhora = snap.val().Hora;
        $scope.finprof = snap.val().Professor;
        $scope.finlocal = snap.val().Local;
        $scope.findisciplina = snap.val().Disciplina;
        val = snap.val().Valor;
        val = val.toString();
        val = val.replace(/\D/g,'');
        val = parseInt(val, 10);
        val = val / 100;
        val = val.toFixed(2);
        val = val.toString().replace(".", ",");
        $scope.finvalor = "R$ " + val;
      }
    });
    $scope.modil.show();
  }
};


})
