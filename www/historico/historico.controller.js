angular.module('app.historicoCtrl', [])

//****************************_HISTORICO CONTROLLER******************************//

.controller('historicoCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, categoryService, ionicToast, $ionicPopup) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

//----------------------------------------------------------------------------//
  $scope.$on("$ionicView.beforeEnter", function(){
    
    var user = firebase.auth().currentUser;
    if (user) {
      user = user.uid;
      //$scope.hists = [];
      var dbRefP = dbRefAlunosPrivate.child(user);
      var dbRefH = dbRefP.child('Disciplinas');
      var dis = [];
      var his = [];
      $scope.nenhumhist = false;

      dbRefP.once('value', function(snapshoot) {
        $timeout(function() {
          $scope.aconc = snapshoot.val().AulasConcluidas;
          $scope.acanc = snapshoot.val().AulasCanceladas;
        }, 0);
      });

      dbRefH.once('value', function(snap) {
        if(snap.val()){
          snap.forEach(function(child) {
            var hist = {"nome" : child.val().Nome, "aulas" : child.val().Aulas, "icon" : child.val().Icon, "cat" : '(' + child.val().Categoria + ')'};
            //$scope.hists.push(hist);
            his.push(hist);
          });
          $scope.hists = his;
        }
        else{
          $scope.nenhumhist = true;
        }
      });

      /*dbRefH.on('child_added', function(snap) {
        var hist = {"nome" : snap.val().Nome, "aulas" : snap.val().Aulas, "icon" : snap.val().Icon, "cat" : '(' + snap.val().Categoria + ')'};
        $scope.hists.push(hist);
      });*/

    }
    
  });


})
