angular.module('app.registerCtrl', [])

//************************_REGISTRATION CONTROLLER_***************************//

.controller('registerCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, categoryService, ConnectivityMonitor, ionicToast, $ionicPopup, $cordovaDevice, dataService, agendamentoService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const dbRefCadastroPermanente = dbRefRoot.child('cadastro_permanente');
  const auth = firebase.auth();

  $scope.data = {};

  $ionicModal.fromTemplateUrl('cadastro/categoriasCadastro.html', {
    scope: $scope
  }).then(function(modccl) {
    $scope.modccl = modccl;
  });

  $ionicModal.fromTemplateUrl('cadastro/cadastroescolamodal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.modccl != 'undefined'){
      $scope.modccl.remove();
    }
    if(typeof $scope.modal != 'undefined'){
      $scope.modal.remove();
    }
  });

//----------------------------------------------------------------------------//
  $scope.escolaSelect = function(code, escola) {
    $scope.data.escola = escola;
    $scope.modal.hide();
  };
//----------------------------------------------------------------------------//
  $scope.interRegister = function(nome, email, phone, escola, pass, passconfirm) {

    if(pass != passconfirm){
      var alertPopup = $ionicPopup.alert({
        title: 'Erro',
        template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>As senhas digitadas não são iguais. Por favor, confirme sua senha novamente.</p></div></div>",
        cssClass: 'popcustom',
        okType: 'customok'
      });
      return;
    }

    else{
      $rootScope.nomecadastro = nome;
      $rootScope.emailcadastro = email;
      phone = phone.toString();
      phone = phone.replace(/\D/g,'');
      if(/^[1-9]{2}9?[0-9]{8}$/.test(phone) === false){
          var alertPopup = $ionicPopup.alert({
           title: 'Contato',
           template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-telephone icon-pop'></i></div></div></div><p class='custom'>Por favor, informe seu DDD antes do seu número (Sem formatação).</p></div></div>",
           cssClass: 'popcustom',
           okType: 'customok'
         });
        return;
      }

      if(/^[ ]*(.+[ ]+)+.+[ ]*$/.test(nome) === false){
          var alertPopup = $ionicPopup.alert({
           title: 'Nome',
           template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-information icon-pop'></i></div></div></div><p class='custom'>Por favor, informe seu nome completo.</p></div></div>",
           cssClass: 'popcustom',
           okType: 'customok'
         });
        return;
      }
      $rootScope.phonecadastro = phone;
      $rootScope.passcadastro = pass;
      $rootScope.escolacadastro = escola;
      $scope.modccl.show();
    }

  };

//----------------------------------------------------------------------------//
    $scope.addCat = function(cat){
      $rootScope.userCat = cat;
      $scope.modccl.hide();
      $scope.register();
    };

//----------------------------------------------------------------------------//
    $scope.register = function() {

        $ionicLoading.show({
          template: "<ion-spinner icon='ios-small'></ion-spinner>"
        });

        var nivercadastro = "Não informado";
        var cpfcadastro = "Não informado";

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd;
        }

        if(mm<10) {
            mm='0'+mm;
        }

        today = dd+'/'+mm+'/'+yyyy;

        var model = $cordovaDevice.getModel();
        var platform = $cordovaDevice.getPlatform();
        var version = $cordovaDevice.getVersion();

        const promise =  auth.createUserWithEmailAndPassword($rootScope.emailcadastro, $rootScope.passcadastro);

        promise.then(function(currentUser) {

          $rootScope.uid = currentUser.uid;

          if(typeof $rootScope.pushID === 'undefined'){
            $rootScope.pushID = "undefined";
          }

          if(typeof $rootScope.escolacadastro === 'undefined'){
            $rootScope.escolacadastro = "Indefinida";
          }

          dbRefAlunosPrivate.child($rootScope.uid).set({"Nome" : $rootScope.nomecadastro, "Cadastro" : today, "Email_Verificado" : false, "Aniversário": nivercadastro, "AulasConcluidas": "0", "AulasCanceladas": "0" , "Senha": $rootScope.passcadastro, "Login": today, "Dispositivo" : model,
          "Telefone": $rootScope.phonecadastro, "OS" : platform, "OS_Version" : version, "Categoria" : $rootScope.userCat, "Email" : $rootScope.emailcadastro, "Push_ID" : $rootScope.pushID, "CPF" : cpfcadastro, "Escola" : $rootScope.escolacadastro,
          "Endereços" : {"Default" : {"Endereço" : "Biblioteca Central (Entrada) - UnB ", "Local" : "Asa Norte"}}});
          dbRefAlunos.child($rootScope.uid).set({"Categoria" : $rootScope.userCat});

          //dbRefCadastroPermanente.child($rootScope.uid).set({"Nome" : $rootScope.nomecadastro, "Email" : $rootScope.emailcadastro, "Telefone" : $rootScope.phonecadastro});

          currentUser.updateProfile({
              displayName: $rootScope.nomecadastro
          }).then(function() {
              currentUser.sendEmailVerification();
          }, function(error) {

          });

          dataService.setProfessores();
          dataService.loadHorariosProfessores();
          agendamentoService.setAlunoKey($rootScope.uid);
          agendamentoService.setAlunoNome($rootScope.nomecadastro);

          firebase.auth().currentUser.getToken(/* forceRefresh */ true).then(function(idToken) {
            dataService.setAuthToken(idToken);
          }).catch(function(error) {
            
          });

          $ionicLoading.hide();

          var alertPopup = $ionicPopup.alert({
            title: 'Concluído!',
            template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Um link de verificação foi enviado para o seu e-mail, confirme sua conta para marcar aulas.</p></div></div>",
            cssClass: 'popcustom',
            okType: 'customok'
          });

          categoryService.setCat($rootScope.userCat);
          $rootScope.alunoaulasid = $rootScope.nomecadastro;
          $state.go('tabsController.aulas');
          return;
        });

        promise.catch(function(e) {
            if(e.code === "auth/email-already-in-use"){
              var alertPopup = $ionicPopup.alert({
               title: 'Atenção',
               template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Esse endereço de e-mail já está em uso.</p></div></div>",
               cssClass: 'popcustom',
               okType: 'customok'
             });
            }
            else{
              ionicToast.show('Esse endereço de e-mail já está em uso. Por favor, tente novamente.', 'middle', false, 2500);
            }
            $ionicLoading.hide();
            return;
        });


    };


})
