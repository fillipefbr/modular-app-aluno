angular.module('app.escolaCtrl', [])

//****************************_ESCOLA CONTROLLER******************************//

.controller('escolaCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, categoryService, ionicToast, $ionicPopup) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const dbRefTours = dbRefRoot.child('tours');
  const dbRefAlunosTour = dbRefRoot.child('alunos_tour');
  const auth = firebase.auth();

  $ionicModal.fromTemplateUrl('escola/escolamodal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

//----------------------------------------------------------------------------//
  $scope.$on("$ionicView.beforeEnter", function(){
    $scope.spinescola = true;
    var user = firebase.auth().currentUser;
    if (user) {
      dbRefAlunosPrivate.child(user.uid).once('value', function(snap) {
        $scope.escola = snap.val().Escola;
        if(typeof $scope.escola === 'undefined'){
          $scope.spinescola = false;
          $scope.nenhumaescola = true;
        }
        else{
          $scope.nenhumaescola = false;
          $scope.spinescola = false;
        }
      });
    }
  });
//----------------------------------------------------------------------------//
$scope.escolaSelect = function(code, escola) {
  var myPopup = $ionicPopup.show({
  template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-refresh-empty icon-pop'></i></div></div></div><p class='custom'>Você gostaria de definir <strong>" + escola + "</strong> como sua escola?</p></div></div>",
  title: "Escola",
  cssClass: 'popcustom',
  scope: $scope,
  buttons: [
    { text: 'Cancelar', type: 'customconfirm' },
    {
      text: '<b>Sim</b>',
      type: 'customconfirm',
      onTap: function(e) {
        var user = firebase.auth().currentUser;
        if(user){
          user = user.uid;
          dbRefAlunosPrivate.child(user).once('value', function(snap) {
            if(typeof snap.val().Escola != 'undefined'){
              window.plugins.OneSignal.sendTag(snap.val().Escola, "false");
            }
            dbRefAlunosPrivate.child(user).update({"Escola" : escola});
            dbRefAlunosPrivate.child(user).update({"Escola_Code" : code});
            window.plugins.OneSignal.sendTag(code, "true");
            var es = $document[0].getElementById("userescola");
            if(es != null){
              es.innerText = escola.substring(0, 15) + '...';
            }
            $scope.escola = escola;
            $scope.nenhumaescola = false;
            $scope.modal.hide();
            var alertPopup = $ionicPopup.alert({
             title: 'Concluído',
             template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Sua escola foi definida para <strong>" + escola +  "</strong></p></div></div>",
             cssClass: 'popcustom',
             okType: 'customok'
           });
          });
        }
      }
    }
  ]
  });
};

})
