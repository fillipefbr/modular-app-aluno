angular.module('app.agefavCtrl', [])

//************************_AGENDAMENTO FAV CONTROLLER*************************//

.controller('agefavCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, paymentService, categoryService, teacherService, ionicToast, favoritesService, $ionicPopup, agendamentoService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

  var local = "";
  var qtd_alunos = 1;

  $ionicModal.fromTemplateUrl('favoritos/disciplinasfav.html', {
    scope: $scope
  }).then(function(moddisfavl) {
    $scope.moddisfavl = moddisfavl;
  });

  $ionicModal.fromTemplateUrl('favoritos/locaisfav.html', {
    scope: $scope
  }).then(function(modlocfavl) {
    $scope.modlocfavl = modlocfavl;
  });

  $ionicModal.fromTemplateUrl('favoritos/codesfav.html', {
    scope: $scope
  }).then(function(modcodfavl) {
    $scope.modcodfavl = modcodfavl;
  });

  $ionicModal.fromTemplateUrl('favoritos/adressfav.html', {
    scope: $scope
  }).then(function(modadfavl) {
    $scope.modadfavl = modadfavl;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.moddisfavl != 'undefined'){
      $scope.moddisfavl.remove();
    }
    if(typeof $scope.modlocfavl != 'undefined'){
      $scope.modlocfavl.remove();
    }
    if(typeof $scope.modcodfavl != 'undefined'){
      $scope.modcodfavl.remove();
    }
    if(typeof $scope.modadfavl != 'undefined'){
      $scope.modadfavl.remove();
    }
  });

//----------------------------------------------------------------------------//
  $scope.$on("$ionicView.beforeEnter", function(){
    $scope.ageact(1);
    $scope.favpnome = teacherService.getProfFav();
    var user = firebase.auth().currentUser;
    if (user) {
      user = user.uid
      dbRefAlunosPrivate.child(user).child('Endereços').on('child_added', function(snap) {
        if(snap.val().Padrão === true){
          $scope.adfav = snap.val().Endereço;
          // added
          local = snap.val().Local;
        }
      });

      var cat = categoryService.getCat();
      categoryService.setCurrentCat(cat);
      var profkk = teacherService.getProfKeyFav();

      if(typeof $scope.facher != 'undefined' && categoryService.hasCatChanged()){
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner> <br/>',
          duration: '2000'
        });
        $scope.facher = [];
        if(typeof cat != 'undefined'){
          dbRefProfessores.child(profkk).child("Disciplinas").on('child_added', function(snop) {
            if(cat === snop.val().Categoria && snop.val().Nome != "Tour pela UnB"){
              dbRefDisciplinas.on('child_added', function(snap) {
                if(snop.val().Nome === snap.val().Nome && snop.val().Categoria === snap.val().Categoria){
                  $timeout(function() {
                    var fach = {"nome" : snop.val().Nome, "icon" : snap.val().Icon};
                    $scope.facher.push(fach);
                  }, 0);
                }
              });
            }
          });
          categoryService.changedCatFalse();
        }
      }
      else if(typeof $scope.facher != 'undefined'){

      }
      else{
        $scope.facher = [];
        if(typeof cat != 'undefined'){
          dbRefProfessores.child(profkk).child("Disciplinas").on('child_added', function(snop) {
            if(cat === snop.val().Categoria && snop.val().Nome != "Tour pela UnB"){
              dbRefDisciplinas.on('child_added', function(snap) {
                if(snop.val().Nome === snap.val().Nome && snop.val().Categoria === snap.val().Categoria){
                  $timeout(function() {
                    var fach = {"nome" : snop.val().Nome, "icon" : snap.val().Icon};
                    $scope.facher.push(fach);
                  }, 0);
                }
              });
            }
          });
        }
      }

      $scope.locais = [];
      dbRefProfessores.child(profkk).child("Locais").on('child_added', function(sloc) {
        $timeout(function() {
          var loc = {"nome" : sloc.val().Local};
          $scope.locais.push(loc);
        }, 0);
      });

      $scope.codes = [];
      $scope.nenhumCP = false;
      var dbRefA = dbRefAlunosPrivate.child(user);
      var dbRefC = dbRefA.child("Códigos_Promocionais");
      dbRefC.once('value', function(snap) {
        if(snap.val()){
          snap.forEach(function(child) {
            var cd = {"code" : child.val().Código, "desconto" : child.val().Desconto, "key" : child.key};
            $scope.codes.push(cd);
          });
        }
        else{
          $scope.nenhumCP = true;
        }
      });
      /*if($scope.codes.length === 0){
        $scope.nenhumCP = true;
      }*/

      $timeout(function() {
        if($scope.facher.length === 0){
          $scope.nenhumaFacher = true;
        }
        else{
          $scope.nenhumaFacher = false;
        }
      }, 1000);
    }
  });
//----------------------------------------------------------------------------//
  $scope.ageact = function(x) {
    switch (x) {
      case 1:
        agendamentoService.setAlunos(1);
        $scope.ageact1 = true;
        $scope.ageact2 = false;
        $scope.ageact3 = false;
        $scope.ageact4 = false;
        $scope.ageact5 = false;
        qtd_alunos = 1;
        break;
      case 2:
        agendamentoService.setAlunos(2);
        $scope.ageact1 = false;
        $scope.ageact2 = true;
        $scope.ageact3 = false;
        $scope.ageact4 = false;
        $scope.ageact5 = false;
        qtd_alunos = 2;
        break;
      case 3:
        agendamentoService.setAlunos(3);
        $scope.ageact1 = false;
        $scope.ageact2 = false;
        $scope.ageact3 = true;
        $scope.ageact4 = false;
        $scope.ageact5 = false;
        qtd_alunos = 3;
        break;
      case 4:
        agendamentoService.setAlunos(4);
        $scope.ageact1 = false;
        $scope.ageact2 = false;
        $scope.ageact3 = false;
        $scope.ageact4 = true;
        $scope.ageact5 = false;
        qtd_alunos = 4;
        break;
      case 5:
        agendamentoService.setAlunos(5);
        $scope.ageact1 = false;
        $scope.ageact2 = false;
        $scope.ageact3 = false;
        $scope.ageact4 = false;
        $scope.ageact5 = true;
        qtd_alunos = 5;
        break;
      default:
        agendamentoService.setAlunos(1);
        $scope.ageact1 = true;
        $scope.ageact2 = false;
        $scope.ageact3 = false;
        $scope.ageact4 = false;
        $scope.ageact5 = false;
        qtd_alunos = 1;
        break;
    }
  };
//----------------------------------------------------------------------------//
  $scope.escolheuDisFav = function(nome) {
    $scope.disfav = nome;
    $scope.moddisfavl.hide();
  };
//----------------------------------------------------------------------------//
  $scope.escolheuLoc = function(nome) {

    local = nome; // added
    $scope.ads = [];
    $scope.nenhumadfav = false;
    $scope.spinadfav = true;
    var profkk = teacherService.getProfKeyFav();

    $scope.modlocfavl.hide();
    $scope.modadfavl.show();

    var user = firebase.auth().currentUser;

    if (user) {
      user = user.uid
      var dbRefA = dbRefAlunosPrivate.child(user);
      var dbRefE = dbRefA.child("Endereços");
      var dbRefEP = dbRefProfessores.child(profkk).child("Locais");
      dbRefE.on('child_added', function(snae) {
        if(nome === snae.val().Local){
          var ad = {"local" : snae.val().Local, "endereco" : snae.val().Endereço, "key" : snae.key};
          $scope.ads.push(ad);
          $scope.spinadfav = false;
        }
      });
    }

    $timeout(function() {
      if($scope.ads.length === 0){
        $scope.spinadfav = false;
        $scope.nenhumadfav = true;
      }
    }, 2000);
  };
//----------------------------------------------------------------------------//
  $scope.escolheuAd = function(ad, kk){
    $scope.adfav = ad;
    $scope.modadfavl.hide();
  };
//----------------------------------------------------------------------------//
  $scope.escolheuCode = function(des, kk){
    $scope.cdfav = des + "% de desconto";
    agendamentoService.setPcd(des);
    $rootScope.cdPromocional = kk;
    $scope.modcodfavl.hide();
  }
//----------------------------------------------------------------------------//
  $scope.checkAgeFav = function(dis, ad, cd, cnt) {
    if(typeof dis === 'undefined'){
      ionicToast.show('Selecione uma disciplina', 'middle', false, 2500);
      return;
    }
    if(typeof ad === 'undefined'){
      ionicToast.show('Selecione um endereço', 'middle', false, 2500);
      return;
    }
    if(typeof cnt === 'undefined' || cnt.length === 0){
      ionicToast.show('Por favor, informe o conteúdo da aula', 'middle', false, 2500);
      return;
    }
    else{
      /*if(typeof cd === 'undefined'){
        agendamentoService.setPcd('Toque para escolher');
      }
      else{
        agendamentoService.setPcd(cd);
      }*/
      
      var arr = agendamentoService.getArrHorsFav();
      arr.local = local;
      arr.endereco = ad;
      arr.conteudo = cnt;
      arr.alunos = qtd_alunos;
      arr.disciplina = dis;
      arr.categoria = categoryService.getCurrentCat();
      if(typeof cd != 'undefined'){
        arr.desconto = agendamentoService.getPcd();
        agendamentoService.setPcd(cd);
        arr.cpkey = $rootScope.cdPromocional;
      }
      agendamentoService.setArrHorsFav(arr);
      
      agendamentoService.setPd(dis);
      agendamentoService.setPadress(ad);
      agendamentoService.setPconteudo(cnt);
      $state.go('tabsController.resumofav');
    }
  };


})
