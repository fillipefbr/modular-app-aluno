angular.module('app.calendariofavoritosCtrl', [])

//************************_CALENDARS CONTROLLER*******************************//

.controller('calendariofavoritosCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, categoryService, teacherService, ionicToast, agendamentoService) {

  $scope.data = {};

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

  $ionicModal.fromTemplateUrl('favoritos/diaFavDetalhes.html', {
    scope: $scope
  }).then(function(modol) {
    $scope.modol = modol;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.modol != 'undefined'){
      $scope.modol.remove();
    }
    if(typeof $scope.moddisfavl != 'undefined'){
      $scope.moddisfavl.remove();
    }
    if(typeof $scope.modlocfavl != 'undefined'){
      $scope.modlocfavl.remove();
    }
    if(typeof $scope.modcodfavl != 'undefined'){
      $scope.modcodfavl.remove();
    }
    if(typeof $scope.modadfavl != 'undefined'){
      $scope.modadfavl.remove();
    }
  });

//----------------------------------------------------------------------------//
  $scope.$on("$ionicView.beforeEnter", function(){
    var profkk = teacherService.getProfKeyFav();
    dbRefProfessores.child(profkk).once("value", function(snap) {
      $timeout(function() {
        $scope.favsrc = snap.val().Avatar;
        $scope.favpnome = snap.val().Nome;
      }, 0);

      var userProf;
      var day;
      var month;
      var year;
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1;
      var yyyy = today.getFullYear();

      $scope.onezoneDatepicker.highlights = [];

      $scope.onezoneDatepicker.disableDates = [];

      var dbRefProf = dbRefProfessores.child(profkk);
      var dbRefHors = dbRefHorariosProfessores.child(profkk);

       dbRefHors.on('child_added', function(snapshoot) {

         day = snapshoot.val().Dia;
         day = parseInt(day, 10);
         month = snapshoot.val().Mês;
         month = parseInt(month, 10);
         year = snapshoot.val().Ano;
         year = parseInt(year, 10);

         if(day < dd && month <= mm && year <= yyyy){
           dbRefHors.child(snapshoot.key).remove();
         }

         if(month >= mm && year >= yyyy){

           var h1 = {

             date: new Date(year, month - 1, day),
             color: '#00b300',
             textColor: '#fff'

           };

           $scope.onezoneDatepicker.highlights.push(h1);

         }

       });

      $scope.onezoneDatepicker.showDatepicker = true;
    });
  });

//----------------------------------------------------------------------------//
  var currentDate = new Date();
  var date = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
  $scope.date = date;

  $scope.selectedDate1;
  $scope.selectedDate2;

  $scope.dataCompleta = "";
  $scope.horaCompleta = "";

  var weekday = new Array(7);
  weekday[0]=  "Domingo";
  weekday[1] = "Segunda-Feira";
  weekday[2] = "Terça-Feira";
  weekday[3] = "Quarta-Feira";
  weekday[4] = "Quinta-Feira";
  weekday[5] = "Sexta-Feira";
  weekday[6] = "Sábado";


  var monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
    "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
  ];


$scope.onezoneDatepicker = {
      date: date,
      mondayFirst: true,
      months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
      daysOfTheWeek: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
      startDate: new Date(1989, 1, 26),
      endDate: new Date(2024, 1, 26),
      disablePastDays: true,
      disableSwipe: false,
      disableWeekend: false,
      //disableDates: [new Date(date.getFullYear(), date.getMonth(), 15), new Date(date.getFullYear(), date.getMonth(), 16), new Date(date.getFullYear(), date.getMonth(), 17)],
      showDatepicker: false,
      showTodayButton: true,
      calendarMode: true,
      hideCancelButton: false,
      hideSetButton: false,
      callback: //$scope.myFunction
        function() {

        } ,
      highlights: [

        ]

  };

  $scope.showDatepicker = function () {
      $scope.onezoneDatepicker.showDatepicker = true;
  };

//----------------------------------------------------------------------------//
  $scope.refreshHorarios = function() {

    var userProf;
    var day;
    var month;
    var year;

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();

    var prof = teacherService.getProfFav();

    $scope.onezoneDatepicker.highlights = [];
    $scope.onezoneDatepicker.disableDates = [];

    dbRefProfessores.on('child_added', function(snap) {

      if(prof === snap.val().Nome){

        var dbRefProf = dbRefProfessores.child(snap.key);
        var dbRefHors = dbRefHorariosProfessores.child(snap.key);

        $timeout(function() {
          var img = $document[0].getElementById('imgprofcircled');
          img.src = snap.val().Avatar;
        }, 500);

         dbRefHors.on('child_added', function(snapshoot) {

           day = snapshoot.val().Dia;
           day = parseInt(day, 10);
           month = snapshoot.val().Mês;
           month = parseInt(month, 10);
           year = snapshoot.val().Ano;
           year = parseInt(year, 10);

           if(day < dd && month <= mm && year <= yyyy){
             dbRefHors.child(snapshoot.key).remove();
           }

           if(month >= mm && year >= yyyy){

             var h1 = {

               date: new Date(year, month - 1, day),
               color: '#00b300',
               textColor: '#fff'

             };

             $scope.onezoneDatepicker.highlights.push(h1);

           }

         });

      }

    });

    $scope.onezoneDatepicker.showDatepicker = true;
    $scope.$broadcast('scroll.refreshComplete');

  };
//----------------------------------------------------------------------------//
  /*$scope.onezoneDatepicker.callback = function() {

    $scope.aulas = [];

    var dbRefP;
    var dbRefH;
    var profavatar;
    var profh2;
    var profcurs;
    var mensagem;
    var key;
    var puser;
    var puserkey;
    $scope.data.nenhumhorario = false;
    $scope.data.spinhor = true;
    var achou = false;

    var professor = teacherService.getProfFav();
    var professorkey = teacherService.getProfKeyFav();

    dbRefP = dbRefProfessores.child(professorkey);
    dbRefH = dbRefHorariosProfessores.child(professorkey);

    dbRefProfessores.on('child_added', function(snap) {

      if(professor === snap.val().Nome){

        profavatar = snap.val().Avatar;
        profh2 = snap.val().Nome;
        profcurs = snap.val().Curso;
        puser = snap.val().User;
        puserkey = snap.key;

      }
    });


    var dia = $scope.onezoneDatepicker.date.getDate();
    var mes = $scope.onezoneDatepicker.date.getMonth();

    var diaString = dia.toString();
    var mesString = mes + 1;
    mesString = mesString.toString();

    $scope.diaescolhido = $scope.onezoneDatepicker.date.getDate();
    $scope.dayescolhido = weekday[$scope.onezoneDatepicker.date.getDay()];
    $scope.mesescolhido = monthNames[$scope.onezoneDatepicker.date.getMonth()];
    $scope.mesescolhido3 = $scope.onezoneDatepicker.date.getMonth() + 1;
    var anoescolhido = $scope.onezoneDatepicker.date.getFullYear();

    $scope.data.dataEscolhida = $scope.diaescolhido + " de " +  $scope.mesescolhido;

    $scope.navtitlefav = $scope.data.dataEscolhida;
    var mesint = parseInt($scope.mesescolhido3, 10);
    var diaint = parseInt($scope.diaescolhido, 10);
    if(diaint < 10){
      $scope.diaescolhido = "0" + $scope.diaescolhido;
    }

    if(mesint < 10){
      $scope.mesescolhido3 = "0" + $scope.mesescolhido3;
    }

    $scope.datadiacirclehorafav = $scope.diaescolhido + "/" + $scope.mesescolhido3;
    $scope.diacirclehorafav = $scope.dayescolhido;
    var add = false;
    var novoModal;

    dbRefH.on('child_added', function(snaap) {

      if(diaString === snaap.val().Dia && mesString === snaap.val().Mês){

        var horaInt = parseInt(snaap.val().Hora, 10);
        var minutosInt = parseInt(snaap.val().Minutos, 10);

        horaInt++;
        minutosInt += 40;

        if(minutosInt >= 60){
          horaInt++;
          minutosInt -= 60;
        }

        horaInt = horaInt.toString();
        minutosInt = minutosInt.toString();

        var now = new Date();
        if(now.getDate().toString() === diaString && mesString === (now.getMonth() + 1).toString() && parseInt(snaap.val().Hora, 10) <= now.getHours()){

        }

        else{
          mensagem = snaap.val().Hora + ":" + snaap.val().Minutos + " às " + horaInt + ":" + minutosInt;
          key = snaap.key;
          var aula = {"hora" : mensagem, "prof" : puserkey, "key" : key};
          $scope.aulas.push(aula);
          $scope.data.spinhor = false;
          achou = true;
        }

     }

   });

   $scope.modol.show();

    $timeout(function() {

      if(achou === false){
        $scope.data.spinhor = false;
        $scope.data.nenhumhorario = true;
      }

    }, 2000);


  };*/

$scope.onezoneDatepicker.callback = function() {

    $scope.aulas = [];

    var dbRefP;
    var dbRefH;
    var profavatar;
    var profh2;
    var profcurs;
    var mensagem;
    var key;
    var puserkey;
    $scope.data.nenhumhorario = false;
    $scope.data.spinhor = true;
    var achou = false;
    var temp;
    var tempdia;

    var professor = teacherService.getProfFav();
    var profkey = teacherService.getProfKeyFav();
    var profpush = teacherService.getProfPushFav(); // Added
    dbRefP = dbRefProfessores.child(profkey);
    dbRefH = dbRefHorariosProfessores.child(profkey);

    dbRefP.once('value', function(snap) {

      profavatar = snap.val().Avatar;
      profh2 = snap.val().Nome;
      profcurs = snap.val().Curso;
      puserkey = snap.key;

    });


    var dia = $scope.onezoneDatepicker.date.getDate();
    var mes = $scope.onezoneDatepicker.date.getMonth();

    var diaString = dia.toString();
    var mesString = mes + 1;
    mesString = mesString.toString();

    $scope.diaescolhido = $scope.onezoneDatepicker.date.getDate();
    $scope.dayescolhido = weekday[$scope.onezoneDatepicker.date.getDay()];
    $scope.mesescolhido = monthNames[$scope.onezoneDatepicker.date.getMonth()];
    $scope.mesescolhido3 = $scope.onezoneDatepicker.date.getMonth() + 1;
    var anoescolhido = $scope.onezoneDatepicker.date.getFullYear();
    $scope.data.dataEscolhida = $scope.diaescolhido + " de " +  $scope.mesescolhido;
    $scope.navtitlefav = $scope.data.dataEscolhida;
    var mesint = parseInt($scope.mesescolhido3, 10);
    var diaint = parseInt($scope.diaescolhido, 10);
    if(diaint < 10){
      $scope.diaescolhido = "0" + $scope.diaescolhido;
    }

    if(mesint < 10){
      $scope.mesescolhido3 = "0" + $scope.mesescolhido3;
    }

    $scope.datadiacirclehorafav = $scope.diaescolhido + "/" + $scope.mesescolhido3;
    $scope.diacirclehorafav = $scope.dayescolhido;
    var add = false;
    var novoModal;

    dbRefH.on('child_added', function(snaap) {

      if(diaString === snaap.val().Dia && mesString === snaap.val().Mês){
        var horaInt = parseInt(snaap.val().Hora, 10);
        var minutosInt = parseInt(snaap.val().Minutos, 10);
        horaInt++;
        minutosInt += 40;
        if(minutosInt >= 60){
          horaInt++;
          minutosInt -= 60;
        }

        //added
        if(minutosInt < 10){
          minutosInt = minutosInt.toString();
          minutosInt = "0" + minutosInt;
        }

        horaInt = horaInt.toString();
        minutosInt = minutosInt.toString();

        var phora = snaap.val().Hora + ":" + snaap.val().Minutos;
        var pminutos = snaap.val().Minutos;
        temp = snaap.val().Mês;
        temp = parseInt(temp, 10);
        if(temp < 10){
          temp = temp.toString();
          temp = "0" + temp;
        }
        else{
          temp = temp.toString();
        }
        tempdia = snaap.val().Dia;
        tempdia = parseInt(tempdia, 10);
        if(tempdia < 10){
          tempdia = tempdia.toString();
          tempdia = "0" + tempdia;
        }
        else{
          tempdia = tempdia.toString();
        }
        var pdata = tempdia + "/" + temp + " às " + phora;

        var now = new Date();
        if(now.getDate().toString() === diaString && mesString === (now.getMonth() + 1).toString() && parseInt(snaap.val().Hora, 10) <= now.getHours()){

        }

        else{
          mensagem = snaap.val().Hora + ":" + snaap.val().Minutos + " às " + horaInt + ":" + minutosInt;
          key = snaap.key;
          var aula = {"hora" : mensagem, "prof" : profkey, "key" : key, "data" : snaap.val().Dia + '/' + snaap.val().Mês + '/' + snaap.val().Ano, "professor" : professor, "ano" : snaap.val().Ano, "dia" : snaap.val().Dia, "mes" : snaap.val().Mês, "horares" : snaap.val().Hora, "pdata" : pdata, "phora" : phora, "profpush" : profpush, "pminutos" : pminutos};
          $scope.aulas.push(aula);
          $scope.data.spinhor = false;
          achou = true;
        }
     }
   });

   $scope.modol.show();

    $timeout(function() {
      if(achou === false){
        $scope.data.spinhor = false;
        $scope.data.nenhumhorario = true;
      }
    }, 2000);

  };

//----------------------------------------------------------------------------//
  /*$scope.escolheuAula = function(prof, key) {

    var dbRefP;
    var dbRefH;
    var profavatar;
    var profh2;
    var profcurs;

    var professor = teacherService.getProfFav();
    agendamentoService.setKey(key); 

    dbRefProfessores.on('child_added', function(snap) {

      if(prof === snap.key){
        profavatar = snap.val().Avatar;
        profh2 = snap.val().Nome;
        profcurs = snap.val().Curso;
        qtdaulasleher = snap.val().Aulas;
      }
    });

    var user = firebase.auth().currentUser;

    if (user) {
      user = user.uid;
    }
    $scope.modol.hide();
    $state.go('tabsController.agendamentofav');

  };*/

  $scope.escolheuAula = function(prof, key, data, hora, professor, ano, mes, dia, horares, pdata, phora, profpush, pminutos) {

    var user = agendamentoService.getAlunoKey();
    var nomeAluno = agendamentoService.getAlunoNome();
    var res = {"key" : key, "prof" : prof, "data" : data, "hora" : hora, "professor" : professor, "ano" : ano, "mes" : mes, "dia" : dia, "horares" : horares, "pdata" : pdata, "phora" : phora, "profpush" : profpush, "user" : user, "cliente" : nomeAluno, "pminutos" : pminutos};
    agendamentoService.setArrHorsFav(res);
    $scope.modol.hide();
    $state.go('tabsController.agendamentofav');

    
  };

})
