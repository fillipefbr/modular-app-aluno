angular.module('app.boletoFavCtrl', [])

//***************************_BOLETO FAV CONTROLLER***************************//

.controller('boletoFavCtrl', function($scope, $rootScope, $document, $ionicLoading, $timeout, $state, $ionicHistory, $ionicPlatform, paymentService, categoryService, teacherService, ionicToast, favoritesService, $ionicPopup, agendamentoService, dataService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
//----------------------------------------------------------------------------//
  $scope.$on("$ionicView.beforeEnter", function(){
    var user = firebase.auth().currentUser;
    if (user) {
      $ionicLoading.show({
        template: "<ion-spinner icon='ios-small'></ion-spinner><br/><p class='custom'>Carregando</p>",
        duration: 2000
      });
      user = user.uid;
      $timeout(function() {
        var dbRefA = dbRefAlunosPrivate.child(user);
        dbRefA.on('value', function(snap) {
          $timeout(function() {
            $scope.boletonome = snap.val().Nome;
            if(snap.val().CPF != "Não informado"){
              $scope.boletocpf = parseInt(snap.val().CPF, 10);
            }
            $scope.boletoemail = snap.val().Email;
            if(snap.val().Aniversário != "Não informado"){
              //$scope.boletonascimento = snap.val().Aniversário;
              var stringniver = snap.val().Aniversário;
              var arraydata = stringniver.split("-");
              var ano = parseInt(arraydata[0], 10);
              var mes = parseInt(arraydata[1], 10);
              var dia = parseInt(arraydata[2], 10);
              $scope.boletonascimento = new Date(ano, mes - 1, dia);
            }
            $scope.boletotelefone = snap.val().Telefone;
          }, 0);
        });
      }, 1000);
    }
  });

//----------------------------------------------------------------------------//
  $scope.confirmarAulaBoleto = function(name, cpf, email, birth, phone_number) {

    name = name.toString();
    name = name.replace(/[0-9]/g,'');
    if(/^[ ]*(.+[ ]+)+.+[ ]*$/.test(name) === false){
      ionicToast.show('Nome inválido', 'middle', false, 3000);
      return;
    }
    cpf = cpf.toString();
    cpf = cpf.replace(/\D/g,'');
    if(cpf.length != 11){
      ionicToast.show('CPF inválido', 'middle', false, 2500);
      return;
    }
    phone_number = phone_number.toString();
    phone_number = phone_number.replace(/\D/g,'');
    if(/^[1-9]{2}9?[0-9]{8}$/.test(phone_number) === false){
      ionicToast.show('Telefone inválido', 'middle', false, 2500);
      return;
    }
    var yearbirth = birth.getFullYear();
    var monthbirth = birth.getMonth() + 1;
    if(monthbirth < 10){
      monthbirth = "0" + monthbirth;
    }
    var datebirth = birth.getDate();
    if(datebirth < 10){
      datebirth = "0" + datebirth;
    }
    birth = yearbirth + "-" + monthbirth + "-" + datebirth;

    var user = agendamentoService.getAlunoKey();

    if (user) {

      var datas = [];
      var child = agendamentoService.getArrHorsFav();

      if(typeof $rootScope.pushID === 'undefined'){
        $ionicPlatform.ready(function() {
           window.plugins.OneSignal.getIds(function(ids) {
             $rootScope.pushID = ids.userId;
           });
         });
      }

      if(typeof $rootScope.pushID != 'undefined'){
        child.pushid = $rootScope.pushID;
      }

      child.name = name;
      child.cpf = cpf;
      child.email = email;
      child.birth = birth;
      child.phone_number = phone_number;
      datas.push(child);
      var authToken = dataService.getAuthToken();
      
      var dbRefAlu = dbRefAlunos.child(user);
      var nomeAluno = agendamentoService.getAlunoNome();
      var charge_id;

      dbRefAlunosPrivate.child(user).update({"CPF" : cpf});
      dbRefAlunosPrivate.child(user).update({"Aniversário" : birth});

      var presumovalor2 = agendamentoService.getPresumovalor2();
      presumovalor2 = parseInt(presumovalor2, 10);
      var aula = "Modular Aulas";

      $ionicLoading.show({
        template: "<ion-spinner icon='ios-small'></ion-spinner><br><p class='custom'>Marcando<br>aula</p>"
      });

      agendamentoService.marcarAulaBoleto(datas).then(function successCallback(response) {

          var resp = response.data;
          resp.forEach(function(child) {

            paymentService.createTransaction(aula, parseInt(child.Valor, 10), authToken).then(function(respo) {
              charge_id = respo.data.data.charge_id;
              dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"Charge_ID" : charge_id});
              dbRefAulasAlunos.child(child.AlunoID).child(child.KeyPush).update({"Charge_ID" : charge_id});
            });

            notificationsService.remindTeacherNotification(child.ProfPush, "Você tem uma aula hoje às " + child.HoraPush + " horas", child.DataPush).then(function(res) {
              notificationprofessorID = res.data.id;
              dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"NotificationProfessor_ID" : notificationprofessorID});
            }, function(error) {
              //ionicToast.show('Erro ao enviar notificação.', 'middle', true, 2500);
            });
            notificationsService.newClassNotification(child.ProfPush, "Você tem uma nova aula marcada para o dia " + child.Data + " horas").then(function(re) {
              
            },
            function(e) {
              //ionicToast.show('Erro ao mandar notificação para o professor.', 'middle', true, 2500);
            });

          });

          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go("tabsController.aulas");

          $timeout(function() {
             var spun = $document[0].getElementById('spanaulas');
             if(spun != null){
               var spaan = spun.innerText;
               spaan = parseInt(spaan, 10);
               spaan = spaan + 1;
               spaan = spaan.toString();
               spun.innerText = spaan;
             }
             
             var alertPopup = $ionicPopup.alert({
               title: 'Concluído',
               template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Aula marcada!</p></div></div>",
               cssClass: 'popcustom',
               okType: 'customok'
             });
            $ionicHistory.clearCache();
            $ionicLoading.hide();

          }, 1000);

        }, function errorCallback(error) {
          var alertPopup = $ionicPopup.alert({
             title: 'Ooops!',
             template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>Ocorreu um erro! Por favor, tente novamente.</p></div></div>",
             cssClass: 'popcustom',
             okType: 'customok'
           });
          $ionicLoading.hide();

        });

    }

    else{
      $ionicLoading.hide();
      ionicToast.show('É necessário verificar seu e-mail antes de marcar uma aula.', 'middle', true, 2500);
      return;
    }

  };

})
