angular.module('app.addfavoritosCtrl', [])

//**************************_FAVORITOS CONTROLLER*****************************//

.controller('addfavoritosCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, ionicToast, favoritesService, ConnectivityMonitor, dataService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

  var professores = [];

  $ionicModal.fromTemplateUrl('favoritos/addproffavdetalhes.html', {
    scope: $scope
  }).then(function(model) {
    $scope.model = model;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.model != 'undefined'){
      $scope.model.remove();
    }
  });

  $scope.$on("$ionicView.beforeEnter", function(){
    $scope.localfavselected = (favoritesService.getLoc()).toUpperCase();
    professores = dataService.getProfessores();
    if(typeof $scope.lehrer === 'undefined'){
      $scope.lehrer = [];
      $scope.data.spinprofs = true;
      $scope.data.hideprofsmsg = true;
      $scope.qtdlehrer = 0;
    }
  });

  $scope.$on("$ionicView.enter", function(){

    var p = categoryService.getCat();
    var l = favoritesService.getLoc();
    var ntemprof = true;
    var loc;
    var cat;
    var locai = [];
    var categ = [];
    var localbool = false;

    for(var i = 0; i < professores.length; i++){
       locai = [];
       categ = [];
       localbool = false;
       if(professores[i].Autorizado === true){
          loc = professores[i].Locais;

          for(var item in loc){
            locai.push(loc[item]);
          }

          locai.forEach(function(child) {
            if(l === child.Local){
              localbool = true;
            }
          });

          if(localbool){
            var ava = professores[i].Avaliações;
            var valorAva = professores[i].Avaliações_Valor;
            ava = parseInt(ava, 10);
            valorAva = parseInt(valorAva, 10);
            if(ava != 0){
              var resultado = valorAva / ava;
              resultado = resultado.toFixed(1);
              rat = resultado;
            }
            else{
              rat = 0;
            }

           if(professores[i].Cadastro){
               var cds = professores[i].Cadastro.substring(0, 5);
             }
             if(typeof cds === 'undefined'){
               cds = "17/03";
             }
             var pp = {"nome" : professores[i].Nome, "curso" : professores[i].Curso, "avatar" : professores[i].Avatar,  "descricao" : professores[i].Descrição, "avaliacoes" : professores[i].Avaliações, "rating" : rat,
             "key" : professores[i].$id, "cadastro" : cds, "profpush" : professores[i].Push_ID};
             $scope.lehrer.push(pp);
             $scope.data.spinprofs = false;
             $scope.data.hideprofsmsg = true;
             ntemprof = false;
          }

        }
    }


    /*if(typeof $scope.lehrer === 'undefined'){
      $scope.lehrer = [];
      $scope.staroutline = [];
      $scope.starhalf = [];
      $scope.starfull = [];
      $scope.data.spinprofs = true;
      $scope.data.hideprofsmsg = false;
      $scope.qtdlehrer = 0;
    }

    var ntemprof = true;

    dbRefProfessores.on('child_added', function(snap) {

      dbRefProfessoresAutorizados.child(snap.key).once('value', function(sniip) {

          if(sniip.val()){
            var user = firebase.auth().currentUser;

            if (user) {
              user = user.uid;
            }

            var dbRefProf = dbRefProfessores.child(snap.key);
            var dbRefDi = dbRefProf.child('Disciplinas');
            var dbRefLo = dbRefProf.child('Locais');
            var dbRefCa = dbRefProf.child('Categorias');
            var local = false;
            var disciplina = false;
            var categoria = false;
            var rat;
            var f1 = false;
            var f2 = false;
            var f3 = false;
            var f4 = false;
            var f5 = false;
            var h1 = false;
            var o1 = false;
            var arrayStar = [];
            arrayStar.push(f1, f2, f3, f4, f5, h1, o1);
            var counter = 0;

            dbRefLo.on('child_added', function(snop) {
              if(favoritesService.getLoc() === snop.val().Local){
                local = true;
              }
            });

            dbRefCa.on('child_added', function(snup) {
              if($rootScope.categoriaAluno === snup.val()){
                categoria = true;
              }
            });

            if(local && categoria){

                var ava = snap.val().Avaliações;
                var valorAva = snap.val().Avaliações_Valor;
                ava = parseInt(ava, 10);
                valorAva = parseInt(valorAva, 10);

                if(ava != 0){
                  var resultado = valorAva / ava;
                  resultado = resultado.toFixed(1);
                }

                if(ava === 0){
                  rat = "0.0";
                  o1 = true;
                }

                else{

                  if(resultado % 1 != 0){
                    rat = resultado;
                    resultado = Math.floor(resultado);
                    for(var i = 0; i < resultado; i++){
                      arrayStar[i] = true;
                      counter += 1;
                    }
                    if(counter === 1){
                      f1 = true;
                    }
                    if(counter === 2){
                      f1 = true;
                      f2 = true;
                    }
                    if(counter === 3){
                      f1 = true;
                      f2 = true;
                      f3 = true;
                    }
                    if(counter === 4){
                      f1 = true;
                      f2 = true;
                      f3 = true;
                      f4 = true;
                    }
                    if(counter === 5){
                      f1 = true;
                      f2 = true;
                      f3 = true;
                      f4 = true;
                      f5 = true;
                    }

                    h1 = true;

                  }

                  else{
                    rat = resultado;
                    for(var i = 0; i < resultado; i++){
                      arrayStar[i] = true;
                      counter += 1;
                    }

                    if(counter === 1){
                      f1 = true;
                    }
                    if(counter === 2){
                      f1 = true;
                      f2 = true;
                    }
                    if(counter === 3){
                      f1 = true;
                      f2 = true;
                      f3 = true;
                    }
                    if(counter === 4){
                      f1 = true;
                      f2 = true;
                      f3 = true;
                      f4 = true;
                    }
                    if(counter === 5){
                      f1 = true;
                      f2 = true;
                      f3 = true;
                      f4 = true;
                      f5 = true;
                    }
                  }
                }

               $timeout(function() {
                 if(snap.val().Cadastro){
                   var cds = snap.val().Cadastro.substring(0, 5);
                 }
                 if(typeof cds === 'undefined'){
                   var cds = "17/03";
                 }
                 var pp = {"nome" : snap.val().Nome, "curso" : snap.val().Curso, "avatar" : snap.val().Avatar,  "descricao" : snap.val().Descrição, "avaliacoes" : snap.val().Avaliações, "rating" : rat,
                 "full1" : f1, "full2" : f2, "full3" : f3, "full4" : f4, "full5" : f5, "half1" : h1, "outline1" : o1, "key" : snap.key, "cadastro" : cds};
                 $scope.lehrer.push(pp);
                 $scope.data.spinprofs = false;
                 ntemprof = false;
               }, 0);
            }
          }
      });
    });*/

    $timeout(function() {
      if(ntemprof && ConnectivityMonitor.isOnline()){
        $scope.data.spinprofs = false;
        $scope.data.hideprofsmsg = false;
      }
      else if(ConnectivityMonitor.isOffline()){
        ionicToast.show('Sem conexão com a internet', 'middle', false, 2500);
        $scope.data.spinprofs = false;
      }
    }, 4000);

  });
//----------------------------------------------------------------------------//
  /*$scope.escolheuProf = function(x,y,w,z,a,c,f1,f2,f3,f4,f5,h1,o1,r,k,cds) {

      $scope.jafav = true;
      favoritesService.setProf(k);

      var user = firebase.auth().currentUser;
      if (user) {
        user = user.uid;
        dbRefAlunosPrivate.child(user).child("Favoritos").on('child_added', function(snap) {
          if(snap.key === k){
            $scope.jafav = false;
          }
        });
      }

      $scope.proffull1 = false;
      $scope.proffull2 = false;
      $scope.proffull3 = false;
      $scope.proffull4 = false;
      $scope.proffull5 = false;
      $scope.profhalf1 = false;
      $scope.profoutline1 = false;

      $rootScope.starfull1 = false;
      $rootScope.starfull2 = false;
      $rootScope.starfull3 = false;
      $rootScope.starfull4 = false;
      $rootScope.starfull5 = false;
      $rootScope.staroutline1 = false;
      $rootScope.starhalf1 = false;
      $rootScope.starrating = "0.0";

      $scope.profNome = x;
      $scope.profPro = "Estudante";
      $scope.profCurso = y;

      $scope.profratingaddproffav = r;
      $rootScope.starrating = r;
      if(f1){
        $scope.proffull1 = true;
        $rootScope.starfull1 = true;
      }
      if(f2){
        $scope.proffull2 = true;
        $rootScope.starfull2 = true;
      }
      if(f3){
        $scope.proffull3 = true;
        $rootScope.starfull3 = true;
      }
      if(f4){
        $scope.proffull4 = true;
        $rootScope.starfull4 = true;
      }
      if(f5){
        $scope.proffull5 = true;
        $rootScope.starfull5 = true;
      }
      if(h1){
        $scope.profhalf1 = true;
        $rootScope.starhalf1 = true;
      }
      if(o1){
        $scope.profoutline1 = true;
        $rootScope.staroutline1 = true;
      }

      $scope.nomeperfilprofaddproffav = x;
      $scope.cursoperfilprofaddproffav = y;
      $scope.descricaoperfilprofaddproffav = z;
      $scope.imgfullprofaddproffav = w;
      $scope.avaliacoesperfilprofaddproffav = a;
      $scope.cadastroperfilprofaddproffav = cds;
      $scope.model.show();
  };*/
//----------------------------------------------------------------------------//
  $scope.escolheuProf = function(x,y,w,z,a,r,k,cds) {

      $scope.jafav = true;
      favoritesService.setProf(k);

      var user = firebase.auth().currentUser;
      if (user) {
        user = user.uid;
        dbRefAlunosPrivate.child(user).child("Favoritos").on('child_added', function(snap) {
          if(snap.key === k){
            $scope.jafav = false;
          }
        });
      }

      $scope.profNome = x;
      $scope.profPro = "Estudante";
      $scope.profCurso = y;

      $scope.profratingaddproffav = r;
      $rootScope.starrating = r;

      $scope.nomeperfilprofaddproffav = x;
      $scope.cursoperfilprofaddproffav = y;
      $scope.descricaoperfilprofaddproffav = z;
      $scope.imgfullprofaddproffav = w;
      $scope.avaliacoesperfilprofaddproffav = a;
      $scope.cadastroperfilprofaddproffav = cds;
      $scope.model.show();
  };

//----------------------------------------------------------------------------//

$scope.addFav = function() {
  var prof = favoritesService.getProf();
  var user = firebase.auth().currentUser;
  if (user) {
    user = user.uid;
    dbRefAlunosPrivate.child(user).child("Favoritos").child(prof).set(true);
    $scope.model.hide();
    ionicToast.show('Adicionado aos favoritos', 'middle', false, 2500);
    $state.go('tabsController.favoritos');
  }
};

})
