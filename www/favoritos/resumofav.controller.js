angular.module('app.resumoFavCtrl', [])

//***************************_RESUMO FAV CONTROLLER***************************//

.controller('resumoFavCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, paymentService, categoryService, teacherService, ionicToast, favoritesService, $ionicPopup, agendamentoService, dataService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

  $ionicModal.fromTemplateUrl('favoritos/resumofavmodal.html', {
    scope: $scope
  }).then(function(modpdl) {
    $scope.modpdl = modpdl;
  });

  $ionicModal.fromTemplateUrl('favoritos/cardsfavmodal.html', {
    scope: $scope
  }).then(function(modcardsl) {
    $scope.modcardsl = modcardsl;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.modpdl != 'undefined'){
      $scope.modpdl.remove();
    }
    if(typeof $scope.modcardsl != 'undefined'){
      $scope.modcardsl.remove();
    }
  });

  $scope.$on("$ionicView.beforeEnter", function(){

    var arr = agendamentoService.getArrHorsFav();
    var has_pkt = false;
    
    dbRefProfessores.child(arr.prof).once('value', function(snap) {

      $scope.avatarprofresumo = snap.val().Avatar;
      $scope.h2profresumo = snap.val().Nome;
      $scope.pprofcursoresumo = snap.val().Curso;

    });

    var user = firebase.auth().currentUser;

    if (user) {
      user = user.uid;
    }

    $timeout(function() {

      var presumovalor2;
      var temp;
      var tempdia;
      
      $scope.pd = arr.disciplina;

      if(arr.endereco.length > 17){
        $scope.padress = arr.endereco.substring(0, 17) + "...";
      }
      else{
        $scope.padress = arr.endereco;
      }
    
      
      $scope.pconteudo = arr.conteudo;
      
      $scope.palunos = (arr.alunos).toString();

      if(categoryService.getCurrentCat() === "Fundamental"){
        $scope.pcat = "Ensino Fundamental";
      }

      else if(categoryService.getCurrentCat() === "Médio"){
        $scope.pcat = "Ensino Médio";
      }

      else if(categoryService.getCurrentCat() === "Vestibular"){
        $scope.pcat = "Pré-Vestibular";
      }

      else{
        $scope.pcat = categoryService.getCurrentCat();
      }
      
      $scope.phora = arr.phora;
      
      temp = arr.mes;
      temp = parseInt(temp, 10);
      if(temp < 10){
        temp = temp.toString();
        temp = "0" + temp;
      }
      else{
        temp = temp.toString();
      }

      tempdia = arr.dia;
      tempdia = parseInt(tempdia, 10);
      if(tempdia < 10){
        tempdia = tempdia.toString();
        tempdia = "0" + tempdia;
      }
      else{
        tempdia = tempdia.toString();
      }
      
      $scope.pdata = arr.pdata;

      dbRefAlunosPrivate.child(user).child("Pacotes").once('value', function(snap) {
        if(snap.val() != null){
          snap.forEach(function(child) {
            if(child.val().Categoria === categoryService.getCurrentCat() && child.val().Aulas > 0){
              paymentService.setFree();
              $scope.pvalor = "R$ 0,00";
              arr.valor = "0000";
              agendamentoService.setPvalor($scope.pvalor);
              presumovalor2 = "0000";
              agendamentoService.setPresumovalor2(presumovalor2);
              agendamentoService.setPkt(child.val().Pacote);
              has_pkt = true;
            }
          });
          if(!has_pkt) {
            paymentService.resetFree();
            dbRefDisciplinas.on('child_added', function(snap) {
              if(arr.disciplina === snap.val().Nome && categoryService.getCurrentCat() === snap.val().Categoria){
                if($scope.palunos === "1"){
                  if(typeof agendamentoService.getPcd() != 'undefined'){
                    var desconto = parseInt(agendamentoService.getPcd(), 10);
                    var valorBruto = parseInt(snap.val().Valor, 10);
                    var descontado = valorBruto - (desconto * valorBruto / 100);
                    arr.valor = descontado.toString();
                    descontado = descontado / 100;
                    descontado = descontado.toFixed(2);
                    var resultado = descontado.toString().replace(".", ",");
                    $scope.pvalor = "R$ " + resultado;
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2 = resultado.replace(",", "");
                    agendamentoService.setPresumovalor2(presumovalor2);
                  }
                  else{
                    var x = parseInt(snap.val().Valor, 10);
                    x = x/100;
                    x = x.toFixed(2);
                    $scope.pvalor = "R$ " +  x.toString().replace(".", ",");
                    agendamentoService.setPvalor($scope.pvalor);
                    arr.valor = snap.val().Valor;
                    presumovalor2 = snap.val().Valor;
                    agendamentoService.setPresumovalor2(presumovalor2);
                  }
                }
                else if($scope.palunos === "2"){
                  var val = parseInt(snap.val().Valor,10);
                  val = val*2 - 4000;
                  if(typeof agendamentoService.getPcd() != 'undefined'){
                    var desconto = parseInt(agendamentoService.getPcd(), 10);
                    var descontado = val - (desconto * val / 100);
                    arr.valor = descontado.toString();
                    descontado = descontado / 100;
                    descontado = descontado.toFixed(2);
                    var resultado = descontado.toString().replace(".", ",");
                    $scope.pvalor = "R$ " + resultado;
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2 = resultado.replace(",", "");
                    agendamentoService.setPresumovalor2(presumovalor2);
                  }
                  else{
                    var val_reais = val / 100;
                    val_reais = val_reais.toFixed(2);
                    $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2 = val.toString();
                    arr.valor = val.toString();
                    agendamentoService.setPresumovalor2(presumovalor2);
                  }
                }
                else if($scope.palunos === "3"){
                  var val = parseInt(snap.val().Valor,10);
                  val = val*3 - 6000;
                  if(typeof agendamentoService.getPcd() != 'undefined'){
                    var desconto = parseInt(agendamentoService.getPcd(), 10);
                    var descontado = val - (desconto * val / 100);
                    arr.valor = descontado.toString();
                    descontado = descontado / 100;
                    descontado = descontado.toFixed(2);
                    var resultado = descontado.toString().replace(".", ",");
                    $scope.pvalor = "R$ " + resultado;
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2 = resultado.replace(",", "");
                    agendamentoService.setPresumovalor2(presumovalor2);
                  }
                  else{
                    var val_reais = val / 100;
                    val_reais = val_reais.toFixed(2);
                    $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2 = val.toString();
                    arr.valor = val.toString();
                    agendamentoService.setPresumovalor2(presumovalor2);
                  }
                }
                else if($scope.palunos === "4"){
                  var val = parseInt(snap.val().Valor,10);
                  val = val*4 - 8000;
                  if(typeof agendamentoService.getPcd() != 'undefined'){
                    var desconto = parseInt(agendamentoService.getPcd(), 10);
                    var descontado = val - (desconto * val / 100);
                    arr.valor = descontado.toString();
                    descontado = descontado / 100;
                    descontado = descontado.toFixed(2);
                    var resultado = descontado.toString().replace(".", ",");
                    $scope.pvalor = "R$ " + resultado;
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2 = resultado.replace(",", "");
                    agendamentoService.setPresumovalor2(presumovalor2);
                  }
                  else{
                    var val_reais = val / 100;
                    val_reais = val_reais.toFixed(2);
                    $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2 = val.toString();
                    arr.valor = val.toString();
                    agendamentoService.setPresumovalor2(presumovalor2);
                  }
                }
                else if($scope.palunos === "5"){
                  var val = parseInt(snap.val().Valor,10);
                  val = val*5 - 10000;
                  if(typeof agendamentoService.getPcd() != 'undefined'){
                    var desconto = parseInt(agendamentoService.getPcd(), 10);
                    var descontado = val - (desconto * val / 100);
                    arr.valor = descontado.toString();
                    descontado = descontado / 100;
                    descontado = descontado.toFixed(2);
                    var resultado = descontado.toString().replace(".", ",");
                    $scope.pvalor = "R$ " + resultado;
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2 = resultado.replace(",", "");
                    agendamentoService.setPresumovalor2(presumovalor2);
                  }
                  else{
                    var val_reais = val / 100;
                    val_reais = val_reais.toFixed(2);
                    $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2 = val.toString();
                    arr.valor = val.toString();
                    agendamentoService.setPresumovalor2(presumovalor2);
                  }
                }
              }
            });
          }
        }
        else{
          paymentService.resetFree();
          dbRefDisciplinas.on('child_added', function(snap) {
            if(arr.disciplina === snap.val().Nome && categoryService.getCurrentCat() === snap.val().Categoria){
              if($scope.palunos === "1"){
                if(typeof agendamentoService.getPcd() != 'undefined'){
                  var desconto = parseInt(agendamentoService.getPcd(), 10);
                  var valorBruto = parseInt(snap.val().Valor, 10);
                  var descontado = valorBruto - (desconto * valorBruto / 100);
                  arr.valor = descontado.toString();
                  descontado = descontado / 100;
                  descontado = descontado.toFixed(2);
                  var resultado = descontado.toString().replace(".", ",");
                  $scope.pvalor = "R$ " + resultado;
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2 = resultado.replace(",", "");
                  agendamentoService.setPresumovalor2(presumovalor2);
                }
                else{
                  var x = parseInt(snap.val().Valor, 10);
                  x = x/100;
                  x = x.toFixed(2);
                  $scope.pvalor = "R$ " +  x.toString().replace(".", ",");
                  agendamentoService.setPvalor($scope.pvalor);
                  arr.valor = snap.val().Valor;
                  presumovalor2 = snap.val().Valor;
                  agendamentoService.setPresumovalor2(presumovalor2);
                }
              }
              else if($scope.palunos === "2"){
                var val = parseInt(snap.val().Valor,10);
                val = val*2 - 4000;
                if(typeof agendamentoService.getPcd() != 'undefined'){
                  var desconto = parseInt(agendamentoService.getPcd(), 10);
                  var descontado = val - (desconto * val / 100);
                  arr.valor = descontado.toString();
                  descontado = descontado / 100;
                  descontado = descontado.toFixed(2);
                  var resultado = descontado.toString().replace(".", ",");
                  $scope.pvalor = "R$ " + resultado;
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2 = resultado.replace(",", "");
                  agendamentoService.setPresumovalor2(presumovalor2);
                }
                else{
                  var val_reais = val / 100;
                  val_reais = val_reais.toFixed(2);
                  $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2 = val.toString();
                  arr.valor = val.toString();
                  agendamentoService.setPresumovalor2(presumovalor2);
                }
              }
              else if($scope.palunos === "3"){
                var val = parseInt(snap.val().Valor,10);
                val = val*3 - 6000;
                if(typeof agendamentoService.getPcd() != 'undefined'){
                  var desconto = parseInt(agendamentoService.getPcd(), 10);
                  var descontado = val - (desconto * val / 100);
                  arr.valor = descontado.toString();
                  descontado = descontado / 100;
                  descontado = descontado.toFixed(2);
                  var resultado = descontado.toString().replace(".", ",");
                  $scope.pvalor = "R$ " + resultado;
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2 = resultado.replace(",", "");
                  agendamentoService.setPresumovalor2(presumovalor2);
                }
                else{
                  var val_reais = val / 100;
                  val_reais = val_reais.toFixed(2);
                  $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2 = val.toString();
                  arr.valor = val.toString();
                  agendamentoService.setPresumovalor2(presumovalor2);
                }
              }
              else if($scope.palunos === "4"){
                var val = parseInt(snap.val().Valor,10);
                val = val*4 - 8000;
                if(typeof agendamentoService.getPcd() != 'undefined'){
                  var desconto = parseInt(agendamentoService.getPcd(), 10);
                  var descontado = val - (desconto * val / 100);
                  arr.valor = descontado.toString();
                  descontado = descontado / 100;
                  descontado = descontado.toFixed(2);
                  var resultado = descontado.toString().replace(".", ",");
                  $scope.pvalor = "R$ " + resultado;
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2 = resultado.replace(",", "");
                  agendamentoService.setPresumovalor2(presumovalor2);
                }
                else{
                  var val_reais = val / 100;
                  val_reais = val_reais.toFixed(2);
                  $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2 = val.toString();
                  arr.valor = val.toString();
                  agendamentoService.setPresumovalor2(presumovalor2);
                }
              }
              else if($scope.palunos === "5"){
                var val = parseInt(snap.val().Valor,10);
                val = val*5 - 10000;
                if(typeof agendamentoService.getPcd() != 'undefined'){
                  var desconto = parseInt(agendamentoService.getPcd(), 10);
                  var descontado = val - (desconto * val / 100);
                  arr.valor = descontado.toString();
                  descontado = descontado / 100;
                  descontado = descontado.toFixed(2);
                  var resultado = descontado.toString().replace(".", ",");
                  $scope.pvalor = "R$ " + resultado;
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2 = resultado.replace(",", "");
                  agendamentoService.setPresumovalor2(presumovalor2);
                }
                else{
                  var val_reais = val / 100;
                  val_reais = val_reais.toFixed(2);
                  $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2 = val.toString();
                  arr.valor = val.toString();
                  agendamentoService.setPresumovalor2(presumovalor2);
                }
              }
            }
          });
        }
      });

    }, 0);
  });

//----------------------------------------------------------------------------//
  $scope.check4email = function() {

    var user = firebase.auth().currentUser;
    var ferboden = false;
    var now = new Date();
    var dia = now.getDate();
    var mes = now.getMonth() + 1;
    var ano = now.getFullYear();
    var diaaula;
    var mesaula;
    var anoaula;

    if (user) {

      dbRefAulasAlunos.child(user.uid).on('child_added', function(snap) {
        if(typeof snap.val().Professor_Finalizou != 'undefined'){
          diaaula = parseInt(snap.val().Dia, 10);
          mesaula = parseInt(snap.val().Mês, 10);
          anoaula = parseInt(snap.val().Ano, 10);
          if(snap.val().Professor_Finalizou === "Não" && diaaula < dia && mesaula <= mes && anoaula <= ano || mesaula < mes && anoaula <= ano){
            function alertDismissed() {
                // do something
            }

            navigator.notification.alert(
                'Constatamos que você ainda não finalizou uma aula que já ocorreu. Por favor, encerre-a na sua agenda antes de marcar outra aula.',  // message
                alertDismissed,         // callback
                'Aviso',            // title
                'OK'                  // buttonName
            );

            ferboden = true;
          }
        }
      });

      $timeout(function() {
        //var codetxt = $document[0].getElementById('codetxt');
        var codetxt = agendamentoService.getPcd();
        if (user.emailVerified) {
          if((codetxt === "100" && !ferboden) || (paymentService.checkFree() && !ferboden)){
            $scope.confirmarAulaPocket();
          }
          else if(!ferboden){
            $scope.cards = [];
            $scope.nenhumCard = false;
            var arrayCards = window.localStorage.getItem('arrayCards');
            arrayCards = JSON.parse(arrayCards);
            if(arrayCards != null){
              for(var i = 0; i < arrayCards.length; i++){
                var original = arrayCards[i].number;
                var cut = original.substr(0, original.length - 3);
                var mask = "";
                for(var j = 0; j < cut.length; j++){
                  mask += "*";
                }
                mask += arrayCards[i].number.slice(-3);
                var card = {"brand" : arrayCards[i].brand, "number" : arrayCards[i].number, "cvv" : arrayCards[i].cvv, "expiration_month" : arrayCards[i].expiration_month, "expiration_year" : arrayCards[i].expiration_year,
                "cartaonome" : arrayCards[i].cartaonome, "cartaocpf" : arrayCards[i].cartaocpf, "cartaotelefone" : arrayCards[i].cartaotelefone, "cartaoemail" : arrayCards[i].cartaoemail, "cartaobirth" : arrayCards[i].cartaobirth,
                "cartaorua" : arrayCards[i].cartaorua, "cartaonumero" : arrayCards[i].cartaonumero, "cartaobairro" : arrayCards[i].cartaobairro, "cartaocep" : arrayCards[i].cartaocep, "cartaocidade" : arrayCards[i].cartaocidade,
                "cartaoestado" : arrayCards[i].cartaoestado, "cartaoid" : arrayCards[i].cartaoid, "mask" : mask};
                 $scope.cards.push(card);
              }
            }
            if($scope.cards.length === 0){
              $scope.nenhumCard = true;
            }
            $scope.modpdl.show();
          }

        }
        else if(!user.emailVerified){
          ionicToast.show('É necessário verificar seu e-mail antes de marcar uma aula. Por favor, confirme seu e-mail e faça login novamente.', 'middle', true, 2500);
        }

      }, 0);
    }

  };

//----------------------------------------------------------------------------//
  /*$scope.escolheuCard = function(id) {

    var user = firebase.auth().currentUser;
    if (user && ConnectivityMonitor.isOnline()) {
      user = user.uid;
      var cardData;
      function onConfirm(buttonIndex) {
        if(buttonIndex === 2){
          $ionicLoading.show({
            template: "'<ion-spinner></ion-spinner><br/><p class='custom'>Verificando</p>'"
          });
          var arrayCards = window.localStorage.getItem('arrayCards');
          if(arrayCards != null){
            arrayCards = JSON.parse(arrayCards);
            for(var i = 0; i < arrayCards.length; i++){
              if(arrayCards[i].cartaoid === id){
                cardData = {"brand" : arrayCards[i].brand, "number" : arrayCards[i].number, "cvv" : arrayCards[i].cvv, "expiration_month" : arrayCards[i].expiration_month, "expiration_year" : arrayCards[i].expiration_year};
                $ionicLoading.hide();
                $rootScope.cartaonome = arrayCards[i].cartaonome;
                $rootScope.cartaocpf = arrayCards[i].cartaocpf;
                $rootScope.cartaotelefone = arrayCards[i].cartaotelefone;
                $rootScope.cartaoemail = arrayCards[i].cartaoemail;
                $rootScope.cartaobirth = arrayCards[i].cartaobirth;
                $rootScope.cartaorua = arrayCards[i].cartaorua;
                $rootScope.cartaonumero = arrayCards[i].cartaonumero;
                $rootScope.cartaobairro = arrayCards[i].cartaobairro;
                $rootScope.cartaocep = arrayCards[i].cartaocep;
                $rootScope.cartaocidade = arrayCards[i].cartaocidade;
                $rootScope.cartaoestado = arrayCards[i].cartaoestado;

                $rootScope.checkout.getPaymentToken(cardData, function(error, response) {
                  if(error){

                    var alertPopup = $ionicPopup.alert({
                      title: 'Erro',
                      template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Os dados do cartão são inválidos. Por favor, adicione outro cartão.</p></div></div>",
                      cssClass: 'popcustom',
                      okType: 'customok'
                    });

                  }
                  else{
                    $rootScope.cardtoken = response.data.payment_token;
                    $scope.confirmarAulaCartao($rootScope.cartaonome, $rootScope.cartaocpf, $rootScope.cartaotelefone, $rootScope.cartaoemail, $rootScope.cartaobirth, $rootScope.cartaorua, $rootScope.cartaonumero, $rootScope.cartaobairro, $rootScope.cartaocep, $rootScope.cartaocidade, $rootScope.cartaoestado);
                  }
                });
              }
            }
          }
          else{
            $ionicLoading.hide();
            return;
          }
        }
        else{

        }
      }

      navigator.notification.confirm(
          'Escolher o cartão ' + id + ' ?', // message
           onConfirm,            // callback to invoke with index of button pressed
          'Confirmar',           // title
          ['Cancelar','Sim']     // buttonLabels
      );
    }

    else{
      ionicToast.show('Sem conexão com a internet. Verifique sua conexão antes de prosseguir', 'middle', true, 2500);
    }

  };*/

  $scope.escolheuCard = function(id, b, l4, cvc) {

    var user = firebase.auth().currentUser;
    if (user) {
      user = user.uid;
      var myPopup = $ionicPopup.show({
        template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-card icon-pop'></i></div></div></div><p class='custom'>Você tem certeza que quer escolher o cartão <strong>•••" + l4 + "</strong> ?</p></div></div>",
        title: b,
        cssClass: 'popcustom',
        scope: $scope,
        buttons: [
          { text: 'Cancelar', type: 'customconfirm' },
          {
            text: '<b>Sim</b>',
            type: 'customconfirm',
            onTap: function(e) {
              $scope.confirmarAulaCartao(id, cvc);
            }
          }
        ]
      });
    }

    else{
      ionicToast.show('Sem conexão com a internet. Verifique sua conexão antes de prosseguir', 'middle', true, 2500);
    }

  };

//----------------------------------------------------------------------------//
  /*$scope.confirmarAulaCartao = function(cartaonome, cartaocpf, cartaotelefone, cartaoemail, cartaobirth, cartaorua, cartaonumero, cartaobairro, cartaocep, cartaocidade, cartaoestado) {

    var user = firebase.auth().currentUser;

    if (user) {
      user = user.uid;
      var datas = [];
      var child = agendamentoService.getArrHorsFav();

      if(typeof $rootScope.pushID === 'undefined'){
        $ionicPlatform.ready(function() {
           window.plugins.OneSignal.getIds(function(ids) {
             $rootScope.pushID = ids.userId;
           });
         });
      }

      if(typeof $rootScope.pushID != 'undefined'){
        child.pushid = $rootScope.pushID;
      }

      child.name = name;
      child.cpf = cpf;
      child.email = email;
      child.birth = birth;
      child.phone_number = phone_number;
      child.street = cartaorua;
      child.number = cartaonumero;
      child.neighborhood = cartaobairro;
      child.cep = cartaocep;
      child.city = cartaocidade;
      child.state = cartaoestado;
      child.token = $rootScope.cardtoken;
      datas.push(child);
      var authToken = dataService.getAuthToken();
      
      var dbRefAlu = dbRefAlunos.child(user);
      var nomeAluno = agendamentoService.getAlunoNome();
      var charge_id;

      dbRefAlunosPrivate.child(user).update({"CPF" : cartaocpf});
      dbRefAlunosPrivate.child(user).update({"Aniversário" : cartaobirth});

      var presumovalor2 = agendamentoService.getPresumovalor2();
      presumovalor2 = parseInt(presumovalor2, 10);
      var aula = "Modular Aulas";

      $ionicLoading.show({
        template: "<ion-spinner icon='ios-small'></ion-spinner><br><p class='custom'>Marcando<br>aula</p>"
      });

      agendamentoService.marcarAulaCartao(datas).then(function successCallback(response) {

          var resp = response.data;
          resp.forEach(function(child) {

            paymentService.createTransaction(aula, parseInt(child.Valor, 10), authToken).then(function(respo) {
              charge_id = respo.data.data.charge_id;
              dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"Charge_ID" : charge_id});
              dbRefAulasAlunos.child(child.AlunoID).child(child.KeyPush).update({"Charge_ID" : charge_id});
            });

            notificationsService.remindTeacherNotification(child.ProfPush, "Você tem uma aula hoje às " + child.HoraPush + " horas", child.DataPush).then(function(res) {
              notificationprofessorID = res.data.id;
              dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"NotificationProfessor_ID" : notificationprofessorID});
            }, function(error) {
              //ionicToast.show('Erro ao enviar notificação.', 'middle', true, 2500);
            });
            notificationsService.newClassNotification(child.ProfPush, "Você tem uma nova aula marcada para o dia " + child.Data + " horas").then(function(re) {
              
            },
            function(e) {
              //ionicToast.show('Erro ao mandar notificação para o professor.', 'middle', true, 2500);
            });

          });

          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go("tabsController.aulas");

          $timeout(function() {
             var spun = $document[0].getElementById('spanaulas');
             if(spun != null){
               var spaan = spun.innerText;
               spaan = parseInt(spaan, 10);
               spaan = spaan + 1;
               spaan = spaan.toString();
               spun.innerText = spaan;
             }
             
             var alertPopup = $ionicPopup.alert({
                title: 'Concluído',
                template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Aula(s) marcada(s)!</p></div></div>",
                cssClass: 'popcustom',
                okType: 'customok'
              });
              $ionicHistory.clearCache();
              $ionicLoading.hide();

          }, 1000);

        }, function errorCallback(error) {
          var alertPopup = $ionicPopup.alert({
            title: 'Ooops!',
            template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>Ocorreu um erro! Por favor, tente novamente.</p></div></div>",
            cssClass: 'popcustom',
            okType: 'customok'
          });
          $ionicLoading.hide();

        });

    }

    else{
      $ionicLoading.hide();
      ionicToast.show('É necessário verificar seu e-mail antes de marcar uma aula.', 'middle', true, 2500);
      return;
    }

  };*/

  $scope.confirmarAulaCartao = function(cardId, cvc) {

    var user = firebase.auth().currentUser;

    if (user) {
      user = user.uid;
      var datas = [];
      var child = agendamentoService.getArrHorsFav();

      /*if(typeof $rootScope.pushID === 'undefined'){
        $ionicPlatform.ready(function() {
           window.plugins.OneSignal.getIds(function(ids) {
             $rootScope.pushID = ids.userId;
           });
         });
      }*/

      if(typeof $rootScope.pushID != 'undefined'){
        child.pushid = $rootScope.pushID;
      }

      child.cardId = cardId;
      child.cvc = cvc;
      datas.push(child);
      var authToken = dataService.getAuthToken();
      var cusId = paymentService.getCusId();
      
      var dbRefAlu = dbRefAlunos.child(user);
      var nomeAluno = agendamentoService.getAlunoNome();
      var charge_id;

      //dbRefAlunosPrivate.child(user).update({"CPF" : cartaocpf});
      //dbRefAlunosPrivate.child(user).update({"Aniversário" : cartaobirth});

      var presumovalor2 = agendamentoService.getPresumovalor2();
      presumovalor2 = parseInt(presumovalor2, 10);
      //var aula = "Modular Aulas";

      $ionicLoading.show({
        template: "<ion-spinner icon='ios-small'></ion-spinner><br><p class='custom'>Marcando<br>aula</p>"
      });

      agendamentoService.scheduleClassWithCreditCard(datas).then(function successCallback(response) {

          var resp = response.data;
          resp.forEach(function(child) {

            var aula = {
              "ownId": child.KeyPush,
              "price": child.Valor,
              "subject": child.Subject,
              "cusId": cusId,
              "merchId": teacherService.getMerchId(),
              "merchAmount": child.ValorProf
            }

            paymentService.createOrder(aula, authToken).then(function successCallback(respo) {
              charge_id = respo.data.id;
              dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"Charge_ID" : charge_id});
              dbRefAulasAlunos.child(child.AlunoID).child(child.KeyPush).update({"Charge_ID" : charge_id});
            }, function errorCallback(error) {
              console.log(error);
            });

            notificationsService.remindTeacherNotification(child.ProfPush, "Você tem uma aula hoje às " + child.HoraPush + " horas", child.DataPush).then(function(res) {
              notificationprofessorID = res.data.id;
              dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"NotificationProfessor_ID" : notificationprofessorID});
            }, function(error) {
              //ionicToast.show('Erro ao enviar notificação.', 'middle', true, 2500);
            });
            notificationsService.newClassNotification(child.ProfPush, "Você tem uma nova aula marcada para o dia " + child.Data + " horas").then(function(re) {
              
            },
            function(e) {
              //ionicToast.show('Erro ao mandar notificação para o professor.', 'middle', true, 2500);
            });

          });

          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go("tabsController.aulas");

          $timeout(function() {
             var spun = $document[0].getElementById('spanaulas');
             if(spun != null){
               var spaan = spun.innerText;
               spaan = parseInt(spaan, 10);
               spaan = spaan + 1;
               spaan = spaan.toString();
               spun.innerText = spaan;
             }
             
             var alertPopup = $ionicPopup.alert({
                title: 'Concluído',
                template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Aula(s) marcada(s)!</p></div></div>",
                cssClass: 'popcustom',
                okType: 'customok'
              });
              $ionicHistory.clearCache();
              $ionicLoading.hide();

          }, 1000);

        }, function errorCallback(error) {
          var alertPopup = $ionicPopup.alert({
            title: 'Ooops!',
            template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>Ocorreu um erro! Por favor, tente novamente.</p></div></div>",
            cssClass: 'popcustom',
            okType: 'customok'
          });
          $ionicLoading.hide();

        });

    }

    else{
      $ionicLoading.hide();
      ionicToast.show('É necessário verificar seu e-mail antes de marcar uma aula.', 'middle', true, 2500);
      return;
    }

  };

//----------------------------------------------------------------------------//
  $scope.interInicio = function() {
    $scope.modpdl.hide();
    $scope.modcardsl.show();
    //$state.go('tabsController.meusCartoes');
  };
//----------------------------------------------------------------------------//
  $scope.inicioBoleto = function() {
    $scope.modpdl.hide();
    $state.go('tabsController.boletofav');
  };

//----------------------------------------------------------------------------//
  $scope.interTrans = function() {
    $scope.modpdl.hide();
    $state.go('tabsController.transferfav');
  };

})
