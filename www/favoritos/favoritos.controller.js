angular.module('app.favoritosCtrl', [])

//**************************_FAVORITOS CONTROLLER*****************************//

.controller('favoritosCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, ionicToast, favoritesService, $cordovaActionSheet, $ionicPopup) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

  $scope.imgdefault = "../img/person-outline.png";

  $ionicModal.fromTemplateUrl('favoritos/favlocais.html', {
    scope: $scope
  }).then(function(modfavl) {
    $scope.modfavl = modfavl;
  });

  $ionicModal.fromTemplateUrl('favoritos/proffavdetalhes.html', {
    scope: $scope
  }).then(function(model) {
    $scope.model = model;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.modfavl != 'undefined'){
      $scope.modfavl.remove();
    }
    if(typeof $scope.model != 'undefined'){
      $scope.model.remove();
    }
  });

  //$ImageCacheFactory.Cache(snap.val().Avatar);

  /*$scope.$on("$ionicView.beforeEnter", function(){
    var hasFav = favoritesService.getHasFav();
    if(hasFav){
      $scope.nenhumfav = false;
    }
    else{
      $scope.nenhumfav = true;
    }
  });*/

  $scope.$on("$ionicView.loaded", function(){
      var hasFav = favoritesService.getHasFav();
      var user = firebase.auth().currentUser;
      $scope.favs = [];
      if (user && hasFav === true) {
        user = user.uid;
        dbRefAlunosPrivate.child(user).child("Favoritos").on('child_added', function(snap) {
          dbRefProfessores.child(snap.key).once('value', function(snapshot) {
            var rat;
            var ava = snapshot.val().Avaliações;
            var valorAva = snapshot.val().Avaliações_Valor;
            ava = parseInt(ava, 10);
            valorAva = parseInt(valorAva, 10);
            if(ava != 0){
              var resultado = valorAva / ava;
              resultado = resultado.toFixed(1);
            }
            if(ava === 0){
              rat = "0.0";
            }
            else{
              rat = resultado;
            }
           $timeout(function() {
             if(snapshot.val().Cadastro){
               var cds = snapshot.val().Cadastro.substring(0, 5);
             }
             if(typeof cds === 'undefined'){
               var cds = "17/03";
             }
             var fav = {"nome" : snapshot.val().Nome, "curso" : snapshot.val().Curso, "avatar" : snapshot.val().Avatar,  "descricao" : snapshot.val().Descrição,
             "avaliacoes" : snapshot.val().Avaliações, "rating" : rat, "key" : snapshot.key, "cadastro" : cds, "profpush" : snapshot.val().Push_ID};
             $scope.favs.push(fav);
           }, 0);
          });
        });
      }
      else{
        $scope.nenhumfav = true;
      }
  });

//----------------------------------------------------------------------------//
$scope.escolheuFav = function(nome, curso, avatar, descricao, avaliacoes, rating, key, cds, p) {

  var options = {
    title: nome,
    buttonLabels: ['Marcar aula', 'Detalhes'],
    addCancelButtonWithLabel: 'Cancelar',
    androidEnableCancelButton : true,
    winphoneEnableCancelButton : true,
    addDestructiveButtonWithLabel : 'Remover'
  };

  //teacherService.setProfFav(nome);
  //teacherService.setProfKeyFav(key);

  //added
  //teacherService.setAvatarFav(avatar);
  //teacherService.setProfPushFav(p);
  //$state.go('tabsController.calendariofav');

  $ionicPlatform.ready(function() {

    $cordovaActionSheet.show(options)
      .then(function(btnIndex) {
        switch(btnIndex) {
          case 1:
              var myPopup = $ionicPopup.show({
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>Você tem certeza que deseja remover esse professor da sua lista de favoritos?</p></div></div>",
              title: "Remover",
              subTitle: "<p class='custom'>" + nome + "</p>",
              cssClass: 'popcustom',
              scope: $scope,
              buttons: [
                { text: 'Cancelar', type: 'customconfirm' },
                {
                  text: '<b>Sim</b>',
                  type: 'customconfirm',
                  onTap: function(e) {
                    var user = firebase.auth().currentUser;
                    if(user){
                      user = user.uid;
                      dbRefAlunosPrivate.child(user).child("Favoritos").child(key).remove();
                      for(var i = 0; i < $scope.favs.length; i++){
                        if($scope.favs[i].key === key){
                          $scope.favs.splice(i, 1);
                          if($scope.favs.length === 0){
                            $scope.nenhumfav = true;
                          }
                          ionicToast.show('Removido da lista', 'middle', false, 2500);
                        }
                      }
                    }
                  }
                }
              ]
              });
              break;
          case 2:
              teacherService.setProfFav(nome);
              teacherService.setProfKeyFav(key);
              teacherService.setAvatarFav(avatar);
              teacherService.setProfPushFav(p);
              $state.go('tabsController.calendariofav');
              break;
          case 3:
              escolheuProf(nome, curso, avatar, descricao, avaliacoes, rating, key, cds);
              break;
          default:
              break;
        }
      });
  });
};
//----------------------------------------------------------------------------//
function escolheuProf(x,y,w,z,a,r,k,cds) {

      $scope.profNome = x;
      $scope.profPro = "Estudante";
      $scope.profCurso = y;

      $scope.profratingfav = r;
      $rootScope.starrating = r;

      $scope.nomeperfilproffav = x;
      $scope.cursoperfilproffav = y;
      $scope.descricaoperfilproffav = z;
      $scope.imgfullproffav = w;
      $scope.avaliacoesperfilproffav = a;
      $scope.cadastroperfilproffav = cds;
      $scope.model.show();

    };

//----------------------------------------------------------------------------//
  $scope.localFavSelect = function(local) {

    $scope.modfavl.hide();
    favoritesService.setLoc(local);
    $state.go('tabsController.profsfav');
  };

//----------------------------------------------------------------------------//
  $scope.increase = function() {

      var p = $document[0].getElementById('qtdalunosfav');
      var num = p.innerText;
      num = parseInt(num, 10);

      if(num < 5){
        num++;
        num = num.toString();
        p.innerText = num;
      }

  };

//----------------------------------------------------------------------------//
  $scope.decrease = function() {

      var p = $document[0].getElementById('qtdalunosfav');
      var num = p.innerText;
      num = parseInt(num, 10);

      if(num >= 2){
        num--;
        num = num.toString();
        p.innerText = num;
      }

  };


})
