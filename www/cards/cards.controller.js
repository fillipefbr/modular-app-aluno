angular.module('app.cardsCtrl', [])

//**************************_CARDS CONTROLLER_********************************//

.controller('cardsCtrl', function($scope, $rootScope, $document, $ionicPopup, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, ionicToast, ConnectivityMonitor, notificationsService, dataService, agendamentoService, $firebaseArray) {

  $scope.data = {};
  $scope.upcard = false;
  $scope.textocard= "Editar";

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

  var public_key = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoP0zdnPAsjGy1pg4gifTGN1WWVONvTAzSfJk6bic6GQtRjfk+pjtYw7g1/zCPNtknADXLx7PTNwmS6UHhUQGHlABGjs+wlW0QpKxXkOETnmtOcFSf2PAnn1ajCLDVQmw7PryIU80ZwVZDN8BQchjStjYn2RoWR3qIZymqRvq6nJojvyYTpufoMsGjv/6Tn51q5US1D+pvBcef9n7izawdxSVNCOwlnh14sDRbPzVPShiYGPVIRtJrhk5d6a0K/Hg7wXVNQrQze5sn/wzMptVr0aSPJSUB+o9zLn9dEM4ZinfqIiPBDPY9TdKpBduNhTbQEf95BZgv24BOy8s1CE5IwIDAQAB-----END PUBLIC KEY-----";
  var cusId;

  $scope.cards = [];
  $scope.nenhumCard = false;

  $scope.data.holder = {};
  $scope.data.holder.phone = {};
  $scope.data.shippingAddress = {};

  /*var cc = new Moip.CreditCard({
    number  : cc_number,
    cvc     : cc_cvc,
    expMonth: cc_exp_month,
    expYear : cc_exp_year,
    pubKey  : public_key
  });

  console.log(cc);
  console.log('isValid: ' + cc.isValid());
  console.log('hash: ' + cc.hash());
  console.log('cardType: ' + cc.cardType());*/
  $scope.cards = $firebaseArray(dbRefAlunosPrivate.child(agendamentoService.getAlunoKey()).child("creditCards"));

  $scope.$on("$ionicView.beforeEnter", function(){

     //$scope.cards = [];
     $scope.nenhumCard = false;
     var user = firebase.auth().currentUser;

     if (user) {

       user = user.uid;
       if(paymentService.hasCusId()){
         cusId = paymentService.getCusId();
         //$scope.cards = $firebaseArray(dbRefAlunosPrivate.child(user).child("creditCards"));
         $ionicLoading.hide();
       }
       else{
        $scope.nenhumCard = true;
        $ionicLoading.hide();
       }

       $timeout(function() {
         if(typeof $scope.cards != 'undefined' && $scope.cards.length === 0){
            $scope.nenhumCard = true;
         }
         else{
          $scope.nenhumCard = false;
         }
       }, 1000);

     }

  });

//----------------------------------------------------------------------------//
  $scope.checkAddCard = function() {
    $state.go('tabsController.addCartao');
  };

//----------------------------------------------------------------------------//
  /*$scope.addCardAdress = function() {

    var cardcartao = $document[0].getElementById('cardcartao2');
    var cardbandeira = $document[0].getElementById('cardbandeira2');
    var cardcvv = $document[0].getElementById('cardcvv2');
    var cardmes = $document[0].getElementById('cardmes2');
    var cardano = $document[0].getElementById('cardano2');

    if($scope.data.b === "Visa"){
      $rootScope.brand = "visa";
    }

    else if($scope.data.b === "MasterCard"){
      $rootScope.brand = "mastercard";
    }

    else if($scope.data.b === "American Express"){
      $rootScope.brand = "amex";
    }

    else if($scope.data.b === "Aura"){
      $rootScope.brand = "aura";
    }

    else if($scope.data.b === "Elo"){
      $rootScope.brand = "elo";
    }

    else if($scope.data.b === "Discover Network"){
      $rootScope.brand = "discover";
    }

    else if($scope.data.b === "JCB"){
      $rootScope.brand = "jcb";
    }

    else if($scope.data.b === "Diners Club"){
      $rootScope.brand = "diners";
    }

    var cv = cardcartao.value;
    cv = cv.replace(/\D/g,'');
    if(cv.length < 13){
      ionicToast.show('Número do cartão inválido', 'middle', false, 2500);
      return;
    }
    $rootScope.number = cv;
    var cvv = cardcvv.value;
    cvv = cvv.replace(/\D/g,'');
    if(cvv.length != 3){
      ionicToast.show('CVV inválido', 'middle', false, 2500);
      return;
    }
    $rootScope.cvv = cvv;
    var mes = cardmes.value;
    mes = mes.replace(/\D/g,'');
    if(mes.length != 2){
      ionicToast.show('Mês inválido', 'middle', false, 2500);
      return;
    }
    $rootScope.expiration_month = mes;
    var ano = cardano.value;
    ano = ano.replace(/\D/g,'');
    if(ano.length != 4){
      ionicToast.show('Ano inválido', 'middle', false, 2500);
      return;
    }
    $rootScope.expiration_year = ano;

    $state.go('tabsController.addCartaoAdress');

  };*/


  $scope.addCardAdressConfig = function(data, onde) {

    var cc = new Moip.CreditCard({
      number  : data.cc_number,
      cvc     : data.cc_cvc,
      expMonth: data.cc_exp_month,
      expYear : data.cc_exp_year,
      pubKey  : public_key
    });

    if(cc.isValid()){
      paymentService.setCc({
        "number"  : data.cc_number,
        "cvc"     : data.cc_cvc,
        "expirationMonth": data.cc_exp_month,
        "expirationYear" : data.cc_exp_year
      });
      //$state.go('tabsController.addCartaoAdressConfig');
      $state.go('tabsController.' + onde);
    }

    else{
      var alertPopup = $ionicPopup.alert({
        title: 'Ops!',
        template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>Dados do cartão inválidos! Por favor, tente novamente.</p></div></div>",
        cssClass: 'popcustom',
        okType: 'customok'
      });
    }

  };

//----------------------------------------------------------------------------//
  $scope.loadCardInfoConfig = function() {

    $timeout(function() {
      var user = firebase.auth().currentUser;
      if (user) {
        user = user.uid;
        dbRefAlunosPrivate.child(user).once('value', function(snap) {
          $scope.data.holder.fullname = snap.val().Nome;
          if(snap.val().CPF != "Não informado"){
            $scope.data.holder.cpf = parseInt(snap.val().CPF, 10);
          }
          $scope.data.holder.phone.number = snap.val().Telefone;
          $scope.data.holder.email = snap.val().Email;
          if(snap.val().Aniversário != "Não informado"){
            var stringniver = snap.val().Aniversário;
            var arraydata = stringniver.split("-");
            var ano = parseInt(arraydata[0], 10);
            var mes = parseInt(arraydata[1], 10);
            var dia = parseInt(arraydata[2], 10);
            $scope.data.holder.birthDate = new Date(ano, mes - 1, dia);
          }
          //$scope.data.cartaocidade = 'Brasília';
          if(snap.val().shippingAddress) {
            $scope.data.shippingAddress = snap.val().shippingAddress;
          }
          else{
            $scope.data.shippingAddress.state = 'DF';
          }
        });
      }
    });
  };

//----------------------------------------------------------------------------//
  $scope.addMyCard = function(data, onde) {

    var user = firebase.auth().currentUser;
    if (user) {
      //user = user.uid;
      data.holder.fullname = (data.holder.fullname).toString();
      data.holder.fullname = (data.holder.fullname).replace(/[0-9]/g,'');
      if(/^[ ]*(.+[ ]+)+.+[ ]*$/.test(data.holder.fullname) === false){
        ionicToast.show('Nome inválido', 'middle', false, 3000);
        return;
      }
      data.holder.cpf = data.holder.cpf.toString();
      data.holder.cpf = (data.holder.cpf).replace(/\D/g,'');
      if((data.holder.cpf).length != 11){
        ionicToast.show('CPF inválido', 'middle', false, 2500);
        return;
      }
      dbRefAlunosPrivate.child(user.uid).update({"CPF" : data.holder.cpf});
      data.holder.phone.number = (data.holder.phone.number).toString();
      data.holder.phone.number = (data.holder.phone.number).replace(/\D/g,'');
      if(/^[1-9]{2}9?[0-9]{8}$/.test(data.holder.phone.number) === false){
        ionicToast.show('Telefone inválido', 'middle', false, 2500);
        return;
      }
      data.shippingAddress.zipCode = (data.shippingAddress.zipCode).toString();
      data.shippingAddress.zipCode = (data.shippingAddress.zipCode).replace(/\D/g,'');

      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner><br/><p class='custom'>Adicionando</p>"
      });

      data.shippingAddress.state = (data.shippingAddress.state).toUpperCase();

      data.holder.phone.areaCode = (data.holder.phone.number).substring(0, 2);
      data.holder.phone.number = (data.holder.phone.number).substring(2);

      var yearbirth = (data.holder.birthDate).getFullYear();
      var monthbirth = (data.holder.birthDate).getMonth() + 1;
      //var monthbirth = (data.holder.birthDate).getMonth();
      if(monthbirth < 10){
        monthbirth = "0" + monthbirth;
      }
      var datebirth = (data.holder.birthDate).getDate();
      if(datebirth < 10){
        datebirth = "0" + datebirth;
      }
      //data.holder.birthDate = yearbirth + "-" + monthbirth + "-" + datebirth;
      var holderBirthDate = yearbirth + "-" + monthbirth + "-" + datebirth;
      dbRefAlunosPrivate.child(user.uid).update({"Aniversário" : holderBirthDate});
      dbRefAlunosPrivate.child(user.uid).update({"CPF" : data.holder.cpf}); //ADDED
      dbRefAlunosPrivate.child(user.uid).update({"shippingAddress" : data.shippingAddress}); //ADDED


      var cc = paymentService.getCc();

      var obj = {
        "number" : cc.number,
        "cvc" : cc.cvc,
        "expirationMonth" : cc.expirationMonth,
        "expirationYear" : cc.expirationYear,
        "holder": {
          "fullname" : data.holder.fullname,
          "birthdate" : holderBirthDate,
          "cpf" : data.holder.cpf,
          "phone" : {
            "areaCode" : data.holder.phone.areaCode,
            "number" : data.holder.phone.number
          }
        }
      };

      var authToken = dataService.getAuthToken();

      if(paymentService.hasCusId()) {
        obj.cusId = paymentService.getCusId();
        paymentService.addCustomerCard(obj, authToken).then(function successCallback(response) {
          dbRefAlunosPrivate.child(user.uid).child("creditCards").child(response.data.creditCard.id).set({
            "id" : response.data.creditCard.id,
            "brand" : response.data.creditCard.brand,
            "first6" : response.data.creditCard.first6,
            "last4" : response.data.creditCard.last4,
            "cvc" : cc.cvc,
            "holder": {
              "fullname" : data.holder.fullname,
              "birthdate" : holderBirthDate,
              "cpf" : data.holder.cpf,
              "phone" : {
                "areaCode" : data.holder.phone.areaCode,
                "number" : data.holder.phone.number
              },
              "shippingAddress" : data.shippingAddress
            }
          });
          $scope.cards = $firebaseArray(dbRefAlunosPrivate.child(user.uid).child("creditCards"));
          $ionicLoading.hide();
          $state.go('tabsController.' + onde);
        }, function errorCallback(error) {
          $ionicLoading.hide();
          console.log(error);
        });
      }
      else{
        var obj2 = {
          "ownId": user.uid,
          "fullname": user.displayName,
          "birthDate" : holderBirthDate,
					"email": user.email,
          "shippingAddress": data.shippingAddress,
          "number" : cc.number,
          "cvc" : cc.cvc,
          "expirationMonth" : cc.expirationMonth,
          "expirationYear" : cc.expirationYear,
          "holder": {
            "fullname" : data.holder.fullname,
            "birthdate" : holderBirthDate,
            "cpf" : data.holder.cpf,
            "phone" : {
              "areaCode" : data.holder.phone.areaCode,
              "number" : data.holder.phone.number
            }
          }
        }
        paymentService.createCustomerWithCreditCard(obj2, authToken).then(function successCallback(response) {
          dbRefAlunosPrivate.child(user.uid).update({"customerId" : response.data.id});
          paymentService.setCusId(response.data.id); //ADDED
          dbRefAlunosPrivate.child(user.uid).child("creditCards").child(response.data.fundingInstrument.creditCard.id).set({
            "id" : response.data.fundingInstrument.creditCard.id,
            "brand" : response.data.fundingInstrument.creditCard.brand,
            "first6" : response.data.fundingInstrument.creditCard.first6,
            "last4" : response.data.fundingInstrument.creditCard.last4,
            "cvc" : cc.cvc,
            "holder": {
              "fullname" : data.holder.fullname,
              "birthdate" : holderBirthDate,
              "cpf" : data.holder.cpf,
              "phone" : {
                "areaCode" : data.holder.phone.areaCode,
                "number" : data.holder.phone.number
              },
              "shippingAddress" : data.shippingAddress
            }
          });
          //$scope.cards = $firebaseArray(dbRefAlunosPrivate.child(user.uid).child("creditCards"));
          //$scope.cards.pu
          $ionicLoading.hide();
          $state.go('tabsController.' + onde);
        }, function errorCallback(error) {
          $ionicLoading.hide();
          console.log(error);
        });
      }

    }

  };

//----------------------------------------------------------------------------//
  $scope.deleteMyCard = function(id, b, l4) {

    var myPopup = $ionicPopup.show({
    template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-card icon-pop'></i></div></div></div><p class='custom'>Você tem certeza que quer excluir o cartão <strong>•••" + l4 + "</strong> ?</p></div></div>",
    title: b,
    cssClass: 'popcustom',
    scope: $scope,
    buttons: [
      { text: 'Cancelar', type: 'customconfirm' },
      {
        text: '<b>Sim</b>',
        type: 'customconfirm',
        onTap: function(e) {

          $ionicLoading.show({
            template: "<ion-spinner></ion-spinner><br/><p class='custom'>Excluindo</p>"
          });

          var user = firebase.auth().currentUser;
          if(user) {
            var hasClass = false;

            dbRefAulasAlunos.child(user.uid).once('value', function(snap) {
              if(snap.val()){
                snap.forEach(function(child) {
                  if(child.val() && child.val().card && child.val().card.id === id){
                    hasClass = true;
                  }
                });
                if(hasClass){
                  $ionicLoading.hide();
                  var alertPopup = $ionicPopup.alert({
                    title: "Atenção",
                    template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Você possui uma aula agendada cujo pagamento será efetuado com este cartão de crédito. Por favor, aguarde a conclusão ou o cancelamento da sua aula para poder excluir este cartão.</p></div></div>",
                    cssClass: 'popcustom',
                    okType: 'customok'
                  });
                }
                else{
                  var authToken = dataService.getAuthToken();
                  paymentService.deleteCustomerCard(id, authToken).then(function successCallback(response) {
                    dbRefAlunosPrivate.child(user.uid).child("creditCards").child(id).remove();
                    $ionicLoading.hide();
                    ionicToast.show('Cartão excluído.', 'middle', false, 2500);
                  }, function errorCallback(error) {
                    console.log(error);
                    $ionicLoading.hide();
                    ionicToast.show('Erro. Por favor, tente novamente.', 'middle', false, 2500);
                  });
                }
              }
              else{
                var authToken = dataService.getAuthToken();
                paymentService.deleteCustomerCard(id, authToken).then(function successCallback(response) {
                  dbRefAlunosPrivate.child(user.uid).child("creditCards").child(id).remove();
                  $ionicLoading.hide();
                  ionicToast.show('Cartão excluído.', 'middle', false, 2500);
                }, function errorCallback(error) {
                  console.log(error);
                  $ionicLoading.hide();
                  ionicToast.show('Erro. Por favor, tente novamente.', 'middle', false, 2500);
                });
              }
            });

          }

        }
      }
    ]
    });

  };

//----------------------------------------------------------------------------//
  $scope.escolheuCard = function(cc) {

    var user = firebase.auth().currentUser;
    if (user) {
      user = user.uid;
      var myPopup = $ionicPopup.show({
        template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-card icon-pop'></i></div></div></div><p class='custom'>Você tem certeza que quer escolher o cartão <strong>•••" + cc.last4 + "</strong> ?</p></div></div>",
        title: cc.brand,
        cssClass: 'popcustom',
        scope: $scope,
        buttons: [
          { text: 'Cancelar', type: 'customconfirm' },
          {
            text: '<b>Sim</b>',
            type: 'customconfirm',
            onTap: function(e) {
              var card = {
                "brand" : cc.brand,
                "id" : cc.id,
                "last4" : cc.last4,
                "cvc" : cc.cvc,
                "holder" : cc.holder
              }
              $scope.confirmarAulaCartao(card);
            }
          }
        ]
      });
    }

    else{
      ionicToast.show('Sem conexão com a internet. Verifique sua conexão antes de prosseguir', 'middle', true, 2500);
    }

  };

//----------------------------------------------------------------------------//
  $scope.detailCard = function(cc) {
    var alertPopup = $ionicPopup.alert({
      title: "Seguro",
      template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-locked-outline icon-pop'></i></div></div></div><p class='custom'><strong>" + cc.brand + "<br>•••" + cc.last4 + "</strong></p></div></div>",
      cssClass: 'popcustom',
      okType: 'customok'
    });
  };

//----------------------------------------------------------------------------//
  $scope.editaCard = function() {
    if($scope.upcard === false){
      $scope.upcard = true;
      $scope.textocard= "Ok";
    }
    else{
      $scope.upcard = false;
      $scope.textocard= "Editar";
    }
  };
//----------------------------------------------------------------------------//
  $scope.calculateProfSlice = function(cat, valor) {
    var v = parseInt(valor, 10);
    if(cat === "Ensino Fundamental" || cat === "Ensino Médio"){
      v = 66.74 * v / 100;
      //v = Math.round(v);
      v = v.toFixed(2);
      v = v.toString();
      return v;
    }
    else if(cat === "Ensino Superior"){
      v = 65.07 * v / 100;
      //v = Math.round(v);
      v = v.toFixed(2);
      v = v.toString();
      return v;
    }
    else if(cat === "Pré-Vestibular"){
      v = 65.07 * v / 100;
      //v = Math.round(v);
      v = v.toFixed(2);
      v = v.toString();
      return v;
    }
    else if(cat === "Idiomas"){
      v = 69.57 * v / 100;
      //v = Math.round(v);
      v = v.toFixed(2);
      v = v.toString();
      return v;
    }
    else{
      v = 66.74 * v / 100;
      //v = Math.round(v);
      v = v.toFixed(2);
      v = v.toString();
      return v;
    }
  };

//----------------------------------------------------------------------------//
  $scope.confirmarAulaCartao = function(cc) {

    var user = firebase.auth().currentUser;

    if (user) {
      user = user.uid;
      var datas = [];
      var child = agendamentoService.getArrHors();

      if(typeof $rootScope.pushID === 'undefined'){
        $ionicPlatform.ready(function() {
           window.plugins.OneSignal.getIds(function(ids) {
             $rootScope.pushID = ids.userId;
           });
         });
      }

      if(typeof $rootScope.pushID != 'undefined'){
        child.pushid = $rootScope.pushID;
      }

      child.card = cc;
      datas.push(child);
      var authToken = dataService.getAuthToken();
      var cusId = paymentService.getCusId();
      
      var dbRefAlu = dbRefAlunos.child(user);
      var nomeAluno = agendamentoService.getAlunoNome();
      var charge_id;

      //dbRefAlunosPrivate.child(user).update({"CPF" : cartaocpf});
      //dbRefAlunosPrivate.child(user).update({"Aniversário" : cartaobirth});

      var presumovalor2 = agendamentoService.getPresumovalor2();
      presumovalor2 = parseInt(presumovalor2, 10);
      //var aula = "Modular Aulas";

      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner><br><p class='custom'>Marcando<br>aula</p>"
      });

      agendamentoService.scheduleClassWithCreditCard(datas, authToken).then(function successCallback(response) {

          var resp = response.data;
          resp.forEach(function(child) {

            var aula = {
              "ownId": child.KeyPush,
              "price": child.Valor,
              "subject": child.Subject,
              "cusId": cusId,
              "merchId": teacherService.getMerchId(),
              "merchAmount": child.ValorProf
            }

            paymentService.createOrder(aula, authToken).then(function successCallback(respo) {
              charge_id = respo.data.id;
              dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"Charge_ID" : charge_id});
              dbRefAulasAlunos.child(child.AlunoID).child(child.KeyPush).update({"Charge_ID" : charge_id});
            }, function errorCallback(error) {
              console.log(error);
            });

            notificationsService.remindTeacherNotification(child.ProfPush, "Você tem uma aula hoje às " + child.HoraPush + " horas", child.DataPush).then(function(res) {
              notificationprofessorID = res.data.id;
              dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"NotificationProfessor_ID" : notificationprofessorID});
            }, function(error) {
              //ionicToast.show('Erro ao enviar notificação.', 'middle', true, 2500);
            });
            notificationsService.newClassNotification(child.ProfPush, "Você tem uma nova aula marcada para o dia " + child.Data + " horas").then(function(re) {
              
            },
            function(e) {
              //ionicToast.show('Erro ao mandar notificação para o professor.', 'middle', true, 2500);
            });

          });

          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go("tabsController.aulas");

          $timeout(function() {
             var spun = $document[0].getElementById('spanaulas');
             if(spun != null){
               var spaan = spun.innerText;
               spaan = parseInt(spaan, 10);
               spaan = spaan + 1;
               spaan = spaan.toString();
               spun.innerText = spaan;
             }
             
             var alertPopup = $ionicPopup.alert({
                title: 'Concluído',
                template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Aula(s) marcada(s)!</p></div></div>",
                cssClass: 'popcustom',
                okType: 'customok'
              });
              $ionicHistory.clearCache();
              $ionicLoading.hide();

          }, 1000);

        }, function errorCallback(error) {
          var alertPopup = $ionicPopup.alert({
            title: 'Ooops!',
            template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>Ocorreu um erro! Por favor, tente novamente.</p></div></div>",
            cssClass: 'popcustom',
            okType: 'customok'
          });
          $ionicLoading.hide();

        });

    }

    else{
      $ionicLoading.hide();
      ionicToast.show('É necessário verificar seu e-mail antes de marcar uma aula.', 'middle', true, 2500);
      return;
    }

  };

})
