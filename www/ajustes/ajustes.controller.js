angular.module('app.ajustesCtrl', [])

//****************************_AJUSTES CONTROLLER******************************//

.controller('ajustesCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, categoryService, ionicToast, $ionicPopup) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessores = dbRefRoot.child('professores');
  const dbRefContatoModular = dbRefRoot.child('contato_modular');
  const dbRefContasDeletadas = dbRefRoot.child('contas_deletadas');

//----------------------------------------------------------------------------//
  $scope.$on("$ionicView.beforeEnter", function(){
    
    dbRefContatoModular.child('c1').once('value', function(snap) {
      $timeout(function() {
        $scope.modular1nome = snap.val().Nome;
        $scope.modular1numero = snap.val().Número;
      }, 0);
    });
    dbRefContatoModular.child('c2').once('value', function(snap) {
      $timeout(function() {
        $scope.modular2nome = snap.val().Nome;
        $scope.modular2numero = snap.val().Número;
      }, 0);
    });

    var user = firebase.auth().currentUser;

    if (user) {
      if(user.emailVerified){
        $timeout(function() {
          $scope.contaok = true;
          $scope.contastatus = "Ativa";
        }, 0);
      }
      else{
        $timeout(function() {
          $scope.contanotok = true;
          $scope.contastatus = "Inativa";
        }, 0);
      }

      user = user.uid;

      var x = "";
      var str = "";
      var categ;
      var dbRefAlu = dbRefAlunosPrivate.child(user);

      dbRefAlu.once('value', function(snap) {

          $timeout(function() {
              $scope.userconfig = snap.val().Nome;
              $scope.valor = true;

              if(snap.val().Email.length > 20){
                str = snap.val().Email.substring(0,20);
                str += "...";
                $scope.useremail = str;
                str = "";
              }
              else{
                $scope.useremail = snap.val().Email;
              }

              $scope.usercontato = snap.val().Telefone;

              for(i = 0; i < (snap.val().Senha).length; i++) {
                x += "*";
              }

              $scope.usersenha = x;

              if(typeof snap.val().Escola != 'undefined'){
                $scope.userescola = snap.val().Escola.substring(0, 15) + '...';
              }

            categ = snap.val().Categoria;

            if(categ === "EF"){
              $scope.usercategoria = "Ensino Fundamental";
            }

            else if(categ === "1EM"){
              $scope.usercategoria = "1º Ano Ensino Médio";
            }

            else if(categ === "2EM"){
              $scope.usercategoria = "2º Ano Ensino Médio";
            }

            else if(categ === "3EM"){
              $scope.usercategoria = "3º Ano Ensino Médio";
            }

            else if(categ === "SUP"){
              $scope.usercategoria = "Ensino Superior";
            }

            else if(categ === "VE"){
              $scope.usercategoria = "Pré-Vestibular";
            }

            else if(categ === "IDI"){
              $scope.usercategoria = "Idiomas";
            }

            else if(categ === "CV"){
              $scope.usercategoria = "Cursos";
            }
          }, 0);

      });
    }
    
  });


//----------------------------------------------------------------------------//
$scope.editContato = function() {

  var user = firebase.auth().currentUser;
  $scope.data.novoContato = "";

  if(user){

    $ionicPopup.show({
        title: "Novo Número" ,
        scope: $scope ,
        cssClass: 'popcustom',
        template: "<input class='custom' type = 'tel' placeholder = 'Número' autofocus = 'true' ng-model = 'data.novoContato' style='text-align: center; border-bottom: 1px solid #A9A9A9;'>",
        buttons: [
          {text: "OK", type: 'customconfirm',
          onTap: function(e) {

            var userText = $scope.data.novoContato.replace(/^\s*/, '').replace(/\s*$/, '');
            if(userText != ""){

            dbRefAlunosPrivate.child(user.uid).update({"Telefone": $scope.data.novoContato});
            $scope.usercontato = $scope.data.novoContato;
            ionicToast.show('Contato atualizado', 'middle', false, 2500);

          }
          else{
            var alertPopup = $ionicPopup.alert({
             title: 'Aviso',
             template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Por favor, informe um número válido.</p></div></div>",
             cssClass: 'popcustom',
             okType: 'customok'
           });
          }

          }},
          {text: "Cancelar", type: 'customconfirm',
           onTap: function(e) {
             $scope.up = false;
             $scope.texto = "Editar";
           }}

        ]
      });
  }

  else{

  }

};

//----------------------------------------------------------------------------//
$scope.statusConta = function() {
  var user = firebase.auth().currentUser;

  if (user && user.emailVerified) {
    var alertPopup = $ionicPopup.alert({
     title: 'Conta Ativa',
     template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-pulse-strong icon-pop'></i></div></div></div><p class='custom'>Sua conta foi verificada com sucesso. Você já pode marcar aulas!</p></div></div>",
     cssClass: 'popcustom',
     okType: 'customok'
   });
  }
  else if(user && !user.emailVerified){
    var myPopup = $ionicPopup.show({
    template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-pulse-strong icon-pop' style='color: #ef473a;'></i></div></div></div><p class='custom'>Você ainda não verificou seu e-mail para esta conta. Gostaria que enviássemos o link de ativação da conta para o seu e-mail novamente?</p></div></div>",
    title: "Conta Inativa",
    cssClass: 'popcustom',
    scope: $scope,
    buttons: [
      { text: 'Cancelar', type: 'customconfirm' },
      {
        text: '<b>Sim</b>',
        type: 'customconfirm',
        onTap: function(e) {
          var user = firebase.auth().currentUser;
          if(user){
            user.sendEmailVerification();
            $ionicLoading.show({
              template: "<ion-spinner icon='ios-small'></ion-spinner> <br/>",
              duration: 2000
            });
            $timeout(function() {
              var alertPopup = $ionicPopup.alert({
               title: 'E-mail Enviado',
               template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-email-outline icon-pop'></i></div></div></div><p class='custom'>Enviamos outro e-mail para você. Por favor, clique no link indicado no e-mail para ativar sua conta.</p></div></div>",
               cssClass: 'popcustom',
               okType: 'customok'
             });
            }, 2000);
          }
        }
      }
    ]
    });
  }
};

$scope.moveMinhaCategoria = function() {
  //$scope.up = false;
  //$scope.texto = "Editar";
  $state.go('tabsController.minhaCategoria');
};

//----------------------------------------------------------------------------//
$scope.excluirConta = function() {

  var myPopup = $ionicPopup.show({
    template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Ao confirmar, todos os dados da sua conta serão apagados. Tem certeza que deseja prosseguir?</p></div></div>",
    title: "Atenção",
    cssClass: 'popcustom',
    scope: $scope,
    buttons: [
      { text: 'Cancelar', type: 'customconfirm' },
      {
        text: '<b>Sim</b>',
        type: 'customconfirm',
        onTap: function(e) {
          $ionicLoading.show({
            template: "<ion-spinner></ion-spinner><br><p class='custom'>Excluindo conta</p>"
          });
          var user = firebase.auth().currentUser;
          if(user){
            var email;
            var senha;
            dbRefAlunosPrivate.child(user.uid).once('value', function(snap) {
              email = snap.val().Email;
              senha = snap.val().Senha;
              var credential = firebase.auth.EmailAuthProvider.credential(email, senha);
              user.reauthenticate(credential).then(function() {
                 dbRefContasDeletadas.child(user.uid).set(snap.val());
                 dbRefAlunos.child(user.uid).remove();
                 dbRefAlunosPrivate.child(user.uid).remove();
                 user.delete().then(function() {
                   $ionicLoading.hide();
                   var alertPopup = $ionicPopup.alert({
                     title: 'Concluído',
                     template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-information icon-pop'></i></div></div></div><p class='custom'>Sua conta foi excluída com sucesso.</p></div></div>",
                     cssClass: 'popcustom',
                     okType: 'customok'
                   });
                   $state.go('inicio');
                 }, function(error) {
                   $ionicLoading.hide();
                   ionicToast.show('Ocorreu um erro. Por favor, faça login novamente antes de excluir sua conta.', 'middle', true, 2500);
                   $state.go('inicio');
                 });
               }, function(error) {
                  $ionicLoading.hide();
                  ionicToast.show('Ocorreu um erro. Por favor, faça login novamente antes de excluir sua conta.', 'middle', true, 2500);
               });
            });
          }
        }
      }
    ]
  });

};

//----------------------------------------------------------------------------//
  $scope.sair = function() {

    $ionicLoading.show({
      template: "<ion-spinner icon='ios-small'></ion-spinner><br><p class='custom'>Saindo</p>",
      duration: 2000
    });

    $scope.data = {};

    firebase.auth().signOut().then(function() {

      $ionicHistory.nextViewOptions({
        disableBack: true,
        historyRoot: true
      });

      $state.go('inicio');

      $timeout(function() {
        $ionicHistory.clearCache();
      }, 200);

    }, function(error) {
      
    });

  };



})
