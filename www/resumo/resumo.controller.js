angular.module('app.resumoCtrl', [])

//***************************_RESUMO CONTROLLER*******************************//

.controller('resumoCtrl', function($scope, $rootScope, $document, $ionicPopup, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicActionSheet, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, ionicToast, ConnectivityMonitor, notificationsService, agendamentoService, pacotesService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const dbRefCadastroPermanente = dbRefRoot.child('cadastro_permanente');
  const dbRefCodesPromocionais = dbRefRoot.child('codes_promocionais');
  const dbRefContatoModular = dbRefRoot.child('contato_modular');
  const auth = firebase.auth();

  $ionicModal.fromTemplateUrl('resumo/paymentDetalhes.html', {
    scope: $scope
  }).then(function(modpdl) {
    $scope.modpdl = modpdl;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.modpdl != 'undefined'){
      $scope.modpdl.remove();
    }
  });

  $scope.$on("$ionicView.beforeEnter", function(){

    var arr = agendamentoService.getArrHors();
    var has_pkt = false;

    dbRefProfessores.child(arr.prof).once('value', function(snap) {

      $scope.avatarprofresumo = snap.val().Avatar;
      $scope.h2profresumo = snap.val().Nome;
      $scope.pprofcursoresumo = snap.val().Curso;

    });

    var user = firebase.auth().currentUser;

    if (user) {
      user = user.uid;
    }


    $timeout(function() {

      var presumovalor2 = $document[0].getElementById('presumovalor2');
      var temp;
      var tempdia;
      
      $scope.pd = arr.disciplina;
      agendamentoService.setPd($scope.pd);
      agendamentoService.setPadress(arr.endereco);

      if(arr.endereco.length > 17){
        $scope.padress = arr.endereco.substring(0, 17) + "...";
      }
      else{
        $scope.padress = arr.endereco;
      }
    
      $scope.pconteudo = arr.conteudo;
      agendamentoService.setPconteudo($scope.pconteudo);
      $scope.palunos = (arr.alunos).toString();
      agendamentoService.setPalunos($scope.palunos);

      if(categoryService.getCurrentCat() === "Fundamental"){
        $scope.pcat = "Ensino Fundamental";
        agendamentoService.setPcat($scope.pcat);
      }

      else if(categoryService.getCurrentCat() === "Médio"){
        $scope.pcat = "Ensino Médio";
        agendamentoService.setPcat($scope.pcat);
      }

      else if(categoryService.getCurrentCat() === "Vestibular"){
        $scope.pcat = "Pré-Vestibular";
        agendamentoService.setPcat($scope.pcat);
      }

      else{
        $scope.pcat = categoryService.getCurrentCat();
        agendamentoService.setPcat($scope.pcat);
      }
      
      $scope.phora = arr.phora;
      agendamentoService.setPhora($scope.phora);
      temp = arr.mes;
      temp = parseInt(temp, 10);
      if(temp < 10){
        temp = temp.toString();
        temp = "0" + temp;
      }
      else{
        temp = temp.toString();
      }
      
      tempdia = arr.dia;
      tempdia = parseInt(tempdia, 10);
      if(tempdia < 10){
        tempdia = tempdia.toString();
        tempdia = "0" + tempdia;
      }
      else{
        tempdia = tempdia.toString();
      }
      
      $scope.pdata = arr.pdata;
      agendamentoService.setPdata($scope.pdata);

      dbRefAlunosPrivate.child(user).child("Pacotes").once('value', function(snap) {
        if(snap.val() != null){
          snap.forEach(function(child) {
            if(child.val().Categoria === categoryService.getCurrentCat() && child.val().Aulas > 0){
              paymentService.setFree();
              $scope.pvalor = "R$ 0,00";
              arr.valor = "0000";
              agendamentoService.setPvalor($scope.pvalor);
              presumovalor2.innerText = "0000";
              agendamentoService.setPresumovalor2(presumovalor2.innerText);
              agendamentoService.setPkt(child.val().Pacote);
              has_pkt = true;
            }
          });
          if(!has_pkt) {
            paymentService.resetFree();
            dbRefDisciplinas.on('child_added', function(snap) {
              if(arr.disciplina === snap.val().Nome && categoryService.getCurrentCat() === snap.val().Categoria){
                if($scope.palunos === "1"){
                  if(typeof agendamentoService.getPcd() != 'undefined'){
                    var desconto = parseInt(agendamentoService.getPcd(), 10);
                    var valorBruto = parseInt(snap.val().Valor, 10);
                    var descontado = valorBruto - (desconto * valorBruto / 100);
                    arr.valor = descontado.toString();
                    descontado = descontado / 100;
                    descontado = descontado.toFixed(2);
                    var resultado = descontado.toString().replace(".", ",");
                    $scope.pvalor = "R$ " + resultado;
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2.innerText = resultado.replace(",", "");
                    agendamentoService.setPresumovalor2(presumovalor2.innerText);
                  }
                  else{
                    var x = parseInt(snap.val().Valor, 10);
                    x = x/100;
                    x = x.toFixed(2);
                    $scope.pvalor = "R$ " +  x.toString().replace(".", ",");
                    agendamentoService.setPvalor($scope.pvalor);
                    arr.valor = snap.val().Valor;
                    presumovalor2.innerText = snap.val().Valor;
                    agendamentoService.setPresumovalor2(presumovalor2.innerText);
                  }
                }
                else if($scope.palunos === "2"){
                  var val = parseInt(snap.val().Valor,10);
                  val = val*2 - 4000;
                  if(typeof agendamentoService.getPcd() != 'undefined'){
                    var desconto = parseInt(agendamentoService.getPcd(), 10);
                    var descontado = val - (desconto * val / 100);
                    arr.valor = descontado.toString();
                    descontado = descontado / 100;
                    descontado = descontado.toFixed(2);
                    var resultado = descontado.toString().replace(".", ",");
                    $scope.pvalor = "R$ " + resultado;
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2.innerText = resultado.replace(",", "");
                    agendamentoService.setPresumovalor2(presumovalor2.innerText);
                  }
                  else{
                    var val_reais = val / 100;
                    val_reais = val_reais.toFixed(2);
                    $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2.innerText = val.toString();
                    arr.valor = val.toString();
                    agendamentoService.setPresumovalor2(presumovalor2.innerText);
                  }
                }
                else if($scope.palunos === "3"){
                  var val = parseInt(snap.val().Valor,10);
                  val = val*3 - 6000;
                  if(typeof agendamentoService.getPcd() != 'undefined'){
                    var desconto = parseInt(agendamentoService.getPcd(), 10);
                    var descontado = val - (desconto * val / 100);
                    arr.valor = descontado.toString();
                    descontado = descontado / 100;
                    descontado = descontado.toFixed(2);
                    var resultado = descontado.toString().replace(".", ",");
                    $scope.pvalor = "R$ " + resultado;
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2.innerText = resultado.replace(",", "");
                    agendamentoService.setPresumovalor2(presumovalor2.innerText);
                  }
                  else{
                    var val_reais = val / 100;
                    val_reais = val_reais.toFixed(2);
                    $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2.innerText = val.toString();
                    arr.valor = val.toString();
                    agendamentoService.setPresumovalor2(presumovalor2.innerText);
                  }
                }
                else if($scope.palunos === "4"){
                  var val = parseInt(snap.val().Valor,10);
                  val = val*4 - 8000;
                  if(typeof agendamentoService.getPcd() != 'undefined'){
                    var desconto = parseInt(agendamentoService.getPcd(), 10);
                    var descontado = val - (desconto * val / 100);
                    arr.valor = descontado.toString();
                    descontado = descontado / 100;
                    descontado = descontado.toFixed(2);
                    var resultado = descontado.toString().replace(".", ",");
                    $scope.pvalor = "R$ " + resultado;
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2.innerText = resultado.replace(",", "");
                    agendamentoService.setPresumovalor2(presumovalor2.innerText);
                  }
                  else{
                    var val_reais = val / 100;
                    val_reais = val_reais.toFixed(2);
                    $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2.innerText = val.toString();
                    arr.valor = val.toString();
                    agendamentoService.setPresumovalor2(presumovalor2.innerText);
                  }
                }
                else if($scope.palunos === "5"){
                  var val = parseInt(snap.val().Valor,10);
                  val = val*5 - 10000;
                  if(typeof agendamentoService.getPcd() != 'undefined'){
                    var desconto = parseInt(agendamentoService.getPcd(), 10);
                    var descontado = val - (desconto * val / 100);
                    arr.valor = descontado.toString();
                    descontado = descontado / 100;
                    descontado = descontado.toFixed(2);
                    var resultado = descontado.toString().replace(".", ",");
                    $scope.pvalor = "R$ " + resultado;
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2.innerText = resultado.replace(",", "");
                    agendamentoService.setPresumovalor2(presumovalor2.innerText);
                  }
                  else{
                    var val_reais = val / 100;
                    val_reais = val_reais.toFixed(2);
                    $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                    agendamentoService.setPvalor($scope.pvalor);
                    presumovalor2.innerText = val.toString();
                    arr.valor = val.toString();
                    agendamentoService.setPresumovalor2(presumovalor2.innerText);
                  }
                }
              }
            });
          }
        }
        else{
          paymentService.resetFree();
          dbRefDisciplinas.on('child_added', function(snap) {
            if(arr.disciplina === snap.val().Nome && categoryService.getCurrentCat() === snap.val().Categoria){
              if($scope.palunos === "1"){
                if(typeof agendamentoService.getPcd() != 'undefined'){
                  var desconto = parseInt(agendamentoService.getPcd(), 10);
                  var valorBruto = parseInt(snap.val().Valor, 10);
                  var descontado = valorBruto - (desconto * valorBruto / 100);
                  arr.valor = descontado.toString();
                  descontado = descontado / 100;
                  descontado = descontado.toFixed(2);
                  var resultado = descontado.toString().replace(".", ",");
                  $scope.pvalor = "R$ " + resultado;
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2.innerText = resultado.replace(",", "");
                  agendamentoService.setPresumovalor2(presumovalor2.innerText);
                }
                else{
                  var x = parseInt(snap.val().Valor, 10);
                  x = x/100;
                  x = x.toFixed(2);
                  $scope.pvalor = "R$ " +  x.toString().replace(".", ",");
                  agendamentoService.setPvalor($scope.pvalor);
                  arr.valor = snap.val().Valor;
                  presumovalor2.innerText = snap.val().Valor;
                  agendamentoService.setPresumovalor2(presumovalor2.innerText);
                }
              }
              else if($scope.palunos === "2"){
                var val = parseInt(snap.val().Valor,10);
                val = val*2 - 4000;
                if(typeof agendamentoService.getPcd() != 'undefined'){
                  var desconto = parseInt(agendamentoService.getPcd(), 10);
                  var descontado = val - (desconto * val / 100);
                  arr.valor = descontado.toString();
                  descontado = descontado / 100;
                  descontado = descontado.toFixed(2);
                  var resultado = descontado.toString().replace(".", ",");
                  $scope.pvalor = "R$ " + resultado;
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2.innerText = resultado.replace(",", "");
                  agendamentoService.setPresumovalor2(presumovalor2.innerText);
                }
                else{
                  var val_reais = val / 100;
                  val_reais = val_reais.toFixed(2);
                  $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2.innerText = val.toString();
                  arr.valor = val.toString();
                  agendamentoService.setPresumovalor2(presumovalor2.innerText);
                }
              }
              else if($scope.palunos === "3"){
                var val = parseInt(snap.val().Valor,10);
                val = val*3 - 6000;
                if(typeof agendamentoService.getPcd() != 'undefined'){
                  var desconto = parseInt(agendamentoService.getPcd(), 10);
                  var descontado = val - (desconto * val / 100);
                  arr.valor = descontado.toString();
                  descontado = descontado / 100;
                  descontado = descontado.toFixed(2);
                  var resultado = descontado.toString().replace(".", ",");
                  $scope.pvalor = "R$ " + resultado;
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2.innerText = resultado.replace(",", "");
                  agendamentoService.setPresumovalor2(presumovalor2.innerText);
                }
                else{
                  var val_reais = val / 100;
                  val_reais = val_reais.toFixed(2);
                  $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2.innerText = val.toString();
                  arr.valor = val.toString();
                  agendamentoService.setPresumovalor2(presumovalor2.innerText);
                }
              }
              else if($scope.palunos === "4"){
                var val = parseInt(snap.val().Valor,10);
                val = val*4 - 8000;
                if(typeof agendamentoService.getPcd() != 'undefined'){
                  var desconto = parseInt(agendamentoService.getPcd(), 10);
                  var descontado = val - (desconto * val / 100);
                  arr.valor = descontado.toString();
                  descontado = descontado / 100;
                  descontado = descontado.toFixed(2);
                  var resultado = descontado.toString().replace(".", ",");
                  $scope.pvalor = "R$ " + resultado;
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2.innerText = resultado.replace(",", "");
                  agendamentoService.setPresumovalor2(presumovalor2.innerText);
                }
                else{
                  var val_reais = val / 100;
                  val_reais = val_reais.toFixed(2);
                  $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2.innerText = val.toString();
                  arr.valor = val.toString();
                  agendamentoService.setPresumovalor2(presumovalor2.innerText);
                }
              }
              else if($scope.palunos === "5"){
                var val = parseInt(snap.val().Valor,10);
                val = val*5 - 10000;
                if(typeof agendamentoService.getPcd() != 'undefined'){
                  var desconto = parseInt(agendamentoService.getPcd(), 10);
                  var descontado = val - (desconto * val / 100);
                  arr.valor = descontado.toString();
                  descontado = descontado / 100;
                  descontado = descontado.toFixed(2);
                  var resultado = descontado.toString().replace(".", ",");
                  $scope.pvalor = "R$ " + resultado;
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2.innerText = resultado.replace(",", "");
                  agendamentoService.setPresumovalor2(presumovalor2.innerText);
                }
                else{
                  var val_reais = val / 100;
                  val_reais = val_reais.toFixed(2);
                  $scope.pvalor = "R$ " + val_reais.toString().replace(".", ",");
                  agendamentoService.setPvalor($scope.pvalor);
                  presumovalor2.innerText = val.toString();
                  arr.valor = val.toString();
                  agendamentoService.setPresumovalor2(presumovalor2.innerText);
                }
              }
            }
          });
        }
      });

    }, 0);

  });

//----------------------------------------------------------------------------//
$scope.confirmarAulaPocket = function() {

    var user = agendamentoService.getAlunoKey();
    var datas = [];
    var child = agendamentoService.getArrHors();
    var qtdpkt = agendamentoService.getPkt();
    child.qtdpkt = qtdpkt;
    datas.push(child);

    if (user && datas) {

      $ionicLoading.show({
        template: "<ion-spinner icon='ios-small'></ion-spinner><br><p class='custom'>Marcando<br>aula</p>"
      });

      if(typeof $rootScope.pushID === 'undefined'){
        $ionicPlatform.ready(function() {
           window.plugins.OneSignal.getIds(function(ids) {
             $rootScope.pushID = ids.userId;
           });
         });
      }

      var dbRefAlu = dbRefAlunos.child(user);
      var nomeAluno = agendamentoService.getAlunoNome();
      var notificationalunoID;
      var notificationprofessorID;

      agendamentoService.marcarAulaPacote(datas).then(function successCallback(response) {

        var resp = response.data;
        resp.forEach(function(child) {
          notificationsService.remindTeacherNotification(child.ProfPush, "Você tem uma aula hoje às " + child.HoraPush + " horas", child.DataPush).then(function(res) {
            notificationprofessorID = res.data.id;
            dbRefAulasProfessores.child(child.ProfID).child(child.KeyPush).update({"NotificationProfessor_ID" : notificationprofessorID});
          }, function(error) {
            //ionicToast.show('Erro ao enviar notificação.', 'middle', true, 2500);
          });
          notificationsService.newClassNotification(child.ProfPush, "Você tem uma nova aula marcada para o dia " + child.Data + " horas").then(function(re) {
            
          },
          function(e) {
            //ionicToast.show('Erro ao mandar notificação para o professor.', 'middle', true, 2500);
          });
        });
        

        $scope.modpdl.hide();
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go("tabsController.aulas");
        var alertPopup = $ionicPopup.alert({
           title: 'Concluído',
           template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Aula marcada!</p></div></div>",
           cssClass: 'popcustom',
           okType: 'customok'
         });
        $ionicHistory.clearCache();
        $ionicLoading.hide();

      }, function errorCallback(error) {
        var alertPopup = $ionicPopup.alert({
           title: 'Ooops!',
           template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>Ocorreu um erro! Por favor, tente novamente.</p></div></div>",
           cssClass: 'popcustom',
           okType: 'customok'
         });
        $ionicLoading.hide();

      });

    }

};


//----------------------------------------------------------------------------//
  $scope.check4email = function() {

    var user = firebase.auth().currentUser;
    var ferboden = false;
    var now = new Date();
    var dia = now.getDate();
    var mes = now.getMonth() + 1;
    var ano = now.getFullYear();
    var diaaula;
    var mesaula;
    var anoaula;

    if (user) {

      dbRefAulasAlunos.child(user.uid).on('child_added', function(snap) {
        if(typeof snap.val().Professor_Finalizou != 'undefined'){
          diaaula = parseInt(snap.val().Dia, 10);
          mesaula = parseInt(snap.val().Mês, 10);
          anoaula = parseInt(snap.val().Ano, 10);
          if(snap.val().Professor_Finalizou === "Não" && diaaula < dia && mesaula <= mes && anoaula <= ano || mesaula < mes && anoaula <= ano){

            var alertPopup = $ionicPopup.alert({
              title: 'Aviso',
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Constatamos que você ainda não finalizou uma aula que já ocorreu. Por favor, encerre-a na sua agenda antes de marcar outra aula.</p></div></div>",
              cssClass: 'popcustom',
              okType: 'customok'
            });

            ferboden = true;
          }
        }
      });

      $timeout(function() {
        if (user.emailVerified) {
          if((agendamentoService.getPcd() === "100" && !ferboden) || (paymentService.checkFree() && !ferboden)){
            $scope.confirmarAulaPocket();
          }
          else if(!ferboden){
            $scope.modpdl.show();
          }
        }
        else if(!user.emailVerified){
          ionicToast.show('É necessário verificar seu e-mail antes de marcar uma aula. Por favor, confirme seu e-mail e faça login novamente.', 'middle', true, 2500);
        }
      }, 0);
    }
  };
//----------------------------------------------------------------------------//
  $scope.interInicio = function() {

    $scope.modpdl.hide();
    $state.go('tabsController.meusCartoes');

  };
//----------------------------------------------------------------------------//
  $scope.inicioBoleto = function() {

    $scope.modpdl.hide();
    $state.go('tabsController.boleto');

  };

//----------------------------------------------------------------------------//
  $scope.interTrans = function() {
    $scope.modpdl.hide();
    $state.go('tabsController.transfer');
  };
  

})
