angular.module('app.pacotesCtrl', [])

//**************************_PACOTES CONTROLLER*******************************//

.controller('pacotesCtrl', function($scope, $rootScope, $document, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, ionicToast, pacotesService, $ionicPopup, dataService, $firebaseArray) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefPacotes = dbRefRoot.child('pacotes');

  $ionicModal.fromTemplateUrl('pacotes/pktmodal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.modal != 'undefined'){
      $scope.modal.remove();
    }
  });

  $scope.$on("$ionicView.beforeEnter", function(){
    $scope.loadpkt = true;
    $scope.pktcat = categoryService.parseCat(categoryService.getCat());
    var cat = categoryService.getCat();
    pacotesService.setPktCat(cat);

    dbRefPacotes.on('child_added', function(snap) {
      if(cat === "Fundamental" && snap.key === "4_aulas"){
        $timeout(function() {
          $scope.pkt4 = snap.val().Fundamental;
        }, 0);
      }
      if(cat === "Médio" && snap.key === "4_aulas"){
        $timeout(function() {
          $scope.pkt4 = snap.val().Médio;
        }, 0);
      }
      if(cat === "Vestibular" && snap.key === "4_aulas"){
        $timeout(function() {
          $scope.pkt4 = snap.val().Vestibular;
        }, 0);
      }
      if(cat === "Superior" && snap.key === "4_aulas"){
        $timeout(function() {
          $scope.pkt4 = snap.val().Superior;
        }, 0);
      }
      if(cat === "Idiomas" && snap.key === "4_aulas"){
        $timeout(function() {
          $scope.pkt4 = snap.val().Idiomas;
        }, 0);
      }
      if(cat === "Cursos" && snap.key === "4_aulas"){
        $timeout(function() {
          $scope.pkt4 = snap.val().Cursos;
        }, 0);
      }
      //
      if(cat === "Fundamental" && snap.key === "8_aulas"){
        $timeout(function() {
          $scope.pkt8 = snap.val().Fundamental;
        }, 0);
      }
      if(cat === "Médio" && snap.key === "8_aulas"){
        $timeout(function() {
          $scope.pkt8 = snap.val().Médio;
        }, 0);
      }
      if(cat === "Vestibular" && snap.key === "8_aulas"){
        $timeout(function() {
          $scope.pkt8 = snap.val().Vestibular;
        }, 0);
      }
      if(cat === "Superior" && snap.key === "8_aulas"){
        $timeout(function() {
          $scope.pkt8 = snap.val().Superior;
        }, 0);
      }
      if(cat === "Idiomas" && snap.key === "8_aulas"){
        $timeout(function() {
          $scope.pkt8 = snap.val().Idiomas;
        }, 0);
      }
      if(cat === "Cursos" && snap.key === "8_aulas"){
        $timeout(function() {
          $scope.pkt8 = snap.val().Cursos;
        }, 0);
      }
    });

    var qtdshowpkt = 0;
    var user = firebase.auth().currentUser;
    $scope.cards = [];
    $scope.nenhumCard = false;
    if (user) {
      user = user.uid;
      dbRefAlunosPrivate.child(user).child("Pacotes").on('child_added', function(snapshot) {
        if(snapshot.val().Categoria === cat){
          qtdshowpkt += snapshot.val().Aulas;
        }
      });
      $timeout(function() {
        $scope.loadpkt = false;
        $scope.aulasqtd = qtdshowpkt;
      }, 1000);

      if(paymentService.hasCusId()){
        //cusId = paymentService.getCusId();
        $scope.cards = $firebaseArray(dbRefAlunosPrivate.child(user).child("creditCards"));
        $ionicLoading.hide();
      }
      else{
       $scope.nenhumCard = true;
       $ionicLoading.hide();
      }


    }
    

  });
//----------------------------------------------------------------------------//
  $scope.escolheuPkt = function(qtd) {

   pacotesService.setPkt(qtd);
   //pacotesService.setPktCat($scope.pktcat);
   if(qtd === '8'){
     pacotesService.setPktVal($scope.pkt8);
   }
   else{
     pacotesService.setPktVal($scope.pkt4);
   }

   var myPopup = $ionicPopup.show({
   template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-download-outline icon-pop'></i></div></div></div><p class='custom'>Ao comprar este pacote, você poderá marcar <strong>" + qtd + "</strong> aulas de qualquer disciplina da categoria <strong>" + $scope.pktcat + "</strong> a qualquer momento. Deseja continuar?</p></div></div>",
   title: qtd + ' Aulas',
   cssClass: 'popcustom',
   scope: $scope,
   buttons: [
     { text: 'Cancelar', type: 'customconfirm' },
     {
       text: '<b>Sim</b>',
       type: 'customconfirm',
       onTap: function(e) {
         $scope.modal.show();
       }
     }
   ]
   });

  };

//----------------------------------------------------------------------------//
  function payPkt(id, cvc) {
    $ionicLoading.show({
      template: "<ion-spinner icon='ios-small'></ion-spinner><br><p class='custom'>Finalizando</p>"
    });
    var qtdpkt = pacotesService.getPkt();
    var pktcat = pacotesService.getPktCat();
    var pkt = pacotesService.getPkt() + "_Aulas_" + pktcat;
    var val = pacotesService.getPktVal();
    val = parseInt(val, 10);
    val = val * 100;
    val = val.toString(); //ADDED
    var chargeID;
    var authToken = dataService.getAuthToken();
    var cusId = paymentService.getCusId();
    //var pt = $rootScope.cardtokenpkt;
    var user = firebase.auth().currentUser;
    if (user && user.emailVerified) {

      var aula = {
        "ownId": pkt,
        "price": val,
        "subject": child.Subject,
        "cusId": cusId,
        "merchId": teacherService.getMerchId(),
        "merchAmount": child.ValorProf
      }

      paymentService.createOrder("Pacote de " + qtdpkt + " Aulas", val, authToken).then(function(resposta) {
        chargeID = resposta.data.data.charge_id;
        $http({
            url: 'https://modular-aulas.herokuapp.com/pagar',
            method: "POST",
            data: { 'id' : chargeID , 'street' : rua, 'token' :  pt, 'number' : numero, 'neighborhood' : bairro, 'zipcode' : cep, 'city' : cidade, 'state' : estado, 'name' : nome, 'email' : email, 'cpf' : cpf, 'birth' : birth, 'phone_number' : telefone}
        })
        .then(function(response2) {
          dbRefAlunosPrivate.child(user.uid).child("Pacotes").child(pkt).once('value', function(snap) {
            if(snap.val()){
              dbRefAlunosPrivate.child(user.uid).child("Pacotes").child(pkt).update({"Categoria" : pktcat, "Aulas" : snap.val().Aulas += qtdpkt, "Pacote" : qtdpkt});
            }
            else{
              dbRefAlunosPrivate.child(user.uid).child("Pacotes").child(pkt).update({"Categoria" : pktcat, "Aulas" : qtdpkt, "Pacote" : qtdpkt});
            }
            $ionicLoading.hide();
            $scope.modal.hide();
            $ionicHistory.nextViewOptions({
               disableBack: true
             });
            $state.go('tabsController.aulas');
            var alertPopup = $ionicPopup.alert({
             title: 'Concluído',
             template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Pagamento realizado com sucesso. Você já pode marcar suas aulas!</p></div></div>",
             cssClass: 'popcustom',
             okType: 'customok'
           });
          });
        });
      });

    }
    else{
      ionicToast.show('Confirme seu e-mail antes de continuar', 'middle', false, 2500);
    }

  };
//----------------------------------------------------------------------------//
  $scope.escolheuCard = function(id, b, l4, cvc) {
    var pkt = pacotesService.getPkt();
    var val = pacotesService.getPktVal();
    val = parseInt(val, 10);
    var cardData;
    var myPopup = $ionicPopup.show({
    template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-card icon-pop'></i></div></div></div><p class='custom'>Confirmar a compra do pacote utilizando este cartão (<strong>•••" + l4 + "</strong>)?</p></div></div>",
    title: b,
    cssClass: 'popcustom',
    scope: $scope,
    buttons: [
      { text: 'Cancelar', type: 'customconfirm' },
      {
        text: '<b>Sim</b>',
        type: 'customconfirm',
        onTap: function(e) {
          payPkt(id, cvc);
        }
      }
    ]
    });
  };

})
