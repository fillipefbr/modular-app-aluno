angular.module('app.categoriasCtrl', [])

//**************************_CATEGORIAS CONTROLLER_****************************//

.controller('categoriasCtrl', function($scope, $rootScope, $document, $timeout, categoryService, $ionicLoading, $state, $cordovaNetwork, ionicToast, ConnectivityMonitor, agendamentoService, dataService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');

  $scope.$on("$ionicView.beforeEnter", function(){

    var user = firebase.auth().currentUser;
    if (user) {
      user = user.uid;
      var categ;
      var dbRefA = dbRefAlunosPrivate.child(user);
      dbRefA.once('value', function(snap) {

        categ = snap.val().Categoria;

        if(categ === "EF"){
          $scope.mychoice = "EF";
        }

        else if(categ === "1EM"){
          $scope.mychoice = "1EM";
        }

        else if(categ === "2EM"){
          $scope.mychoice = "2EM";
        }

        else if(categ === "3EM"){
          $scope.mychoice = "3EM";
        }

        else if(categ === "SUP"){
          $scope.mychoice = "SUP";
        }

        else if(categ === "VE"){
          $scope.mychoice = "VE";
        }

        else if(categ === "IDI"){
          $scope.mychoice = "IDI";
        }

        else if(categ === "CV"){
          $scope.mychoice = "CV";
        }

      });
    }

  });

//----------------------------------------------------------------------------//
  $scope.setCat = function(categ) {

    var user = firebase.auth().currentUser;
    if (user) {
      user = user.uid;
      var span = $document[0].getElementById('usercategoria');
      categoryService.setCat(categ);
      categoryService.changedCat();
      var dbRefA;

      dbRefAlunos.child(user).child("Categoria").set(categ);
      dbRefAlunosPrivate.child(user).child("Categoria").set(categ);

      if(categ === "EF"){
        span.innerText = "Ensino Fundamental";
        $scope.mychoice = "EF";
        $rootScope.categoriaAluno = "EF";
      }

      else if(categ === "1EM"){
        span.innerText = "1° Ano Ensino Médio";
        $scope.mychoice = "1EM";
        $rootScope.categoriaAluno = "1EM";
      }

      else if(categ === "2EM"){
        span.innerText = "2° Ano Ensino Médio";
        $scope.mychoice = "2EM";
        $rootScope.categoriaAluno = "2EM";
      }

      else if(categ === "3EM"){
        span.innerText = "3° Ano Ensino Médio";
        $scope.mychoice = "3EM";
        $rootScope.categoriaAluno = "3EM";
      }

      else if(categ === "SUP"){
        span.innerText = "Ensino Superior";
        $scope.mychoice = "SUP";
        $rootScope.categoriaAluno = "SUP";
      }

      else if(categ === "VE"){
        span.innerText = "Vestibular/Enem";
        $scope.mychoice = "VE";
        $rootScope.categoriaAluno = "VE";
      }

      else if(categ === "IDI"){
        span.innerText = "Idiomas";
        $scope.mychoice = "IDI";
        $rootScope.categoriaAluno = "IDI";
      }

      /*else if(categ === "CV"){
        span.innerText = "Cursos";
        $scope.mychoice = "CV";
        $rootScope.categoriaAluno = "CV";
      }*/

    }

  };




})
