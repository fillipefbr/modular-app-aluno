angular.module('app.calendarioPessoalCtrl', [])

//************************_CALENDARS CONTROLLER*******************************//

.controller('calendarioPessoalCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, agendamentoService, $ionicPopup, ionicToast, dataService) {

  $scope.data = {};

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

  $ionicModal.fromTemplateUrl('calendariopessoal/diaPessoalDetalhes.html', {
    scope: $scope
  }).then(function(modul) {
    $scope.modul = modul;
  });

  $scope.$on('$destroy', function() {
    $scope.modul.remove();
  });


//----------------------------------------------------------------------------//
  var currentDate = new Date();
  var date = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
  $scope.date = date;
  $scope.selectedDate1;
  $scope.selectedDate2;
  $scope.dataCompleta = "";
  $scope.horaCompleta = "";

  var weekday = new Array(7);
  weekday[0]=  "Domingo";
  weekday[1] = "Segunda-Feira";
  weekday[2] = "Terça-Feira";
  weekday[3] = "Quarta-Feira";
  weekday[4] = "Quinta-Feira";
  weekday[5] = "Sexta-Feira";
  weekday[6] = "Sábado";


  var monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
    "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
  ];

$scope.onezoneDatepicker3 = {
      date: date,
      mondayFirst: true,
      months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
      daysOfTheWeek: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
      startDate: new Date(1989, 1, 26),
      endDate: new Date(2024, 1, 26),
      disablePastDays: false,
      disableSwipe: false,
      disableWeekend: false,
      showDatepicker: false,
      showTodayButton: true,
      calendarMode: true,
      hideCancelButton: false,
      hideSetButton: false,
      callback:
        function() {

        } ,
      highlights: [

        ]
  };

  $scope.onezoneDatepicker3.showDatepicker = true;


  $scope.$on("$ionicView.beforeEnter", function(ev){
    if(ev.targetScope !== $scope)
        return;
    
     var userProf;
     var day;
     var month;
     var year;
     var today = new Date();
     var dd = today.getDate();
     var mm = today.getMonth()+1;
     var yyyy = today.getFullYear();
     $scope.onezoneDatepicker3.highlights = [];
     $scope.onezoneDatepicker3.disableDates = [];
     var user = firebase.auth().currentUser;

     if (user) {
        user = user.uid;

        var dbRefProf = dbRefAulasAlunos.child(user);

         dbRefProf.on('child_added', function(snapshoot) {

           day = snapshoot.val().Dia;
           day = parseInt(day, 10);
           month = snapshoot.val().Mês;
           month = parseInt(month, 10);
           year = snapshoot.val().Ano;
           year = parseInt(year, 10);

           if(day < dd && month <= mm && year <= yyyy){
             //dbRefProf.child(snapshoot.key).remove();
             
           }

           var h1 = {
             date: new Date(year, month - 1, day),
             color: '#3acfd5',
             textColor: '#fff'
           };

           $scope.onezoneDatepicker3.highlights.push(h1);

         });


        $scope.onezoneDatepicker3.showDatepicker = true;

      }
    
  });

//----------------------------------------------------------------------------//
$scope.onezoneDatepicker3.callback = function() {

    $scope.marcadas = [];
    var dbRefP;
    var dbRefH;
    var dbRefPendentes;
    var mensagem;
    var fach;
    $scope.data.nenhumamarcada = false;
    $scope.data.spinpessoal = true;
    var achou = false;

    var user = firebase.auth().currentUser;

    if (user) {
      user = user.uid;
      dbRefP = dbRefAulasAlunos.child(user);
    }

    var dia = $scope.onezoneDatepicker3.date.getDate();
    var mes = $scope.onezoneDatepicker3.date.getMonth();

    var diaString = dia.toString();
    var mesString = mes + 1;
    mesString = mesString.toString();

    $scope.diaescolhido2 = $scope.onezoneDatepicker3.date.getDate();
    $scope.dayescolhido2 = weekday[$scope.onezoneDatepicker3.date.getDay()];
    $scope.mesescolhido2 = monthNames[$scope.onezoneDatepicker3.date.getMonth()];
    $scope.diaescolhido22 = $scope.diaescolhido2.toString();
    $scope.mesescolhido22 = $scope.onezoneDatepicker3.date.getMonth() + 1;
    $scope.mesescolhido22 = $scope.mesescolhido22.toString();

    $scope.data.dataEscolhida2 = $scope.diaescolhido2 + " de " +  $scope.mesescolhido2;

    $scope.modul.show();

    $scope.navtitleprof = $scope.data.dataEscolhida2;
    var mesint = parseInt($scope.mesescolhido22, 10);
    var diaint = parseInt($scope.diaescolhido2, 10);

    if(diaint < 10){
      $scope.diaescolhido2 = "0" + $scope.diaescolhido2;
    }

    if(mesint < 10){
      $scope.mesescolhido22 = "0" + $scope.mesescolhido22;
    }

    $scope.datadiacircle = $scope.diaescolhido2 + "/" + $scope.mesescolhido22;
    $scope.diacircle = $scope.dayescolhido2;
    var add = false;

    dbRefP.on('child_added', function(snaap) {

      if(diaString === snaap.val().Dia && mesString === snaap.val().Mês){

        mensagem = snaap.val().Hora;
        fach = snaap.val().Disciplina;
        var marcada = {"hora" : mensagem, "prof" : snaap.val().Professor, "key" : snaap.key, "fach" : fach};
        $scope.marcadas.push(marcada);
        $scope.data.spinpessoal = false;
        achou = true;

      }

    });


    $timeout(function() {
      if(achou === false){
        $scope.data.spinpessoal = false;
        $scope.data.nenhumamarcada = true;
      }
    }, 3000);

 };


//----------------------------------------------------------------------------//
$scope.escolheuMarcada = function(professor, key, fach, aluno, index) {

  if(professor === "Modular" && fach === "Tour pela UnB"){
    ionicToast.show('Sua presença está confirmada. Aguarde o dia do evento.', 'middle', false, 2500);
  }
  else{
    //agendamentoService.setAlunoKey(aluno);
    agendamentoService.setMarcadaKey(key);
    //agendamentoService.setAlunoNome(parseNomeAluno(aluno));
    agendamentoService.setCancelarIndex(index);
    $scope.modul.hide();

    $state.go('tabsController.marcadas');

  }

};





})
