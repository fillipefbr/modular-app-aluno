angular.module('app.ConnectivityMonitor', [])

//**************************_INTERNET CONNECTION******************************//
.factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork, ionicToast, $ionicPopup){

  return {
    isOnline: function(){
      if(ionic.Platform.isWebView()){
        return $cordovaNetwork.isOnline();
      }
    },
    isOffline: function(){
      if(ionic.Platform.isWebView()){
        return !$cordovaNetwork.isOnline();
      }
    },
    startWatching: function(){
        if(ionic.Platform.isWebView()){

          $rootScope.$on('$cordovaNetwork:online', function(event, networkState){

          });

          $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
						//ionicToast.show('Sem conexão com a internet. Verifique sua conexão antes de continuar', 'middle', false, 2500);
            var alertPopup = $ionicPopup.alert({
             title: 'Sem Conexão',
             template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-social-rss icon-pop'></i></div></div></div><p class='custom'>Sem conexão com a internet. Por favor, verifique sua conexão antes de continuar</p></div></div>",
             cssClass: 'popcustom',
             okType: 'customok'
           });
          });

        }
    }
  }
})
