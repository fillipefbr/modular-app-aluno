angular.module('app.cadastroService', [])

//*****************************_CADASTRO_**************************************//
.factory('cadastroService', function($rootScope, categoryService, $http) {

  var nome;

	return {
		createUser: function(profile){
      return $http({
          method: 'POST',
          url: 'https://us-central1-modular-c3962.cloudfunctions.net/createAluno',
          data: profile
        });
		},
    setNome: function(nm){
      nome = nm;
		}
	}
})
