angular.module('app.favoritesService', [])

//*****************************_FAVORITES_************************************//
.factory('favoritesService', function($rootScope) {

	var loc;
  var prof;
	var hasFav = false;

	return {
		setLoc: function(local){
			loc = local;
		},
		getLoc: function(){
      return loc;
		},
    setProf: function(professor){
			prof = professor;
		},
		getProf: function(){
      return prof;
		},
		setHasFavTrue: function(){
			hasFav = true;
		},
		setHasFavFalse: function(){
      hasFav = false;
		},
		getHasFav: function(){
      return hasFav;
		}
	}
})
