angular.module('app.categoryService', [])

//*****************************_Category_*************************************//
.factory('categoryService', function($rootScope) {

	var cat;
	var currentCat;
	var changedCat = false;

	return {
		setCat: function(nivel){

			if(nivel === "1EM"){
				cat = "Médio";
				$rootScope.categoriaAluno = "1EM";
			}

			else if(nivel === "2EM"){
				cat = "Médio";
				$rootScope.categoriaAluno = "2EM";
			}

			else if(nivel === "3EM"){
				cat = "Médio";
				$rootScope.categoriaAluno = "3EM";
			}

			else if(nivel === "SUP"){
				cat = "Superior";
				$rootScope.categoriaAluno = "SUP";
			}

			else if(nivel === "EF"){
				cat = "Fundamental";
				$rootScope.categoriaAluno = "EF";
			}

			else if(nivel === "IDI"){
				cat = "Idiomas";
				$rootScope.categoriaAluno = "IDI";
			}

			else if(nivel === "VE"){
				cat = "Vestibular";
				$rootScope.categoriaAluno = "VE";
			}

			else if(nivel === "CV"){
				cat = "Cursos";
				$rootScope.categoriaAluno = "CV";
			}

		},
		getCat: function(){
      return cat;
		},
		setCurrentCat: function(nivel){
      currentCat =  nivel;
		},
		getCurrentCat: function(){
      return currentCat;
		},
		hasCatChanged: function(){
      return changedCat;
		},
		changedCat: function(){
      changedCat = true;
		}
		,
		changedCatFalse: function(){
      changedCat = false;
		},
		parseCat: function(cc){
			if(cc === "Médio"){
				return "Ensino Médio";
			}
			else if(cc === "Fundamental"){
				return "Ensino Fundamental";
			}
			else if(cc === "Vestibular"){
				return "Pré-Vestibular";
			}
			else if(cc === "Superior"){
				return "Ensino Superior";
			}
			else if(cc === "Idiomas"){
				return "Idiomas";
			}
			else if(cc === "Cursos"){
				return "Cursos";
			}
		},
		reParseCat: function(rc){
			if(rc === "Ensino Médio"){
				return "Médio";
			}
			else if(rc === "Ensino Fundamental"){
				return "Fundamental";
			}
			else if(rc === "Pré-Vestibular"){
				return "Vestibular";
			}
			else if(rc === "Ensino Superior"){
				return "Superior";
			}
			else if(rc === "Idiomas"){
				return "Idiomas";
			}
			else if(rc === "Cursos"){
				return "Cursos";
			}
		},
		parseCatShort: function(ccsh){
			if(ccsh === "Médio"){
				return "1EM";
			}
			else if(ccsh === "Fundamental"){
				return "EF";
			}
			else if(ccsh === "Vestibular"){
				return "VE";
			}
			else if(ccsh === "Superior"){
				return "SUP";
			}
			else if(ccsh === "Idiomas"){
				return "IDI";
			}
			else if(ccsh === "Cursos"){
				return "CV";
			}
		}
	}
})
