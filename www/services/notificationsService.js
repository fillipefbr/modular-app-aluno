angular.module('app.notificationsService', [])

//************************_PUSH NOTIFICATIONS_********************************//
.factory('notificationsService', function($rootScope, $http) {

	var badgeCount = 1;

	return {
		canceledClassNotification: function(player_id, message){
			return $http({
        method: 'POST',
        url: 'https://onesignal.com/api/v1/notifications',
        data: { 'app_id' : "bd15488d-02c6-4a8d-b66f-4d0d317fec85" , 'include_player_ids' : [player_id], "contents": {"en": message} , "headings": {"en": "Aula Cancelada"}, "ios_badgeType" : "Increase", "ios_badgeCount" : badgeCount }
      });
		},
		newClassNotification: function(player_id, message){
			return $http({
        method: 'POST',
        url: 'https://onesignal.com/api/v1/notifications',
        data: { 'app_id' : "bd15488d-02c6-4a8d-b66f-4d0d317fec85" , 'include_player_ids' : [player_id], "contents": {"en": message} , "headings": {"en": "Nova Aula"}, "ios_badgeType" : "Increase", "ios_badgeCount" : badgeCount }
      });
		},
		mailTeacher : function(email, nome, data){
			return $http({
					url: 'https://modular-aulas.herokuapp.com/emailprofessor',
					method: "POST",
					data: { 'email' : email, 'nome' : nome , 'data' : data}
			});
		},
		remindTeacherNotification: function(player_id, message, time){
			return $http({
        method: 'POST',
        url: 'https://onesignal.com/api/v1/notifications',
        data: { 'app_id' : "bd15488d-02c6-4a8d-b66f-4d0d317fec85" , 'include_player_ids' : [player_id], "contents": {"en": message} , "headings": {"en": "Lembrete"}, "ios_badgeType" : "Increase", "ios_badgeCount" : badgeCount, "send_after" : time }
      });
		},
		remindStudentNotification: function(player_id, message, time){
		 	return $http({
        method: 'POST',
        url: 'https://onesignal.com/api/v1/notifications',
        data: { 'app_id' : "c0b2933e-7141-492f-9a00-aa6b2a290b9a" , 'include_player_ids' : [player_id], "contents": {"en": message} , "headings": {"en": "Lembrete"}, "ios_badgeType" : "Increase", "ios_badgeCount" : badgeCount, "send_after" : time }
      });
		},
		cancelStudentNotification: function(id){
		 	return $http({
        method: 'DELETE',
        url: 'https://onesignal.com/api/v1/notifications/' + id + '?app_id=' + 'c0b2933e-7141-492f-9a00-aa6b2a290b9a'
      });
		},
		cancelTeacherNotification: function(id){
		 	return $http({
        method: 'DELETE',
        url: 'https://onesignal.com/api/v1/notifications/' + id + '?app_id=' + 'bd15488d-02c6-4a8d-b66f-4d0d317fec85'
      });
		}
	}
})
