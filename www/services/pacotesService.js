angular.module('app.pacotesService', [])

//*****************************_PACOTES_**************************************//
.factory('pacotesService', function($rootScope, categoryService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefPacotes = dbRefRoot.child('pacotes');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  var pkt;
  var pktval;
  var pktcat;

	return {
		getPkt: function(){
      return pkt;
		},
    setPkt: function(pp){
      pkt = pp;
		},
    getPktVal: function(){
      return pktval;
		},
    setPktVal: function(ppval){
      pktval = ppval;
		},
    getPktCat: function(){
      return pktcat;
		},
    setPktCat: function(ppcat){
      pktcat = ppcat;
		}
	}
})
