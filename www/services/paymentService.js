angular.module('app.paymentService', [])

//*****************************_PAYMENT_**************************************//
.factory('paymentService', function($http) {

	var id;
	var free = false;
	var hasCusId = false;
	var cusId;
	var cc = {};

	return {
		createCustomer: function(cus, idToken){
			return $http({
				method: 'POST',
				url: 'https://us-central1-modular-c3962.cloudfunctions.net/createCustomer',
				headers: {
					'Content-Type': 'application/json',
					'Authorization':  'Bearer ' + idToken
				},
				data: {
					"ownId": cus.ownId,
					"fullname": cus.fullname,
					"email": cus.email,
					"birthDate": cus.birthDate,
					"taxDocument": {
						"type": "CPF",
						"number": cus.cpf
					},
					"phone": {
						"countryCode": "55",
						"areaCode": cus.phone.areaCode,
						"number": cus.phone.number
					},
					"shippingAddress": {
						"city": cus.shippingAddress.city,
						"complement": cus.shippingAddress.complement,
						"district": cus.shippingAddress.district,
						"street": cus.shippingAddress.street,
						"streetNumber": cus.shippingAddress.streetNumber,
						"zipCode": cus.shippingAddress.zipCode,
						"state": cus.shippingAddress.state,
						"country": "BRA"
					}
				}
			});
		},
		createCustomerWithCreditCard: function(cus, idToken){
			return $http({
				method: 'POST',
				url: 'https://us-central1-modular-c3962.cloudfunctions.net/createCustomerWithCreditCard',
				//url: 'http://localhost:5000/modular-c3962/us-central1/createCustomerWithCreditCard',
				headers: {
					'Content-Type': 'application/json',
					'Authorization':  'Bearer ' + idToken
				},
				data: {
					"ownId": cus.ownId,
					"fullname": cus.fullname,
					"email": cus.email,
					"birthDate": cus.birthDate,
					"taxDocument": {
						"type": "CPF",
						"number": cus.holder.cpf
					},
					"phone": {
						"countryCode": "55",
						"areaCode": cus.holder.phone.areaCode,
						"number": cus.holder.phone.number
					},
					"shippingAddress": {
						"city": cus.shippingAddress.city,
						"complement": cus.shippingAddress.complement,
						"district": cus.shippingAddress.district,
						"street": cus.shippingAddress.street,
						"streetNumber": cus.shippingAddress.streetNumber,
						"zipCode": cus.shippingAddress.zipCode,
						"state": cus.shippingAddress.state,
						"country": "BRA"
					},
					"fundingInstrument":{  
						"method":"CREDIT_CARD",
						"creditCard":{  
						   "expirationMonth": cus.expirationMonth,
						   "expirationYear": cus.expirationYear,
						   "number": cus.number,
						   "cvc": cus.cvc,
						   "holder":{  
							  "fullname": cus.holder.fullname,
							  "birthdate": cus.holder.birthdate,
							  "taxDocument":{  
								 "type":"CPF",
								 "number": cus.holder.cpf
							  },
							  "billingAddress":{  
								 "city": cus.shippingAddress.city,
								 "complement": cus.shippingAddress.complement,
								 "district": cus.shippingAddress.district,
								 "street": cus.shippingAddress.street,
								 "streetNumber": cus.shippingAddress.streetNumber,
								 "zipCode": cus.shippingAddress.zipCode,
								 "state": cus.shippingAddress.state,
								 "country":"BRA"
							  },
							  "phone":{  
								 "countryCode":"55",
								 "areaCode": cus.holder.phone.areaCode,
								 "number": cus.holder.phone.number
							  }
						   }
						}
					}
				}
			});
		},
		addCustomerCard: function(cc, idToken){
			return $http({
				method: 'POST',
				url: 'https://us-central1-modular-c3962.cloudfunctions.net/addCustomerCard',
				//url: 'http://localhost:5000/modular-c3962/us-central1/addCustomerCard',
				headers: {
					'Content-Type': 'application/json',
					'Authorization':  'Bearer ' + idToken
				},
				data: {
					"cusId": cc.cusId,
					"expirationMonth": cc.expirationMonth,
					"expirationYear": cc.expirationYear,
					"number": cc.number,
					"cvc": cc.cvc,
					"holder": {
						"fullname": cc.holder.fullname,
						"birthdate": cc.holder.birthdate,
						"taxDocument": {
							"type": "CPF",
							"number": cc.holder.cpf
						},
						"phone": {
							"countryCode": "55",
							"areaCode": cc.holder.phone.areaCode,
							"number": cc.holder.phone.number
						}
					}
				}
			});
		},
		deleteCustomerCard: function(crc, idToken){
			return $http({
				method: 'POST',
				url: 'https://us-central1-modular-c3962.cloudfunctions.net/deleteCustomerCard',
				//url: 'http://localhost:5000/modular-c3962/us-central1/deleteCustomerCard',
				headers: {
					'Content-Type': 'application/json',
					'Authorization':  'Bearer ' + idToken
				},
				data: {
					"crc" : crc
				}
			});
		},
		getCustomers: function(idToken){
			return $http({
				method: 'GET',
				url: 'https://us-central1-modular-c3962.cloudfunctions.net/getCustomers',
				headers: {
					'Authorization':  'Bearer ' + idToken
				}
			});
		},
		getCustomer: function(cusId, idToken){
			return $http({
				method: 'POST',
				url: 'https://us-central1-modular-c3962.cloudfunctions.net/getCustomer',
				//url: 'http://localhost:5000/modular-c3962/us-central1/getCustomer',
				headers: {
					'Content-Type': 'application/json',
					'Authorization':  'Bearer ' + idToken
				},
				data: {
					"cusId" : cusId
				}
			});
		},
		createOrder: function(data, idToken){
			return $http({
				method: 'POST',
				url: 'https://us-central1-modular-c3962.cloudfunctions.net/createOrder',
				//url: 'http://localhost:5000/modular-c3962/us-central1/createOrder',
				headers: {
					'Content-Type': 'application/json',
					'Authorization':  'Bearer ' + idToken
				},
				data: {
					"ownId": data.ownId,
					"price": data.price,
					"subject": data.subject,
					"cusId": data.cusId,
					"merchId": data.merchId,
					"merchAmount": data.merchAmount
				}
			});
		},
		getOrder: function(orderId, idToken){
			return $http({
				method: 'POST',
				url: 'https://us-central1-modular-c3962.cloudfunctions.net/getOrder',
				headers: {
					'Content-Type': 'application/json',
					'Authorization':  'Bearer ' + idToken
				},
				data: {
					"orderId" : orderId
				}
			});
		},
		setCusId: function(cid){
			cusId = cid;
			hasCusId = true;
		},
		resetCusId: function(){
			var x;
			cusId = x;
			hasCusId = false;
		},
		getCusId: function(){
			return cusId;
		},
		hasCusId: function(){
			return hasCusId;
		},
		setCc: function(c){
			cc = c;
		},
		getCc: function(){
			return cc;
		},
		/*createTransaction: function(aula, valor, idToken){
			return $http({
			url: 'https://us-central1-modular-c3962.cloudfunctions.net/createCharge',
					method: "POST",
					headers: {
					'Authorization': 'Bearer ' + idToken
				},
					data: { 'name' : aula , 'value' : valor }
			});
		},*/
		/*cancelTransaction : function(chargeID, idToken){
			return $http({
				url: 'https://us-central1-modular-c3962.cloudfunctions.net/cancelCharge',
				method: "POST",
				headers: {
		          'Authorization': 'Bearer ' + idToken
		        },
				data: { 'id' : chargeID }
			});
		},*/
		checkFree: function(){
			return free;
		},
		setFree: function(){
			free = true;
		},
		resetFree: function(){
			free = false;
		},
		createPaymentWithBankBillet: function(id, idToken){
	      return $http({
			  url: 'https://us-central1-modular-c3962.cloudfunctions.net/createPaymentWithBankBillet',
			  //url: 'http://localhost:5000/modular-c3962/us-central1/createPaymentWithBankBillet',
	          method: "POST",
	          headers: {
				'Content-Type': 'application/json',
		        'Authorization': 'Bearer ' + idToken
		      },
	          data: { 'orderId' : id }
	      });
	    },
	    createPaymentWithCreditCard: function(orderId, cus, idToken){
	      return $http({
			  url: 'https://us-central1-modular-c3962.cloudfunctions.net/createPaymentWithCreditCard',
			  //url: 'http://localhost:5000/modular-c3962/us-central1/createPaymentWithCreditCard',
	          method: "POST",
	          headers: {
				'Content-Type': 'application/json',
		        'Authorization': 'Bearer ' + idToken
		      },
	          data: {
					"orderId" : orderId,
					"id": cus.id,
					"cvc": cus.cvc,
					"holder": {
						"fullname": cus.holder.fullname,
						"birthdate": cus.holder.birthdate,
						"taxDocument": {
							"type": "CPF",
							"number": cus.holder.cpf
						},
						"phone": {
							"areaCode": cus.holder.phone.areaCode,
							"number": cus.holder.phone.number
						},
						"billingAddress":{  
							"city": cus.holder.shippingAddress.city,
							"complement": cus.holder.shippingAddress.complement,
							"district": cus.holder.shippingAddress.district,
							"street": cus.holder.shippingAddress.street,
							"streetNumber": cus.holder.shippingAddress.streetNumber,
							"zipCode": cus.holder.shippingAddress.zipCode,
							"state": cus.holder.shippingAddress.state
						}
					}
				}
	      });
	    }
	}
})
