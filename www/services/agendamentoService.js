angular.module('app.agendamentoService', [])

//*****************************_AGENDAMENTO_**********************************//
.factory('agendamentoService', function($rootScope, $http) {

	var dis;
	var loc;
	var prof;
	var key;
	var als;
	var pcat;
	var pd;
	var cd;
	var plocal;
	var adresstxt;
	var pconteudo;
	var palunos;
	var pprof;
	var pdata;
	var phora;
	var pvalor;
	var presumodia;
	var presumomes;
	var presumoano;
	var presumohora;
	var presumovalor2;
	var pkt = 0;
	//
	var alunokey;
	var alunonome;
	var alunopushid;
	var arrhors = [];
	var arrhorsfav = [];
	var isaluno = false;
	var marcadakey;
	var cancelarIndex;

	return {
		setDis: function(dd){
			dis = dd;
		},
		getDis: function(){
      return dis;
		},
		setLoc: function(lloc){
			loc = lloc;
		},
		getLoc: function(){
      return loc;
		},
		setProf: function(pp){
			prof = pp;
		},
		getProf: function(){
      return prof;
		},
		setKey: function(kk){
			key = kk;
		},
		getKey: function(){
      return key;
		},
		setAlunos: function(qtd){
			als = qtd;
		},
		getAlunos: function(){
      return als;
		},
		setPcat: function(ctg){
			pcat = ctg;
		},
		getPcat: function(){
      return pcat;
		},
		setPcd: function(ccd){
			cd = ccd;
		},
		reSetPcd: function(){
			var cdnew;
			cd = cdnew;
		},
		getPcd: function(){
      return cd;
		},
		setPd: function(ddis){
			pd = ddis;
		},
		getPd: function(){
      return pd;
		},
		setPlocal: function(locc){
			plocal = locc;
		},
		getPlocal: function(){
      return plocal;
		},
		setPadress: function(adtxt){
			adresstxt = adtxt;
		},
		getPadress: function(){
      return adresstxt;
		},
		setPconteudo: function(cnt){
			pconteudo = cnt;
		},
		getPconteudo: function(){
      return pconteudo;
		},
		setPalunos: function(alqtd){
			palunos = alqtd;
		},
		getPalunos: function(){
      return palunos;
		},
		setPprof: function(ppp){
			pprof = ppp;
		},
		getPprof: function(){
      return pprof;
		},
		setPdata: function(ddata){
			pdata = ddata;
		},
		getPdata: function(){
      return pdata;
		},
		setPhora: function(hhora){
			phora = hhora;
		},
		getPhora: function(){
      return phora;
		},
		setPvalor: function(vvalor){
			pvalor = vvalor;
		},
		getPvalor: function(){
      return pvalor;
		},
		setPresumodia: function(rdia){
			presumodia = rdia;
		},
		getPresumodia: function(){
      return presumodia;
		},
		setPresumomes: function(rmes){
			presumomes = rmes;
		},
		getPresumomes: function(){
      return presumomes;
		},
		setPresumoano: function(rano){
			presumoano = rano;
		},
		getPresumoano: function(){
      return presumoano;
		},
		setPresumohora: function(rhora){
			presumohora = rhora;
		},
		getPresumohora: function(){
      return presumohora;
		},
		setPresumovalor2: function(vrvalor){
			presumovalor2 = vrvalor;
		},
		getPresumovalor2: function(){
      return presumovalor2;
		},
		setPkt: function(ppkk){
			pkt = ppkk;
		},
		getPkt: function(){
      return pkt;
		},
		//
		setAlunoKey: function(alkk){
			alunokey = alkk;
		},
		getAlunoKey: function(){
      return alunokey;
		},
		setAlunoNome: function(alnm){
			alunonome = alnm;
		},
		getAlunoNome: function(){
      return alunonome;
		},
		setAlunoPushID: function(alpid){
			alunopushid = alpid;
		},
		getAlunoPushID: function(){
      return alunopushid;
		},
		setArrHors: function(hor){
			arrhors = hor;
		},
		getArrHors: function(){
      return arrhors;
		},
		setArrHorsFav: function(hor){
			arrhorsfav = hor;
		},
		getArrHorsFav: function(){
      return arrhorsfav;
		},
		resetArrHors: function(){
      		//arrhors = [];
      		arrhors = {};
		},
		resetArrHorsFav: function(){
      		arrhorsfav = {};
		},
		getMarcadaKey: function(){
			return marcadakey;
		},
		setMarcadaKey: function(mkkey){
			marcadakey = mkkey;
		},
		setCancelarIndex: function(ckk){
	      cancelarIndex = ckk;
	    },
	    getCancelarIndex: function(){
	      return cancelarIndex;
	    },
		marcarAulaTrans: function(auladata){
	      return $http({
	          method: 'POST',
	          url: 'https://us-central1-modular-c3962.cloudfunctions.net/marcarAulaTrans',
	          data: auladata
	        });
		},
		marcarAulaBoletoManual: function(auladata){
	      return $http({
	          method: 'POST',
	          url: 'https://us-central1-modular-c3962.cloudfunctions.net/marcarAulaBoletoManual',
	          data: auladata
	        });
		},
		scheduleClassWithBankBillet: function(auladata, idToken){
	      return $http({
	          method: 'POST',
			  url: 'https://us-central1-modular-c3962.cloudfunctions.net/scheduleClassWithBankBillet',
			  //url: 'http://localhost:5000/modular-c3962/us-central1/scheduleClassWithBankBillet',
			  headers: {
				'Content-Type': 'application/json',
				'Authorization':  'Bearer ' + idToken
			  },
	          data: auladata
	        });
		},
		scheduleClassWithCreditCard: function(auladata, idToken){
	      return $http({
	          method: 'POST',
			  url: 'https://us-central1-modular-c3962.cloudfunctions.net/scheduleClassWithCreditCard',
			  //url: 'http://localhost:5000/modular-c3962/us-central1/scheduleClassWithCreditCard',
			  headers: {
				'Content-Type': 'application/json',
				'Authorization':  'Bearer ' + idToken
			  },
	          data: auladata
	        });
		}
	}
})
