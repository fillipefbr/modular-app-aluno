angular.module('app.teacherService', [])

//*****************************_TEACHER_**************************************//
.factory('teacherService', function($rootScope) {

	var prof;
	var proffav;
	var key;
	var keyfav;
	var qtd = 0;
	var avatar;
	var avatarfav;
	var profpush;
	var profpushfav;
	var merchId;

	return {
		setProf: function(professor){
			prof = professor;
		},
		getProf: function(){
      return prof;
		},
		setProfFav: function(professorfav){
			proffav = professorfav;
		},
		getProfFav: function(){
      return proffav;
		},
		setProfKey: function(kk){
			key = kk;
		},
		getProfKey: function(){
      return key;
		},
		setProfKeyFav: function(kkf){
			keyfav = kkf;
		},
		getProfKeyFav: function(){
      return keyfav;
		},
		getProfessores: function(){
			/*dbRefProfessores.once('value').then(function(snapshot) {
        var x = snapshot.val();
        for(var i in x){
          console.log(x[i].Nome);
        }
      });*/
		},
		getAvatar: function(){
			return avatar;
		},
		setAvatar: function(avat){
			avatar = avat;
		},
		getAvatarFav: function(){
			return avatarfav;
		},
		setAvatarFav: function(avat){
			avatarfav = avat;
		},
		setProfPush: function(ppush){
			profpush = ppush;
		},
		getProfPush: function(){
      		return profpush;
		},
		setProfPushFav: function(ppush){
			profpushfav = ppush;
		},
		getProfPushFav: function(){
      		return profpushfav;
		},
		setMerchId: function(mid){
			merchId = mid;
		},
		getMerchId: function(){
      		return merchId;
		}
	}
})
