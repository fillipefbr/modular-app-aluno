angular.module('app.dataService', [])

//*****************************_DATA_**************************************//
.factory('dataService', function($rootScope, categoryService, $firebaseArray) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefProfessores = dbRefRoot.child('professores');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');

  var disciplinas = $firebaseArray(dbRefDisciplinas);
  //var alunos = $firebaseArray(dbRefAlunosPrivate);
  var professores = $firebaseArray(dbRefProfessores);
  //var professores_private = $firebaseArray(dbRefProfessoresPrivate);
  //var horarios_professores = $firebaseArray(dbRefHorariosProfessores);

  var aulasalunos = [];
  var aulasalunoshelper = [];
  var horariosprofessores = []; // added

  var prof = {};
  var aluno = {};
  var aulaaluno = {};
  var horarioprof = {};

  var i_aluno = 0;
  var i_aula = 0;

  var authToken = " ";

  return {

    /*initAlunos: function(){
      alunos = [];
      i_aluno = 0;
      dbRefAlunosPrivate.once('value', function(snap) {
        snap.forEach(function(child) {
          aluno = child.val();
          aluno.key = child.key;
          aluno.index = i_aluno;
          alunos.push(aluno);
          i_aluno++;
        });
      });
    },

    getAlunos: function(){
      return alunos;
    },*/


    /*loadAulasAlunos: function(){

      aulasalunoshelper = []; //added
      i_aula = 0;
      dbRefAulasAlunos.once('value', function(snap) {
        snap.forEach(function(child) {
          child.forEach(function(neto) {
            aulaaluno = neto.val();
            aulaaluno.Key = neto.key;
            aulaaluno.Aluno = child.key;
            aulaaluno.Index = i_aula;
            aulasalunoshelper.push(aulaaluno);
            i_aula++;
          });
        });
        if(aulasalunos.length != aulasalunoshelper.length){
          aulasalunos = aulasalunoshelper;
          return;
        }
        for(var propertyName in aulasalunos) {
            if(aulasalunos[propertyName] !== aulasalunoshelper[propertyName]) {
               aulasalunos = aulasalunoshelper;
               return;
            }
        }
        return;

      });

    },

    getAulasAlunos: function(pp){
      return aulasalunos;
    },*/

    setProfessores: function(){

      for(var i = 0; i < professores.length; i++){
        if(professores[i].Autorizado === true){
            professores.push(prof);
          }
      }

    },

    getProfessores: function(){
      return professores;
    },

    loadDisciplinas: function(){
      dbRefDisciplinas.once('value', function(snap) {
        snap.forEach(function(child){
          disciplinas.push(child.val());
        });
      });
    },

    getDisciplinas: function(){
      return disciplinas;
    },

    loadHorariosProfessores: function(){
      /*dbRefHorariosProfessores.once('value', function(snap) {
        snap.forEach(function(child) {
          horarioprof = child.val();
          horarioprof.key = child.key;
          horariosprofessores.push(horarioprof);
        });
      });*/
      horariosprofessores = $firebaseArray(dbRefHorariosProfessores);
    },

    getHorariosProfessores: function(){
      //return horarios_professores;
      return horariosprofessores;
    },

    setAuthToken: function(tk){
      authToken = tk;
    },

    getAuthToken: function(){
      return authToken;
    }

  }
})
