angular.module('app.loginCtrl', [])

//****************************_LOGIN CONTROLLER*******************************//

.controller('loginCtrl', function($scope, $rootScope, $document, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, categoryService, ionicToast, ConnectivityMonitor, $ionicPopup, $cordovaDevice, favoritesService, agendamentoService, dataService, paymentService) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const dbRefTours = dbRefRoot.child('tours');
  const dbRefAlunosTour = dbRefRoot.child('alunos_tour');
  const auth = firebase.auth();

  $scope.data = {};

  var professores = dataService.getProfessores();

  $ionicModal.fromTemplateUrl('login/resetPass.html', {
    scope: $scope
  }).then(function(modrpl) {
    $scope.modrpl = modrpl;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.modrpl != 'undefined'){
      $scope.modrpl.remove();
    }
  });


  $scope.$on("$ionicView.beforeEnter", function(){
    if(typeof(Storage) != "undefined") {
      $scope.data.login = window.localStorage.getItem("username");
      $scope.data.password = window.localStorage.getItem("password");
      $scope.data.rememberlogin = (window.localStorage.getItem("remember") == 'true');
    }
  });

//----------------------------------------------------------------------------//
    $scope.login = function(remember, login, word) {

        //if(ConnectivityMonitor.isOnline()){
          $ionicLoading.show({
            template: "<ion-spinner icon='ios-small'></ion-spinner><br/><p class='custom'>Carregando</p>"
          });

          var logged = false;
          var user = $document[0].getElementById('inputUserName');
          var pass = $document[0].getElementById('inputUserPassword');
          var isProf = false;

          var now = new Date();
          var dia = now.getDate();
          var mes = now.getMonth() + 1;
          var ano = now.getFullYear();
          var hora = now.getHours();
          var minutos = now.getMinutes();
          if(dia < 10) {
              dia = '0' + dia;
          }
          if(mes < 10) {
              mes = '0' + mes;
          }
          var lastLogin = dia + '/' + mes + '/' + ano + '-' + hora + ':' + minutos;

          //var model = $cordovaDevice.getModel();
          //var platform = $cordovaDevice.getPlatform();
          //var version = $cordovaDevice.getVersion();

          var promise =  auth.signInWithEmailAndPassword(user.value, pass.value);
          promise.catch(function(e) {
            if(e.code === "auth/wrong-password"){
              ionicToast.show('Senha incorreta', 'top', false, 2500);
            }
            else if(e.code === "auth/too-many-requests"){
              ionicToast.show('Limite de tentativas atingido, tente novamente mais tarde', 'top', false, 2500);
            }
            else if(e.code === "auth/user-not-found"){
              ionicToast.show('E-mail não encontrado', 'top', false, 2500);
            }
            else{
              ionicToast.show('Usuário não encontrado', 'top', false, 2500);
            }
            $ionicLoading.hide();
             return;
          });

          promise.then(function(currentUser) {

            dataService.setProfessores();
            dataService.loadHorariosProfessores();

            firebase.auth().currentUser.getToken(/* forceRefresh */ true).then(function(idToken) {
              dataService.setAuthToken(idToken);
            }).catch(function(error) {
              
            });

            for(var i = 0; i < professores.length; i++){
              if(professores[i].$id === currentUser.uid){
                isProf = true;
                break;
              }
            }

            if(!isProf){
              $scope.saveRemember(remember, login, word);
              if(typeof $rootScope.pushID != 'undefined'){
                dbRefAlunosPrivate.child(currentUser.uid).update({"Push_ID" : $rootScope.pushID});
              }

              dbRefAlunosPrivate.child(currentUser.uid).once('value', function(snap) {
                if(snap.val().Favoritos){
                  favoritesService.setHasFavTrue();
                }
              });

              /*dbRefAlunosPrivate.child(currentUser.uid).update({"Login" : lastLogin});
              dbRefAlunosPrivate.child(currentUser.uid).update({"Dispositivo" : model});
              dbRefAlunosPrivate.child(currentUser.uid).update({"OS" : platform});
              dbRefAlunosPrivate.child(currentUser.uid).update({"OS_Version" : version});*/
              var dbRefAlu = dbRefAlunosPrivate.child(currentUser.uid);
              dbRefAlu.once('value', function(snap) {
                if(pass.value != snap.val().Senha){
                  dbRefAlunosPrivate.child(currentUser.uid).update({"Senha" : pass.value});
                }
                if(snap.val().customerId) {
                  paymentService.setCusId(snap.val().customerId);
                }
                categoryService.setCat(snap.val().Categoria);
                $rootScope.alunoaulasid = snap.val().Nome;
              });
              if (currentUser.emailVerified) {
                dbRefAlunosPrivate.child(currentUser.uid).update({"Email_Verificado" : true});
              }
              agendamentoService.setAlunoKey(currentUser.uid);
              agendamentoService.setAlunoNome(currentUser.displayName);
              $ionicLoading.hide();
              logged = true;
              $state.go('tabsController.aulas');
            }
            else{
              $ionicLoading.hide();

              var alertPopup = $ionicPopup.alert({
               title: 'Aviso',
               template: "<div class='row'><div class='col text-center'><i class='icon ion-shuffle dark' style='font-size: 40px;'></i><br><p class='custom'>Você está tentando acessar com uma conta de professor. Por favor, cadastre-se como aluno para utilizar esse aplicativo.</p></div></div>",
               cssClass: 'popcustom',
               okType: 'customok'
             });
              return;
            }

          });

        //}

        /*else{
          ionicToast.show('Sem conexão com a internet. Verifique sua conexão antes de fazer login', 'middle', false, 2500);
        }*/

    };
//----------------------------------------------------------------------------//
    $scope.checkRemember = function() {
      var user;
      var pass;
      $timeout(function() {
        user = $document[0].getElementById('inputUserName');
        pass = $document[0].getElementById('inputUserPassword');
        check = $document[0].getElementById('inputUserCheck');
        if(typeof(Storage) != "undefined") {
            user.value = window.localStorage.getItem("username");
            $scope.data.login = user.value;
            pass.value = window.localStorage.getItem("password");
            $scope.data.password = pass.value;
            check.checked = window.localStorage.getItem("remember");
            $scope.data.rememberlogin = check.checked;
        }
      }, 500);
    };
//----------------------------------------------------------------------------//
    $scope.saveRemember = function(salva, login, pass) {
      if(salva){
        if(typeof(Storage) != "undefined") {
            window.localStorage.setItem("username", login);
            window.localStorage.setItem("password", pass);
            window.localStorage.setItem("remember", true);
        }
      }
      else if(!salva){
        if(typeof(Storage) != "undefined") {
          window.localStorage.removeItem("username");
          window.localStorage.removeItem("password");
          window.localStorage.removeItem("remember");
        }
      }
    };

//----------------------------------------------------------------------------//
  $scope.resetPass = function(email) {
      firebase.auth().sendPasswordResetEmail(email).then(function() {
        $scope.modrpl.hide();

        var alertPopup = $ionicPopup.alert({
           title: 'Concluído',
           template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Um link de redefinição de senha foi enviado para o seu e-mail.</p></div></div>",
           cssClass: 'popcustom',
           okType: 'customok'
        });

      }, function(error) {

        var alertPopup = $ionicPopup.alert({
           title: 'Erro',
           template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-close-round icon-pop'></i></div></div></div><p class='custom'>Ocorreu um erro. Por favor, verifique seu e-mail e tente novamente.</p></div></div>",
           cssClass: 'popcustom',
           okType: 'customok'
        });

      });
  };


})
