angular.module('app.marcadasCtrl', [])

//***************************_MARCADAS CONTROLLER*******************************//

.controller('marcadasCtrl', function($scope, $rootScope, $document, $ionicPopup, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicActionSheet, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, ionicToast, ConnectivityMonitor, notificationsService, agendamentoService, pacotesService, dataService, $firebaseArray) {

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const dbRefCadastroPermanente = dbRefRoot.child('cadastro_permanente');
  const dbRefCodesPromocionais = dbRefRoot.child('codes_promocionais');
  const dbRefContatoModular = dbRefRoot.child('contato_modular');
  const dbRefAvaliacoes = dbRefRoot.child('avaliacoes_aulas');
  const auth = firebase.auth();

  $ionicModal.fromTemplateUrl('marcadas/profRating.html', {
    scope: $scope
  }).then(function(modrtl) {
    $scope.modrtl = modrtl;
  });

  $scope.$on('$destroy', function() {
    if(typeof $scope.modrtl != 'undefined'){
      $scope.modrtl.remove();
    }
  });

  $scope.ratingsObject = {
      iconOn : 'ion-android-star',
      iconOff : 'ion-android-star-outline',
      iconOnColor: 'rgb(200, 200, 100)',
      iconOffColor:  'rgb(68, 75, 182)',
      rating:  1,
      minRating:1,
      callback: function(rating) {
        $scope.ratingsCallback(rating);
      }
  };

  function parsePagamento(fp) {
    if(fp === "Transfer"){
      return "Transferência Bancária";
    }
    else if(fp === "Boleto" || fp === "Boleto Manual"){
      return "Boleto Bancário";
    }
    else if(fp === "Cartão"){
      return "Cartão de Crédito";
    }
    else if(fp === "Pocket"){
      return "Pagamento efetuado";
    }
    else{
      return fp;
    }
  };

  var address;

  $scope.showAlunoInfo = function() {
    var user = agendamentoService.getAlunoKey();
    var aluno_nome = agendamentoService.getAlunoNome();
    var alertPopup = $ionicPopup.alert({
       title: 'Aluno',
       template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-android-person icon-pop'></i></div></div></div><p class='custom'><strong>Nome:</strong><br>" + aluno_nome + "<br><strong>UID:</strong><br>" + user + "</p></div></div>",
       cssClass: 'popcustom',
       okType: 'customok'
     });
  }

  $scope.showAddressInfo = function() {
    var alertPopup = $ionicPopup.alert({
       title: 'Endereço',
       template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-ios-location-outline icon-pop'></i></div></div></div><p class='custom'><strong>Local:</strong><br>" + address + "</p></div></div>",
       cssClass: 'popcustom',
       okType: 'customok'
     });
  }


  $scope.$on("$ionicView.beforeEnter", function(){
    $ionicLoading.show({
      template: "<ion-spinner icon='ios-small'></ion-spinner><br/><p class='custom'>Aguarde</p>"
    });
    $scope.avatarprof = "img/person-outline.png";
    var aulakey = agendamentoService.getMarcadaKey();
    var user = agendamentoService.getAlunoKey();
    var aluno_nome = agendamentoService.getAlunoNome();
    dbRefAulasAlunos.child(user).child(aulakey).once('value', function(snap) {
      $timeout(function() {
        $scope.categoriaaluno = snap.val().Categoria;
        $scope.nomealuno = aluno_nome.substring(0, 20) + '...';
        $scope.disciplinaaluno = snap.val().Disciplina;
        address = snap.val().Endereço;
        $scope.enderecoaluno = (snap.val().Endereço).substring(0, 20) + '...';
        $scope.alunosaluno = snap.val().Alunos;
        $scope.dataaluno = snap.val().Data;
        //$scope.pagamentoaluno = snap.val().Pagamento;
        $scope.pagamentoaluno = parsePagamento(snap.val().Pagamento);
        var valorparsed = parseInt(snap.val().Valor, 10);
        valorparsed = valorparsed / 100;
        valorparsed = valorparsed.toFixed(2);
        valorparsed = valorparsed.toString();
        valorparsed = valorparsed.replace(".", ",");
        $scope.valoraluno = "R$ " + valorparsed;
        //var prof = snap.val().Prof_ID;
      }, 0);
        dbRefProfessores.child(snap.val().Prof_ID).once('value', function(snapshot) {
          $timeout(function() {
            $scope.avatarprof = snapshot.val().Avatar;
            $scope.nomeprof = snapshot.val().Nome;
            $scope.cursoprof = snapshot.val().Curso;
            $ionicLoading.hide();
          }, 0);
        });
    });

    $timeout(function() {
      $ionicLoading.hide();
    }, 4000);
  });

//----------------------------------------------------------------------------//
  $scope.avaprof = function(av) {
    if(av === 'sim'){
      $scope.avasim = true;
      $scope.avanao = false;
    }
    else{
      $scope.avasim = false;
      $scope.avanao = true;
    }
  };

//----------------------------------------------------------------------------//
  $scope.ratingsCallback = function(rating) {
    $rootScope.lastClassProfRating = rating;
  };

//----------------------------------------------------------------------------//
  $scope.rateProf = function() {

    if(typeof $rootScope.lastClassProfRating != 'undefined'){
      dbRefProfessores.on('child_added', function(snap) {

        if($rootScope.lastClassProf === snap.val().Nome){
          var ava = snap.val().Avaliações;
          ava = parseInt(ava, 10);
          ava += 1;
          ava = ava.toString();
          dbRefProfessores.child(snap.key).update({"Avaliações": ava});
          var nota = snap.val().Avaliações_Valor;
          $rootScope.lastClassProfRating = parseInt($rootScope.lastClassProfRating, 10);
          nota = parseInt(nota, 10);
          nota += $rootScope.lastClassProfRating;
          nota = nota.toString();
          dbRefProfessores.child(snap.key).update({"Avaliações_Valor": nota});
          $scope.modrtl.hide();
        }

      });
    }

    else{
      dbRefProfessores.on('child_added', function(snap) {

        if($rootScope.lastClassProf === snap.val().Nome){
          var ava = snap.val().Avaliações;
          ava = parseInt(ava, 10);
          ava += 1;
          ava = ava.toString();
          dbRefProfessores.child(snap.key).update({"Avaliações": ava});
          var nota = snap.val().Avaliações_Valor;
          nota = parseInt(nota, 10);
          nota += 1;
          nota = nota.toString();
          dbRefProfessores.child(snap.key).update({"Avaliações_Valor": nota});
          $scope.modrtl.hide();
        }

      });
    }

    if($scope.avasim === true){
      dbRefAvaliacoes.once('value', function(snop) {
        dbRefAvaliacoes.update({"Sim" : snop.val().Sim + 1});
      });
    }
    else if($scope.avanao === true){
      dbRefAvaliacoes.once('value', function(snop) {
        dbRefAvaliacoes.update({"Não" : snop.val().Não + 1});
      });
    }

  };

//----------------------------------------------------------------------------//
function parseCategoria(cat) {

    var novacat;

    if(cat === "Ensino Médio"){
      novacat = "Médio";
    }

    if(cat === "Ensino Fundamental"){
      novacat = "Fundamental";
    }

    if(cat === "Ensino Superior"){
      novacat = "Superior";
    }

    if(cat === "Pré-Vestibular/Enem"){
      novacat = "Vestibular";
    }

    if(cat === "Idiomas"){
      novacat = "Idiomas";
    }

    return novacat;

  };

//----------------------------------------------------------------------------//
  $scope.realPayment = function() {

    var user = agendamentoService.getAlunoKey();
    var aulakey = agendamentoService.getMarcadaKey();

    if (user && aulakey) {
      
      var dbRefAlu;
      var dbRefAM;
      var id;
      var card;
      var tk;

      dbRefAlu = dbRefAulasAlunos.child(user).child(aulakey);

      dbRefAlu.once('value', function(snap) {

        if(snap.val().Pagamento === "Boleto" && snap.val().Professor_Finalizou === "Sim" && snap.val().Pagamento_Efetuado === "Não"){
          id = snap.val().Charge_ID;
          //id = parseInt(id, 10);
          efetuaBoleto(id, snap.key, user);
        }

        if(snap.val().Pagamento === "Cartão" && snap.val().Professor_Finalizou === "Sim" && snap.val().Pagamento_Efetuado === "Não"){
          id = snap.val().Charge_ID;
          card = snap.val().card;
          //tk = snap.val().Token;
          efetuaCartao(id, card, snap.key, user);
        }

        if(snap.val().Pagamento === "Pocket" && snap.val().Professor_Finalizou === "Sim" && snap.val().Pagamento_Efetuado === "Não"){
          efetuaPocket(snap.key, user);
        }

        if(snap.val().Pagamento === "Transfer" && snap.val().Professor_Finalizou === "Sim" && snap.val().Pagamento_Efetuado === "Não"){
          return efetuaTransfer(snap.key, user);
        }

        if(snap.val().Pagamento === "Boleto Manual" && snap.val().Professor_Finalizou === "Sim" && snap.val().Pagamento_Efetuado === "Não"){
          return efetuaBoletoManual(snap.key, user);
        }

        if(snap.val().Pagamento_Efetuado === "Sim"){

          var alertPopup = $ionicPopup.alert({
             title: 'Aviso',
             template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Essa aula já foi paga!</p></div></div>",
             cssClass: 'popcustom',
             okType: 'customok'
           });
           return;
        }

        if(snap.val().Professor_Finalizou === "Não"){

          var alertPopup = $ionicPopup.alert({
             title: 'Aviso',
             template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>O professor ainda não finalizou a aula. É necessário que ele finalize a aula antes de você realizar o pagamento.</p></div></div>",
             cssClass: 'popcustom',
             okType: 'customok'
          });

          return;

        }

      });

    }

  };

//----------------------------------------------------------------------------//
  function efetuaBoleto(id, key, user) {

    $ionicLoading.show({
      template: "<ion-spinner></ion-spinner><br/><p class='custom'>Carregando</p>"
    });

    var authToken = dataService.getAuthToken();

    paymentService.createPaymentWithBankBillet(id, authToken).then(function(response) {

      //console.log(response.data._links.payBoleto.printHref);

      var AulasConcluidas;
      var mes;
      var mesint;
      var dia;
      var diaint;
      var aulas;
      var novacat;
      var existe = false;
      var diskey;
      var prof;
      var aulaID;
      var profID;
      var aulasProf;
      var dbRefA = dbRefAlunosPrivate.child(user);
      var dbRefAlu = dbRefAulasAlunos.child(user);
      var dbRefF = dbRefA.child("Financeiro");
      var dbRefD = dbRefA.child("Disciplinas");
      dbRefAlu.child(key).update({"Aluno_Finalizou": "Sim"});
      dbRefAlu.child(key).update({"Pagamento_Efetuado": "Sim"});

      dbRefA.once('value', function(snap) {

        AulasConcluidas = snap.val().AulasConcluidas;
        AulasConcluidas = parseInt(AulasConcluidas, 10);
        AulasConcluidas += 1;
        AulasConcluidas = AulasConcluidas.toString();
        dbRefA.update({"AulasConcluidas": AulasConcluidas});

      });

      dbRefAlu.child(key).once('value', function(snapshot) {

        mes = snapshot.val().Mês;
        mesint = parseInt(mes, 10);
        if(mesint < 10){
          mes = "0" + mes;
        }
        dia = snapshot.val().Dia;
        diaint = parseInt(dia, 10);
        if(diaint < 10){
          dia = "0" + dia;
        }
        dbRefF.push({
          "Ano": snapshot.val().Ano, 
          "Data": snapshot.val().Dia + '/' + mes + '/' + snapshot.val().Ano, 
          "Dia": snapshot.val().Dia, 
          "Disciplina": snapshot.val().Disciplina, 
          "Hora": snapshot.val().Hora, 
          "Local": snapshot.val().Endereço, 
          "Mês": snapshot.val().Mês, 
          "Professor": snapshot.val().Professor, 
          "Valor": snapshot.val().Valor, 
          "Pagamento" : "Boleto", 
          "Charge_ID" : snapshot.val().Charge_ID, 
          "Pay_ID" : response.data.id,
          "Cidade" : snapshot.val().Local
        });
        novacat = snapshot.val().Categoria;
        novacat = parseCategoria(novacat);
        prof = snapshot.val().Professor;
        aulaID = snapshot.val().Aula_ID;
        profID = snapshot.val().Prof_ID;


        dbRefProfessores.child(profID).once('value', function(snep) {

          aulasProf = snep.val().Aulas;
          aulasProf += 1;
          dbRefProfessores.child(profID).update({"Aulas": aulasProf});

        });

        dbRefAulasProfessores.child(profID).child(aulaID).update({"Aluno_Finalizou": "Sim"});
        dbRefAulasProfessores.child(profID).child(aulaID).update({"Pagamento_Efetuado": "Sim"});

        dbRefD.once('value', function(snop) {

          if(snop.val()){
            snop.forEach(function(child) {
              if(novacat === child.val().Categoria && snapshot.val().Disciplina === child.val().Nome){
                existe = true;
                diskey = child.key;
                aulas = child.val().Aulas;
                aulas = parseInt(aulas, 10);
              }
            });

            if(!existe){
              var dis = snapshot.val().Disciplina;
              var icon;
              dbRefDisciplinas.on('child_added', function(snep) {
                if(dis === snep.val().Nome && novacat === snep.val().Categoria){
                  icon = snep.val().Icon;
                  dbRefD.push({"Nome": dis, "Aulas" : "1", "Categoria" : novacat, "Icon" : icon, "Cor" : "#444bb6"});
                }
              });
              dbRefAlu.child(key).remove();
              $ionicLoading.hide();

              var alertPopup = $ionicPopup.alert({
                title: 'Aula Finalizada!',
                template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Por favor, conclua a avaliação da sua aula para visualizar o boleto gerado.</p></div></div>",
                cssClass: 'popcustom',
                okType: 'customok'
              });

              $rootScope.lastClassProf = prof;
              $scope.modrtl.show();

              $scope.$on('modal.hidden', function() {
                window.open(response.data._links.payBoleto.printHref, '_system', 'location=yes');
                $ionicHistory.nextViewOptions({
                  disableBack: true
                });                
                $state.go('tabsController.aulas');
              });
            }

            else{
              aulas += 1;
              dbRefD.child(diskey).update({"Aulas": aulas.toString()});
              dbRefAlu.child(key).remove();
              $ionicLoading.hide();

              var alertPopup = $ionicPopup.alert({
                title: 'Aula Finalizada!',
                template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Por favor, conclua a avaliação da sua aula para visualizar o boleto gerado.</p></div></div>",
                cssClass: 'popcustom',
                okType: 'customok'
              });

              $rootScope.lastClassProf = prof;
              $scope.modrtl.show();
              $scope.$on('modal.hidden', function() {
                window.open(response.data._links.payBoleto.printHref, '_system', 'location=yes');  
                $ionicHistory.nextViewOptions({
                  disableBack: true
                }); 
                $state.go('tabsController.aulas');
              });
            }
          }
          else{
            var dis = snapshot.val().Disciplina;
            var icon;
            dbRefDisciplinas.on('child_added', function(snep) {
              if(dis === snep.val().Nome && novacat === snep.val().Categoria){
                icon = snep.val().Icon;
                dbRefD.push({"Nome": dis, "Aulas" : "1", "Categoria" : novacat, "Icon" : icon, "Cor" : "#444bb6"});
              }
            });
            dbRefAlu.child(key).remove();
            $ionicLoading.hide();

            var alertPopup = $ionicPopup.alert({
              title: 'Aula Finalizada!',
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Por favor, conclua a avaliação da sua aula para visualizar o boleto gerado.</p></div></div>",
              cssClass: 'popcustom',
              okType: 'customok'
            });

            $rootScope.lastClassProf = prof;
            $scope.modrtl.show();

            $scope.$on('modal.hidden', function() {
              window.open(response.data._links.payBoleto.printHref, '_system', 'location=yes');  
              $ionicHistory.nextViewOptions({
                disableBack: true
              }); 
              $state.go('tabsController.aulas');
            });
          }

        });

      });

    },
    function(error) {
          
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
          title: 'Erro',
          template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Ocorreu um erro ao finalizar sua aula. Por favor, entre em contato conosco caso o problema persista.</p></div></div>",
          cssClass: 'popcustom',
          okType: 'customok'
        });

    });

  };

//----------------------------------------------------------------------------//
  function efetuaPocket(key, user) {

    $ionicLoading.show({
      template: "<ion-spinner></ion-spinner><br/><p class='custom'>Carregando</p>"
    });

    var AulasConcluidas;
    var mes;
    var mesint;
    var dia;
    var diaint;
    var aulas;
    var prof;
    var novacat;
    var existe = false;
    var diskey;
    var aulaID;
    var profID;
    var aulasProf;
    var dbRefA = dbRefAlunosPrivate.child(user);
    var dbRefAlu = dbRefAulasAlunos.child(user);
    var dbRefF = dbRefA.child("Financeiro");
    var dbRefD = dbRefA.child("Disciplinas");
    dbRefAlu.child(key).update({"Aluno_Finalizou": "Sim"});
    dbRefAlu.child(key).update({"Pagamento_Efetuado": "Sim"});

    dbRefA.once('value', function(snap) {

      AulasConcluidas = snap.val().AulasConcluidas;
      AulasConcluidas = parseInt(AulasConcluidas, 10);
      AulasConcluidas += 1;
      AulasConcluidas = AulasConcluidas.toString();
      dbRefA.update({"AulasConcluidas": AulasConcluidas});

    });

    dbRefAlu.child(key).once('value', function(snapshot) {

      mes = snapshot.val().Mês;
      mesint = parseInt(mes, 10);
      if(mesint < 10){
        mes = "0" + mes;
      }
      dia = snapshot.val().Dia;
      diaint = parseInt(dia, 10);
      if(diaint < 10){
        dia = "0" + dia;
      }
      dbRefF.push({"Ano": snapshot.val().Ano, "Data": dia + '/' + mes + '/' + snapshot.val().Ano, "Dia": snapshot.val().Dia, "Disciplina": snapshot.val().Disciplina, "Hora": snapshot.val().Hora, "Local": snapshot.val().Endereço, "Mês": snapshot.val().Mês, "Professor": snapshot.val().Professor, "Valor": snapshot.val().Valor, "Pagamento" : "Pocket", "Prof_ID" : snapshot.val().Prof_ID, "Cidade" : snapshot.val().Local});
      novacat = snapshot.val().Categoria;
      novacat = parseCategoria(novacat);
      prof = snapshot.val().Professor;
      aulaID = snapshot.val().Aula_ID;
      profID = snapshot.val().Prof_ID;

      dbRefProfessores.child(profID).once('value', function(snep) {

        aulasProf = snep.val().Aulas;
        aulasProf += 1;
        dbRefProfessores.child(profID).update({"Aulas": aulasProf});

      });

      dbRefAulasProfessores.child(profID).child(aulaID).update({"Aluno_Finalizou": "Sim"});
      dbRefAulasProfessores.child(profID).child(aulaID).update({"Pagamento_Efetuado": "Sim"});

      dbRefD.once('value', function(snop) {

        if(snop.val()){
          snop.forEach(function(child) {
            if(novacat === child.val().Categoria && snapshot.val().Disciplina === child.val().Nome){
              existe = true;
              diskey = child.key;
              aulas = child.val().Aulas;
              aulas = parseInt(aulas, 10);
            }
          });

          if(!existe){
            var dis = snapshot.val().Disciplina;
            var icon;
            dbRefDisciplinas.on('child_added', function(snep) {
              if(dis === snep.val().Nome && novacat === snep.val().Categoria){
                icon = snep.val().Icon;
                dbRefD.push({"Nome": dis, "Aulas" : "1", "Categoria" : novacat, "Icon" : icon, "Cor" : "#444bb6"});
              }
            });
            dbRefAlu.child(key).remove();
            $ionicLoading.hide();

            var alertPopup = $ionicPopup.alert({
              title: 'Aula Finalizada!',
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Confira os detalhes para obter o boleto gerado no seu e-mail.</p></div></div>",
              cssClass: 'popcustom',
              okType: 'customok'
            });

            $rootScope.lastClassProf = prof;
            $scope.modrtl.show();

            $scope.$on('modal.hidden', function() {
              $state.go('tabsController.aulas');
            });
          }

          else{
            aulas += 1;
            dbRefD.child(diskey).update({"Aulas": aulas.toString()});
            dbRefAlu.child(key).remove();
            $ionicLoading.hide();

            var alertPopup = $ionicPopup.alert({
              title: 'Aula Finalizada!',
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Confira os detalhes para obter o boleto gerado no seu e-mail.</p></div></div>",
              cssClass: 'popcustom',
              okType: 'customok'
            });

            $rootScope.lastClassProf = prof;
            $scope.modrtl.show();
            $scope.$on('modal.hidden', function() {
              $state.go('tabsController.aulas');
            });
          }
        }
        else{
          var dis = snapshot.val().Disciplina;
          var icon;
          dbRefDisciplinas.on('child_added', function(snep) {
            if(dis === snep.val().Nome && novacat === snep.val().Categoria){
              icon = snep.val().Icon;
              dbRefD.push({"Nome": dis, "Aulas" : "1", "Categoria" : novacat, "Icon" : icon, "Cor" : "#444bb6"});
            }
          });
          dbRefAlu.child(key).remove();
          $ionicLoading.hide();

          var alertPopup = $ionicPopup.alert({
            title: 'Aula Finalizada!',
            template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Confira os detalhes para obter o boleto gerado no seu e-mail.</p></div></div>",
            cssClass: 'popcustom',
            okType: 'customok'
          });

          $rootScope.lastClassProf = prof;
          $scope.modrtl.show();

          $scope.$on('modal.hidden', function() {
            $state.go('tabsController.aulas');
          });
        }

      });

    });


  };

//----------------------------------------------------------------------------//
  function efetuaTransfer(key, user) {

    $ionicLoading.show({
      template: "<ion-spinner></ion-spinner><br/><p class='custom'>Carregando</p>"
    });

    var AulasConcluidas;
    var mes;
    var mesint;
    var dia;
    var diaint;
    var aulas;
    var prof;
    var novacat;
    var existe = false;
    var diskey;
    var aulaID;
    var profID;
    var aulasProf;
    var dbRefA = dbRefAlunosPrivate.child(user);
    var dbRefAlu = dbRefAulasAlunos.child(user);
    var dbRefF = dbRefA.child("Financeiro");
    var dbRefD = dbRefA.child("Disciplinas");
    var dbRefDis = $firebaseArray(dbRefDisciplinas);
    var dbRefDisAl = $firebaseArray(dbRefD);
    dbRefAlu.child(key).update({"Aluno_Finalizou": "Sim"});
    dbRefAlu.child(key).update({"Pagamento_Efetuado": "Sim"});

    dbRefA.once('value', function(snap) {
      AulasConcluidas = snap.val().AulasConcluidas;
      AulasConcluidas = parseInt(AulasConcluidas, 10);
      AulasConcluidas += 1;
      AulasConcluidas = AulasConcluidas.toString();
      dbRefA.update({"AulasConcluidas": AulasConcluidas});
    });

    dbRefAlu.child(key).once('value', function(snapshot) {
      mes = snapshot.val().Mês;
      mesint = parseInt(mes, 10);
      if(mesint < 10){
        mes = "0" + mes;
      }
      dia = snapshot.val().Dia;
      diaint = parseInt(dia, 10);
      if(diaint < 10){
        dia = "0" + dia;
      }
      dbRefF.push({"Ano": snapshot.val().Ano, "Data": dia + '/' + mes + '/' + snapshot.val().Ano, "Dia": snapshot.val().Dia, "Disciplina": snapshot.val().Disciplina, "Hora": snapshot.val().Hora, "Local": snapshot.val().Endereço, "Mês": snapshot.val().Mês, "Professor": snapshot.val().Professor, "Valor": snapshot.val().Valor, "Pagamento" : "Transfer", "Prof_ID" : snapshot.val().Prof_ID, "Cidade" : snapshot.val().Local});
      novacat = snapshot.val().Categoria;
      novacat = parseCategoria(novacat);
      prof = snapshot.val().Professor;
      aulaID = snapshot.val().Aula_ID;
      profID = snapshot.val().Prof_ID;

      dbRefProfessores.child(profID).once('value', function(snep) {
        aulasProf = snep.val().Aulas;
        aulasProf += 1;
        dbRefProfessores.child(profID).update({"Aulas": aulasProf});
      });

      dbRefAulasProfessores.child(profID).child(aulaID).update({"Aluno_Finalizou": "Sim"});
      dbRefAulasProfessores.child(profID).child(aulaID).update({"Pagamento_Efetuado": "Sim"});

      dbRefD.once('value', function(snop) {

        if(snop.val()){
          snop.forEach(function(child) {
            if(novacat === child.val().Categoria && snapshot.val().Disciplina === child.val().Nome){
              existe = true;
              diskey = child.key;
              aulas = child.val().Aulas;
              aulas = parseInt(aulas, 10);
            }
          });

          if(!existe){
            var dis = snapshot.val().Disciplina;
            var icon;
            dbRefDisciplinas.on('child_added', function(snep) {
              if(dis === snep.val().Nome && novacat === snep.val().Categoria){
                icon = snep.val().Icon;
                dbRefD.push({"Nome": dis, "Aulas" : "1", "Categoria" : novacat, "Icon" : icon, "Cor" : "#444bb6"});
              }
            });
            dbRefAlu.child(key).remove();
            $ionicLoading.hide();

            var alertPopup = $ionicPopup.alert({
              title: 'Aula Finalizada!',
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Por favor, envie o comprovante de transferência para o WhatsApp da Modular, ou entre em contato conosco.</p></div></div>",
              cssClass: 'popcustom',
              okType: 'customok'
            });

            $rootScope.lastClassProf = prof;
            $scope.modrtl.show();

            $scope.$on('modal.hidden', function() {
              $state.go('tabsController.aulas');
            });
          }

          else{
            aulas += 1;
            dbRefD.child(diskey).update({"Aulas": aulas.toString()});
            dbRefAlu.child(key).remove();
            $ionicLoading.hide();

            var alertPopup = $ionicPopup.alert({
              title: 'Aula Finalizada!',
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Por favor, envie o comprovante de transferência para o WhatsApp da Modular, ou entre em contato conosco.</p></div></div>",
              cssClass: 'popcustom',
              okType: 'customok'
            });

            $rootScope.lastClassProf = prof;
            $scope.modrtl.show();
            $scope.$on('modal.hidden', function() {
              $state.go('tabsController.aulas');
            });
          }
        }
        else{
          var dis = snapshot.val().Disciplina;
          var icon;
          dbRefDisciplinas.on('child_added', function(snep) {
            if(dis === snep.val().Nome && novacat === snep.val().Categoria){
              icon = snep.val().Icon;
              dbRefD.push({"Nome": dis, "Aulas" : "1", "Categoria" : novacat, "Icon" : icon, "Cor" : "#444bb6"});
            }
          });
          dbRefAlu.child(key).remove();
          $ionicLoading.hide();

          var alertPopup = $ionicPopup.alert({
            title: 'Aula Finalizada!',
            template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Por favor, envie o comprovante de transferência para o WhatsApp da Modular, ou entre em contato conosco.</p></div></div>",
            cssClass: 'popcustom',
            okType: 'customok'
          });

          $rootScope.lastClassProf = prof;
          $scope.modrtl.show();

          $scope.$on('modal.hidden', function() {
            $state.go('tabsController.aulas');
          });
        }

      });

    });

 };


//----------------------------------------------------------------------------//
  function efetuaBoletoManual(key, user) {

    $ionicLoading.show({
      template: "<ion-spinner></ion-spinner><br/><p class='custom'>Carregando</p>"
    });

    var AulasConcluidas;
    var mes;
    var mesint;
    var dia;
    var diaint;
    var aulas;
    var prof;
    var novacat;
    var existe = false;
    var diskey;
    var aulaID;
    var profID;
    var aulasProf;
    var dbRefA = dbRefAlunosPrivate.child(user);
    var dbRefAlu = dbRefAulasAlunos.child(user);
    var dbRefF = dbRefA.child("Financeiro");
    var dbRefD = dbRefA.child("Disciplinas");
    dbRefAlu.child(key).update({"Aluno_Finalizou": "Sim"});
    dbRefAlu.child(key).update({"Pagamento_Efetuado": "Sim"});

    dbRefA.once('value', function(snap) {
      AulasConcluidas = snap.val().AulasConcluidas;
      AulasConcluidas = parseInt(AulasConcluidas, 10);
      AulasConcluidas += 1;
      AulasConcluidas = AulasConcluidas.toString();
      dbRefA.update({"AulasConcluidas": AulasConcluidas});
    });

    dbRefAlu.child(key).once('value', function(snapshot) {
      
      mes = snapshot.val().Mês;
      mesint = parseInt(mes, 10);
      if(mesint < 10){
        mes = "0" + mes;
      }
      dia = snapshot.val().Dia;
      diaint = parseInt(dia, 10);
      if(diaint < 10){
        dia = "0" + dia;
      }
      dbRefF.push({"Ano": snapshot.val().Ano, "Data": dia + '/' + mes + '/' + snapshot.val().Ano, "Dia": snapshot.val().Dia, "Disciplina": snapshot.val().Disciplina, "Hora": snapshot.val().Hora, "Local": snapshot.val().Endereço, "Mês": snapshot.val().Mês, "Professor": snapshot.val().Professor, "Valor": snapshot.val().Valor, "Pagamento" : "Boleto Manual", "Prof_ID" : snapshot.val().Prof_ID, "Cidade" : snapshot.val().Local});
      novacat = snapshot.val().Categoria;
      novacat = parseCategoria(novacat);
      prof = snapshot.val().Professor;
      aulaID = snapshot.val().Aula_ID;
      profID = snapshot.val().Prof_ID;

      dbRefProfessores.child(profID).once('value', function(snep) {
        aulasProf = snep.val().Aulas;
        aulasProf += 1;
        dbRefProfessores.child(profID).update({"Aulas": aulasProf});
      });

      dbRefAulasProfessores.child(profID).child(aulaID).update({"Aluno_Finalizou": "Sim"});
      dbRefAulasProfessores.child(profID).child(aulaID).update({"Pagamento_Efetuado": "Sim"});

      dbRefD.once('value', function(snop) {

        if(snop.val()){
          snop.forEach(function(child) {
            if(novacat === child.val().Categoria && snapshot.val().Disciplina === child.val().Nome){
              existe = true;
              diskey = child.key;
              aulas = child.val().Aulas;
              aulas = parseInt(aulas, 10);
            }
          });

          if(!existe){
            var dis = snapshot.val().Disciplina;
            var icon;
            dbRefDisciplinas.on('child_added', function(snep) {
              if(dis === snep.val().Nome && novacat === snep.val().Categoria){
                icon = snep.val().Icon;
                dbRefD.push({"Nome": dis, "Aulas" : "1", "Categoria" : novacat, "Icon" : icon, "Cor" : "#444bb6"});
              }
            });
            dbRefAlu.child(key).remove();
            $ionicLoading.hide();

            var alertPopup = $ionicPopup.alert({
              title: 'Aula Finalizada!',
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Confira os detalhes para obter o boleto gerado no seu e-mail.</p></div></div>",
              cssClass: 'popcustom',
              okType: 'customok'
            });

            $rootScope.lastClassProf = prof;
            $scope.modrtl.show();

            $scope.$on('modal.hidden', function() {
              $state.go('tabsController.aulas');
            });
          }

          else{
            aulas += 1;
            dbRefD.child(diskey).update({"Aulas": aulas.toString()});
            dbRefAlu.child(key).remove();
            $ionicLoading.hide();

            var alertPopup = $ionicPopup.alert({
              title: 'Aula Finalizada!',
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Confira os detalhes para obter o boleto gerado no seu e-mail.</p></div></div>",
              cssClass: 'popcustom',
              okType: 'customok'
            });

            $rootScope.lastClassProf = prof;
            $scope.modrtl.show();
            $scope.$on('modal.hidden', function() {
              $state.go('tabsController.aulas');
            });
          }
        }
        else{
          var dis = snapshot.val().Disciplina;
          var icon;
          dbRefDisciplinas.on('child_added', function(snep) {
            if(dis === snep.val().Nome && novacat === snep.val().Categoria){
              icon = snep.val().Icon;
              dbRefD.push({"Nome": dis, "Aulas" : "1", "Categoria" : novacat, "Icon" : icon, "Cor" : "#444bb6"});
            }
          });
          dbRefAlu.child(key).remove();
          $ionicLoading.hide();

          var alertPopup = $ionicPopup.alert({
            title: 'Aula Finalizada!',
            template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Confira os detalhes para obter o boleto gerado no seu e-mail.</p></div></div>",
            cssClass: 'popcustom',
            okType: 'customok'
          });

          $rootScope.lastClassProf = prof;
          $scope.modrtl.show();

          $scope.$on('modal.hidden', function() {
            $state.go('tabsController.aulas');
          });
        }

      });

    });

 };


//----------------------------------------------------------------------------//
  function efetuaCartao(id, card, key, user) {

    $ionicLoading.show({
      template: "<ion-spinner></ion-spinner><br/><p class='custom'>Carregando</p>"
    });

    var authToken = dataService.getAuthToken();
    var cusId = paymentService.getCusId();

    paymentService.createPaymentWithCreditCard(id, card, authToken).then(function(response) {

      var AulasConcluidas;
      var mes;
      var mesint;
      var dia;
      var diaint;
      var aulas;
      var prof;
      var novacat;
      var existe = false;
      var diskey;
      var aulaID;
      var profID;
      var aulasProf;
      var dbRefA = dbRefAlunosPrivate.child(user);
      var dbRefAlu = dbRefAulasAlunos.child(user);
      var dbRefF = dbRefA.child("Financeiro");
      var dbRefD = dbRefA.child("Disciplinas");
      dbRefAlu.child(key).update({"Aluno_Finalizou": "Sim"});
      dbRefAlu.child(key).update({"Pagamento_Efetuado": "Sim"});

      dbRefA.once('value', function(snap) {

        AulasConcluidas = snap.val().AulasConcluidas;
        AulasConcluidas = parseInt(AulasConcluidas, 10);
        AulasConcluidas += 1;
        AulasConcluidas = AulasConcluidas.toString();
        dbRefA.update({"AulasConcluidas": AulasConcluidas});

      });

      dbRefAlu.child(key).once('value', function(snapshot) {

        mes = snapshot.val().Mês;
        mesint = parseInt(mes, 10);
        if(mesint < 10){
          mes = "0" + mes;
        }
        dia = snapshot.val().Dia;
        diaint = parseInt(dia, 10);
        if(diaint < 10){
          dia = "0" + dia;
        }
        dbRefF.push({
          "Ano": snapshot.val().Ano, 
          "Data": dia + '/' + mes + '/' + snapshot.val().Ano, 
          "Dia": snapshot.val().Dia, 
          "Disciplina": snapshot.val().Disciplina, 
          "Hora": snapshot.val().Hora, 
          "Local": snapshot.val().Endereço, 
          "Mês": snapshot.val().Mês, 
          "Professor": snapshot.val().Professor, 
          "Valor": snapshot.val().Valor, 
          "Pagamento" : "Cartão", 
          "Charge_ID" : snapshot.val().Charge_ID, 
          "Pay_ID" : response.data.id,
          "Cidade" : snapshot.val().Local
        });
        novacat = snapshot.val().Categoria;
        novacat = parseCategoria(novacat);
        prof = snapshot.val().Professor;
        aulaID = snapshot.val().Aula_ID;
        profID = snapshot.val().Prof_ID;

        dbRefProfessores.child(profID).once('value', function(snep) {

          aulasProf = snep.val().Aulas;
          aulasProf += 1;
          dbRefProfessores.child(profID).update({"Aulas": aulasProf});

        });

        dbRefAulasProfessores.child(profID).child(aulaID).update({"Aluno_Finalizou": "Sim"});
        dbRefAulasProfessores.child(profID).child(aulaID).update({"Pagamento_Efetuado": "Sim"});

        dbRefD.once('value', function(snop) {

        if(snop.val()){
          snop.forEach(function(child) {
            if(novacat === child.val().Categoria && snapshot.val().Disciplina === child.val().Nome){
              existe = true;
              diskey = child.key;
              aulas = child.val().Aulas;
              aulas = parseInt(aulas, 10);
            }
          });

          if(!existe){
            var dis = snapshot.val().Disciplina;
            var icon;
            dbRefDisciplinas.on('child_added', function(snep) {
              if(dis === snep.val().Nome && novacat === snep.val().Categoria){
                icon = snep.val().Icon;
                dbRefD.push({"Nome": dis, "Aulas" : "1", "Categoria" : novacat, "Icon" : icon, "Cor" : "#444bb6"});
              }
            });
            dbRefAlu.child(key).remove();
            $ionicLoading.hide();

            var alertPopup = $ionicPopup.alert({
              title: 'Aula Finalizada!',
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Aula finalizada com sucesso.</p></div></div>",
              cssClass: 'popcustom',
              okType: 'customok'
            });

            $rootScope.lastClassProf = prof;
            $scope.modrtl.show();

            $scope.$on('modal.hidden', function() {
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go('tabsController.aulas');
            });
          }

          else{
            aulas += 1;
            dbRefD.child(diskey).update({"Aulas": aulas.toString()});
            dbRefAlu.child(key).remove();
            $ionicLoading.hide();

            var alertPopup = $ionicPopup.alert({
              title: 'Aula Finalizada!',
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Aula finalizada com sucesso.</p></div></div>",
              cssClass: 'popcustom',
              okType: 'customok'
            });

            $rootScope.lastClassProf = prof;
            $scope.modrtl.show();
            $scope.$on('modal.hidden', function() {
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go('tabsController.aulas');
            });
          }
        }
        else{
          var dis = snapshot.val().Disciplina;
          var icon;
          dbRefDisciplinas.on('child_added', function(snep) {
            if(dis === snep.val().Nome && novacat === snep.val().Categoria){
              icon = snep.val().Icon;
              dbRefD.push({"Nome": dis, "Aulas" : "1", "Categoria" : novacat, "Icon" : icon, "Cor" : "#444bb6"});
            }
          });
          dbRefAlu.child(key).remove();
          $ionicLoading.hide();

          var alertPopup = $ionicPopup.alert({
            title: 'Aula Finalizada!',
            template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Aula finalizada com sucesso.</p></div></div>",
            cssClass: 'popcustom',
            okType: 'customok'
          });

          $rootScope.lastClassProf = prof;
          $scope.modrtl.show();

          $scope.$on('modal.hidden', function() {
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            $state.go('tabsController.aulas');
          });
        }

      });

    });

  },
  function(error) {

    var alertPopup = $ionicPopup.alert({
      title: 'Erro',
      template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Ocorreu um erro ao finalizar sua aula. Por favor, entre em contato conosco caso o problema persista.</p></div></div>",
      cssClass: 'popcustom',
      okType: 'customok'
    });

  });

};


//----------------------------------------------------------------------------//
$scope.cancelarMarcada = function() {

  var user = agendamentoService.getAlunoKey();
  var aulakey = agendamentoService.getMarcadaKey();
  var aula_index = agendamentoService.getCancelarIndex();
  var authToken = dataService.getAuthToken();

  if (user && aulakey) {

    var dbRefA = dbRefAulasAlunos.child(user).child(aulakey);
    var professor;
    var dbRefProf;
    var dbRefPAC;
    var dia;
    var mes;
    var ano;
    var profID;
    var chargeID;
    var aula = "";
    var aulaID;
    var canceladas;
    var profPush;
    var NotificationAluno_ID;
    var NotificationProfessor_ID;
    var now = new Date();
    var horacut;
    var indexcut;
    var pagamento;
    var geld;
    var totalProfessor;

    dbRefA.once('value', function(snap) {

      professor = snap.val().Professor;
      profID = snap.val().Prof_ID;
      aulaID = snap.val().Aula_ID;
      chargeID = snap.val().Charge_ID;
      profPush = snap.val().Push_ID;
      NotificationAluno_ID = snap.val().NotificationAluno_ID;
      NotificationProfessor_ID = snap.val().NotificationProfessor_ID;
      aulakey = snap.key;
      dbRefProf = dbRefAulasProfessores.child(profID);
      dia = snap.val().Dia;
      mes = snap.val().Mês;
      ano = snap.val().Ano;
      horacut = snap.val().Hora;
      indexcut = horacut.indexOf(':');
      horacut = horacut.substring(0, indexcut);
      horacut = parseInt(horacut, 10);
      aula = "marcada";
      pagamento = snap.val().Pagamento;

    });


    var myPopup = $ionicPopup.show({
      template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-alert-circled icon-pop'></i></div></div></div><p class='custom'>Você tem certeza que gostaria de cancelar esta aula?</p></div></div>",
      title: "Cancelar",
      cssClass: 'popcustom',
      scope: $scope,
      buttons: [
        { text: 'Fechar', type: 'customconfirm' },
        {
          text: '<b>Sim</b>',
          type: 'customconfirm',
          onTap: function(e) {
            $ionicLoading.show({
              template: "<ion-spinner></ion-spinner><br/><p class='custom'>Cancelando</p>"
            });

            dbRefAulasProfessores.child(profID).child(aulaID).remove();
            dbRefAulasAlunos.child(user).child(aulakey).remove();

            dbRefAlunosPrivate.child(user).once('value', function(snapshot) {
              canceladas = snapshot.val().AulasCanceladas;
              canceladas = parseInt(canceladas, 10);
              canceladas += 1;
              dbRefAlunosPrivate.child(user).update({"AulasCanceladas" : canceladas});
            });

            notificationsService.canceledClassNotification(profPush, "Sua aula do dia " + $scope.dataaluno + " horas foi cancelada.").then(function(response) {
                
              },
              function(error) {
              //ionicToast.show('Erro ao mandar notificação para o professor.', 'middle', true, 2500);
              });

            notificationsService.cancelStudentNotification(NotificationAluno_ID).then(function(res) {
                  
              },
              function(err) {
              //ionicToast.show('Erro ao cancelar notificação do aluno.', 'middle', true, 2500);
              });

            notificationsService.cancelTeacherNotification(NotificationProfessor_ID).then(function(re) {
                
              },
              function(e) {
              //ionicToast.show('Erro ao cancelar notificação do professor.', 'middle', true, 2500);
              });

            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            $state.go('tabsController.aulas');
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Aula Cancelada',
              template: "<div class='row'><div class='col text-center'><div class='div-pop'><i class='icon ion-checkmark-round icon-pop'></i></div></div></div><p class='custom'>Sua aula foi cancelada com sucesso.</p></div></div>",
              cssClass: 'popcustom',
              okType: 'customok'
            });
          }
        }
      ]
    });


  }


};

    

})
