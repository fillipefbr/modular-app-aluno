angular.module('app.calendarAllCtrl', [])

//***********************_ALL TEACHERS CONTROLLER_****************************//

.controller('calendarAllCtrl', function($scope, $rootScope, $document, $ionicListDelegate, $ionicLoading, $timeout, $ionicModal, $state, $ionicHistory, $ionicPlatform, $http, paymentService, categoryService, teacherService, ConnectivityMonitor, ionicToast, agendamentoService, dataService) {

  $scope.data = {};

  const dbRefRoot = firebase.database().ref();
  const dbRefAlunos = dbRefRoot.child('alunos');
  const dbRefAulasAlunos = dbRefRoot.child('aulas_alunos');
  const dbRefAulasProfessores = dbRefRoot.child('aulas_professores');
  const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
  const dbRefProfessoresPrivate = dbRefRoot.child('professores_private');
  const dbRefProfessoresAutorizados = dbRefRoot.child('professores_autorizados');
  const dbRefDisciplinas = dbRefRoot.child('disciplinas');
  const dbRefHorariosProfessores = dbRefRoot.child('horarios_professores');
  const dbRefProfessores = dbRefRoot.child('professores');
  const auth = firebase.auth();

  var professores = [];
  var disciplinas = [];
  var horarios_professores = [];

  $ionicModal.fromTemplateUrl('calendarall/diaTodos.html', {
    scope: $scope
  }).then(function(modtodol) {
    $scope.modtodol = modtodol;
  });

  $scope.$on('$destroy', function() {
    $scope.modtodol.remove();
  });

  var currentDate = new Date();
  var date = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
  $scope.date = date;

  var weekday = new Array(7);
  weekday[0]=  "Domingo";
  weekday[1] = "Segunda-Feira";
  weekday[2] = "Terça-Feira";
  weekday[3] = "Quarta-Feira";
  weekday[4] = "Quinta-Feira";
  weekday[5] = "Sexta-Feira";
  weekday[6] = "Sábado";


  var monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
    "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
  ];


  $scope.onezoneDatepicker4 = {
        date: date,
        mondayFirst: true,
        months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        daysOfTheWeek: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
        startDate: new Date(1989, 1, 26),
        endDate: new Date(2024, 1, 26),
        disablePastDays: true,
        disableSwipe: false,
        disableWeekend: false,
        //disableDates: [new Date(date.getFullYear(), date.getMonth(), 15), new Date(date.getFullYear(), date.getMonth(), 16), new Date(date.getFullYear(), date.getMonth(), 17)],
        showDatepicker: false,
        showTodayButton: true,
        calendarMode: true,
        hideCancelButton: false,
        hideSetButton: false,
        callback: //$scope.myFunction
          function() {

          } ,
        highlights: [

          ]

    };

    $scope.$on("$ionicView.beforeEnter", function(){
      professores = dataService.getProfessores();
      disciplinas = dataService.getDisciplinas();
      horarios_professores = dataService.getHorariosProfessores();
    });

  $scope.$on("$ionicView.enter", function(){

    var userProf;
    var day;
    var month;
    var year;

    var colors = [];

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();

    var e = agendamentoService.getDis();
    var p = categoryService.getCat();
    var l = agendamentoService.getLoc();
    var ntemprof = true;
    var dis;
    var loc;
    var cat;
    var disci = [];
    var locai = [];
    var categ = [];
    var hor_tds = [];
    var localbool = false;
    var disciplinabool = false;

    for(var i = 0; i < professores.length; i++){
       disci = [];
       locai = [];
       categ = [];
       localbool = false;
       disciplinabool = false;
      for(var j = 0; j < horarios_professores.length; j++){
        if(professores[i].$id === horarios_professores[j].$id && professores[i].Autorizado === true){
          dis = professores[i].Disciplinas;
          loc = professores[i].Locais;

          for(var item in dis){
            disci.push(dis[item]);
          }
          for(var item in loc){
            locai.push(loc[item]);
          }

          disci.forEach(function(child) {
            if(e === child.Nome && p === child.Categoria){
              disciplinabool = true;
            }
          });

          locai.forEach(function(child) {
            if(l === child.Local){
              localbool = true;
            }
          });

          if(localbool && disciplinabool){

            for(var item in horarios_professores[j]){
              hor_tds.push(horarios_professores[j][item]);
            }

            hor_tds.forEach(function(child) {

              if(child != null && child.Dia != 'undefined'){
                
                day = child.Dia;
                day = parseInt(day, 10);
                month = child.Mês;
                month = parseInt(month, 10);
                year = child.Ano;
                year = parseInt(year, 10);

                if((day < dd && month <= mm && year <= yyyy) || (month < mm && year <= yyyy)){

                }

                else{
                  var h1 = {

                    date: new Date(year, month - 1, day),
                    color: '#00b300',
                    textColor: '#fff'

                  };

                  //$scope.onezoneDatepicker4.highlights.push(h1);
                  colors.push(h1);
                }
              }

            });
            
          }

        }
      }
    }

    $scope.onezoneDatepicker4.showDatepicker = true; // may be not neccessary
    $scope.onezoneDatepicker4.highlights = colors;

  });

    /*$scope.$on("$ionicView.beforeEnter", function(){
      var userProf;
      var day;
      var month;
      var year;

      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1;
      var yyyy = today.getFullYear();

      var local = $document[0].getElementById('localselect');
      var categoria = categoryService.getCat();
      var disciplina = $document[0].getElementById('divider1');

      $scope.onezoneDatepicker4.highlights = [];
      $scope.onezoneDatepicker4.disableDates = [];

      dbRefProfessores.on('child_added', function(snap) {

        dbRefProfessoresAutorizados.on('value', function(snip) {
          if(snip.val()) {

             var dbRefProf = dbRefProfessores.child(snap.key);
             var dbRefHors = dbRefHorariosProfessores.child(snap.key);
             var dbRefLocs = dbRefProf.child("Locais");
             var dbRefD = dbRefProf.child("Disciplinas");

              dbRefLocs.on('child_added', function(snop) {

                dbRefD.on('child_added', function(snip) {

                  if(local.innerText === snop.val().Local && categoria === snip.val().Categoria && disciplina.innerText === snip.val().Nome){

                    dbRefHors.on('child_added', function(snapshoot) {

                      day = snapshoot.val().Dia;
                      day = parseInt(day, 10);
                      month = snapshoot.val().Mês;
                      month = parseInt(month, 10);
                      year = snapshoot.val().Ano;
                      year = parseInt(year, 10);

                      if((day < dd && month <= mm && year <= yyyy) || (month < mm && year <= yyyy)){

                      }

                      else{
                        var h1 = {

                          date: new Date(year, month - 1, day),
                          color: '#00b300',
                          textColor: '#fff'

                        };

                        $scope.onezoneDatepicker4.highlights.push(h1);
                      }


                    });

                  }

                });

              });
          }
        });

      });

      $scope.onezoneDatepicker4.showDatepicker = true;
    });*/


//----------------------------------------------------------------------------//
$scope.refreshTodosHorarios = function() {

  var userProf;
  var day;
  var month;
  var year;

  var colors = [];

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1;
  var yyyy = today.getFullYear();

  var e = agendamentoService.getDis();
  var p = categoryService.getCat();
  var l = agendamentoService.getLoc();
  var ntemprof = true;
  var dis;
  var loc;
  var cat;
  var disci = [];
  var locai = [];
  var categ = [];
  var hor_tds = [];
  var localbool = false;
  var disciplinabool = false;

  for(var i = 0; i < professores.length; i++){
     disci = [];
     locai = [];
     categ = [];
     localbool = false;
     disciplinabool = false;
    for(var j = 0; j < horarios_professores.length; j++){
      if(professores[i].$id === horarios_professores[j].$id && professores[i].Autorizado === true){
        dis = professores[i].Disciplinas;
        loc = professores[i].Locais;

        for(var item in dis){
          disci.push(dis[item]);
        }
        for(var item in loc){
          locai.push(loc[item]);
        }

        disci.forEach(function(child) {
          if(e === child.Nome && p === child.Categoria){
            disciplinabool = true;
          }
        });

        locai.forEach(function(child) {
          if(l === child.Local){
            localbool = true;
          }
        });

        if(localbool && disciplinabool){

          for(var item in horarios_professores[j]){
            hor_tds.push(horarios_professores[j][item]);
          }

          hor_tds.forEach(function(child) {

            if(child != null && child.Dia != 'undefined'){
              
              day = child.Dia;
              day = parseInt(day, 10);
              month = child.Mês;
              month = parseInt(month, 10);
              year = child.Ano;
              year = parseInt(year, 10);

              if((day < dd && month <= mm && year <= yyyy) || (month < mm && year <= yyyy)){

              }

              else{
                var h1 = {

                  date: new Date(year, month - 1, day),
                  color: '#00b300',
                  textColor: '#fff'

                };

                //$scope.onezoneDatepicker4.highlights.push(h1);
                colors.push(h1);
              }
            }

          });
          
        }

      }
    }
  }

  $scope.onezoneDatepicker4.showDatepicker = true; // may be not neccessary
  $scope.onezoneDatepicker4.highlights = colors;
  $scope.$broadcast('scroll.refreshComplete');

};

//----------------------------------------------------------------------------//
    /*$scope.onezoneDatepicker4.callback = function() {

      $scope.todos = [];

      var cat = categoryService.getCat();
      var dis = $document[0].getElementById('divider1');
      var loc = $document[0].getElementById('localselect');
      var datadiacircle = $document[0].getElementById('datadiacircletodos');
      var dbRefP;
      var dbRefH;
      var dbRefD;
      var dbRefL;
      var cancela = false;
      var mensagem;
      var nome;
      var incluiu = false;
      var temp;
      var tempdia;

      var dia = $scope.onezoneDatepicker4.date.getDate();
      var mes = $scope.onezoneDatepicker4.date.getMonth();

      var diaString = dia.toString();
      var mesString = mes + 1;
      mesString = mesString.toString();

      $scope.diaescolhido = $scope.onezoneDatepicker4.date.getDate();
      $scope.dayescolhido = weekday[$scope.onezoneDatepicker4.date.getDay()];
      $scope.mesescolhido = monthNames[$scope.onezoneDatepicker4.date.getMonth()];
      $scope.mesescolhido4 = $scope.onezoneDatepicker4.date.getMonth() + 1;
      $scope.data.dataEscolhida = $scope.diaescolhido + " de " +  $scope.mesescolhido;

      $scope.data.add = false;
      $scope.data.spintodos = true;

      $scope.modtodol.show();

      $scope.navtitle = $scope.data.dataEscolhida;
      var diaint = parseInt($scope.diaescolhido, 10);
      var mesint = parseInt($scope.mesescolhido4, 10);
      if(diaint < 10){
        $scope.diaescolhido = "0" + $scope.diaescolhido;
      }

      if(mesint < 10){
        $scope.mesescolhido4 = "0" + $scope.mesescolhido4;
        $scope.datadiacircletodos = $scope.diaescolhido + "/" + $scope.mesescolhido4;
      }
      else {
        $scope.datadiacircletodos = $scope.diaescolhido + "/" + $scope.mesescolhido4;
      }
      
      $scope.diacircletodos = $scope.dayescolhido;

         dbRefProfessores.on('child_added', function(snap) {

           dbRefProfessoresAutorizados.child(snap.key).once('value', function(sniip) {
             if(sniip.val()){

               dbRefP = dbRefProfessores.child(snap.key);
               dbRefH = dbRefHorariosProfessores.child(snap.key);
               dbRefD = dbRefP.child("Disciplinas");
               dbRefL = dbRefP.child("Locais");
               var modal = $document[0].getElementById('diaList');

               dbRefD.on('child_added', function(snip) {

                 if(cat === snip.val().Categoria && dis.innerText === snip.val().Nome){

                   dbRefL.on('child_added', function(snup) {

                     if(loc.innerText === snup.val().Local){

                          dbRefH.on('child_added', function(snaap) {

                            if(diaString === snaap.val().Dia && mesString === snaap.val().Mês){

                              var horaInt = parseInt(snaap.val().Hora, 10);
                              var minutosInt = parseInt(snaap.val().Minutos, 10);

                              horaInt++;
                              minutosInt += 40;

                              if(minutosInt >= 60){
                                horaInt++;
                                minutosInt -= 60;
                              }

                              //added
                              if(minutosInt < 10){
                                minutosInt = minutosInt.toString();
                                minutosInt = "0" + minutosInt;
                              }

                              horaInt = horaInt.toString();
                              minutosInt = minutosInt.toString();

                              var phora = snaap.val().Hora + ":" + snaap.val().Minutos;
                              var pminutos = snaap.val().Minutos;
                              temp = snaap.val().Mês;
                              temp = parseInt(temp, 10);
                              if(temp < 10){
                                temp = temp.toString();
                                temp = "0" + temp;
                              }
                              else{
                                temp = temp.toString();
                              }
                              tempdia = snaap.val().Dia;
                              tempdia = parseInt(tempdia, 10);
                              if(tempdia < 10){
                                tempdia = tempdia.toString();
                                tempdia = "0" + tempdia;
                              }
                              else{
                                tempdia = tempdia.toString();
                              }
                              var pdata = tempdia + "/" + temp + " às " + phora;

                              var now = new Date();
                              if(now.getDate().toString() === diaString && mesString === (now.getMonth() + 1).toString() && parseInt(snaap.val().Hora, 10) <= now.getHours()){

                              }
                              else{
                                mensagem = snaap.val().Hora + ":" + snaap.val().Minutos + " às " + horaInt + ":" + minutosInt;
                                nome = snap.val().Nome;
                                var todo = {"hora" : mensagem, "professor" : nome, "key" : snaap.key, "prof" : snap.key, "data" : snaap.val().Dia + '/' + snaap.val().Mês + '/' + snaap.val().Ano, "ano" : snaap.val().Ano, "dia" : snaap.val().Dia, "mes" : snaap.val().Mês, "horares" : snaap.val().Hora, "pdata" : pdata, "phora" : phora, "profpush" : snap.val().Push_ID, "pminutos" : pminutos};
                                $scope.data.spintodos = false;
                                $scope.todos.push(todo);
                                incluiu = true;
                              }


                           }

                         });
                     }

                   });

                 }

               });
             }
           });

        });


       $timeout(function() {

         if($scope.data.add === false && incluiu === false){
           $scope.data.spintodos = false;
           $scope.data.add = true;
         }

       }, 3000);

    };*/

//----------------------------------------------------------------------------//
    $scope.onezoneDatepicker4.callback = function() {

      $scope.todos = [];

      var cat = categoryService.getCat();
      var dis = $document[0].getElementById('divider1');
      var loc = $document[0].getElementById('localselect');
      //var datadiacircle = $document[0].getElementById('datadiacircletodos');
      var dbRefP;
      var dbRefH;
      var dbRefD;
      var dbRefL;
      var cancela = false;
      var mensagem;
      var nome;
      var incluiu = false;
      var temp;
      var tempdia;

      var dia = $scope.onezoneDatepicker4.date.getDate();
      var mes = $scope.onezoneDatepicker4.date.getMonth();

      var diaString = dia.toString();
      var mesString = mes + 1;
      mesString = mesString.toString();

      $scope.diaescolhido = $scope.onezoneDatepicker4.date.getDate();
      $scope.dayescolhido = weekday[$scope.onezoneDatepicker4.date.getDay()];
      $scope.mesescolhido = monthNames[$scope.onezoneDatepicker4.date.getMonth()];
      $scope.mesescolhido4 = $scope.onezoneDatepicker4.date.getMonth() + 1;
      $scope.data.dataEscolhida = $scope.diaescolhido + " de " +  $scope.mesescolhido;

      $scope.data.add = false;
      $scope.data.spintodos = true;

      $scope.navtitle = $scope.data.dataEscolhida;
      var diaint = parseInt($scope.diaescolhido, 10);
      var mesint = parseInt($scope.mesescolhido4, 10);
      if(diaint < 10){
        $scope.diaescolhido = "0" + $scope.diaescolhido;
      }

      if(mesint < 10){
        $scope.mesescolhido4 = "0" + $scope.mesescolhido4;
        $scope.datadiacircletodos = $scope.diaescolhido + "/" + $scope.mesescolhido4;
      }
      else {
        $scope.datadiacircletodos = $scope.diaescolhido + "/" + $scope.mesescolhido4;
      }
      
      $scope.diacircletodos = $scope.dayescolhido;

      $scope.modtodol.show();

         dbRefProfessores.on('child_added', function(snap) {

           if(snap.val().Autorizado === true){
              dbRefP = dbRefProfessores.child(snap.key);
               dbRefH = dbRefHorariosProfessores.child(snap.key);
               dbRefD = dbRefP.child("Disciplinas");
               dbRefL = dbRefP.child("Locais");

               dbRefD.on('child_added', function(snip) {

                 if(cat === snip.val().Categoria && dis.innerText === snip.val().Nome){

                   dbRefL.on('child_added', function(snup) {

                     if(loc.innerText === snup.val().Local){

                          dbRefH.on('child_added', function(snaap) {

                            if(diaString === snaap.val().Dia && mesString === snaap.val().Mês){

                              var horaInt = parseInt(snaap.val().Hora, 10);
                              var minutosInt = parseInt(snaap.val().Minutos, 10);

                              horaInt++;
                              minutosInt += 40;

                              if(minutosInt >= 60){
                                horaInt++;
                                minutosInt -= 60;
                              }

                              //added
                              if(minutosInt < 10){
                                minutosInt = minutosInt.toString();
                                minutosInt = "0" + minutosInt;
                              }
                              
                              horaInt = horaInt.toString();
                              minutosInt = minutosInt.toString();

                              var phora = snaap.val().Hora + ":" + snaap.val().Minutos;
                              var pminutos = snaap.val().Minutos;
                              temp = snaap.val().Mês;
                              temp = parseInt(temp, 10);
                              if(temp < 10){
                                temp = temp.toString();
                                temp = "0" + temp;
                              }
                              else{
                                temp = temp.toString();
                              }
                              tempdia = snaap.val().Dia;
                              tempdia = parseInt(tempdia, 10);
                              if(tempdia < 10){
                                tempdia = tempdia.toString();
                                tempdia = "0" + tempdia;
                              }
                              else{
                                tempdia = tempdia.toString();
                              }
                              var pdata = tempdia + "/" + temp + " às " + phora;

                              var now = new Date();
                              if(now.getDate().toString() === diaString && mesString === (now.getMonth() + 1).toString() && parseInt(snaap.val().Hora, 10) <= now.getHours()){

                              }
                              else{
                                mensagem = snaap.val().Hora + ":" + snaap.val().Minutos + " às " + horaInt + ":" + minutosInt;
                                nome = snap.val().Nome;
                                var todo = {"hora" : mensagem, "professor" : nome, "key" : snaap.key, "prof" : snap.key, "data" : snaap.val().Dia + '/' + snaap.val().Mês + '/' + snaap.val().Ano, "ano" : snaap.val().Ano, "dia" : snaap.val().Dia, "mes" : snaap.val().Mês, "horares" : snaap.val().Hora, "pdata" : pdata, "phora" : phora, "profpush" : snap.val().Push_ID, "pminutos" : pminutos};
                                $scope.data.spintodos = false;
                                $scope.todos.push(todo);
                                incluiu = true;
                              }


                           }

                         });
                     }

                   });

                 }

               });
           }

        });


       $timeout(function() {

         if($scope.data.add === false && incluiu === false){
           $scope.data.spintodos = false;
           $scope.data.add = true;
         }

       }, 3000);

    };

//----------------------------------------------------------------------------//
    $scope.escolheuTodos = function(prof, key, data, hora, professor, ano, mes, dia, horares, pdata, phora, profpush, pminutos) {

      $rootScope.starfull1 = false;
      $rootScope.starfull2 = false;
      $rootScope.starfull3 = false;
      $rootScope.starfull4 = false;
      $rootScope.starfull5 = false;
      $rootScope.staroutline1 = false;
      $rootScope.starhalf1 = false;
      $rootScope.starrating = "";

      /*agendamentoService.setProf(nome);
      agendamentoService.setKey(key);
      $scope.modtodol.hide();
      $state.go('tabsController.resumo');*/

      // ADDED

      if(typeof $rootScope.cdPromocional != 'undefined'){
        var desconto = agendamentoService.getPcd();
        //agendamentoService.reSetPcd();
      }

      var adresstxt = $document[0].getElementById('adresstxt');
      var locale = $document[0].getElementById('localselect');
      var conteudo = $document[0].getElementById('inputOBS');
      var user = agendamentoService.getAlunoKey();
      var nomeAluno = agendamentoService.getAlunoNome();

      if(typeof desconto != 'undefined' && typeof $rootScope.cdPromocional != 'undefined'){
        var res = {"disciplina" : agendamentoService.getDis(), "key" : key, "prof" : prof, "data" : data, "hora" : hora, "professor" : professor, "categoria" : categoryService.getCurrentCat(), "alunos" : agendamentoService.getAlunos(), "ano" : ano, "mes" : mes, "dia" : dia, "horares" : horares, "pdata" : pdata, "phora" : phora, "desconto" : desconto, "cpkey" : $rootScope.cdPromocional, "profpush" : profpush, "endereco" : adresstxt.innerText, "local" : locale.innerText, "conteudo" : conteudo.value, "user" : user, "cliente" : nomeAluno, "pminutos" : pminutos};
      }
      else{
        var res = {"disciplina" : agendamentoService.getDis(), "key" : key, "prof" : prof, "data" : data, "hora" : hora, "professor" : professor, "categoria" : categoryService.getCurrentCat(), "alunos" : agendamentoService.getAlunos(), "ano" : ano, "mes" : mes, "dia" : dia, "horares" : horares, "pdata" : pdata, "phora" : phora, "profpush" : profpush, "endereco" : adresstxt.innerText, "local" : locale.innerText, "conteudo" : conteudo.value, "user" : user, "cliente" : nomeAluno, "pminutos" : pminutos};
      }
      agendamentoService.setArrHors(res);
      $scope.modtodol.hide();
      $state.go('tabsController.resumo');

      

    };

})
