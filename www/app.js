
angular.module('app', ['ionic', 'app.routes', 'app.mainCtrl', 'app.disciplinasCtrl', 'app.tourCtrl', 'app.calendarCtrl', 'app.calendarAllCtrl', 'app.teachersCtrl', 'app.cardsCtrl', 'onezone-datepicker', 'ngCordova', 'ionic-ratings', 'ionic-toast', 'app.categoryService', 'app.ConnectivityMonitor', 'app.notificationsService', 'app.paymentService', 'app.teacherService', 'app.favoritosCtrl', 'app.calendariofavoritosCtrl', 'app.favoritesService', 'app.addfavoritosCtrl', 'app.registerCtrl', 'app.loginCtrl', 'ionMDRipple', 'app.codesCtrl', 'app.addressCtrl', 'app.agendamentoCtrl', 'app.agendamentoService', 'ti-segmented-control', 'ionic.ion.imageCacheFactory', 'app.resumoCtrl', 'app.pacotesCtrl', 'app.agefavCtrl', 'app.resumoFavCtrl', 'app.escolaCtrl', 'app.boletoFavCtrl', 'app.pacotesService', 'app.dataService', 'firebase', 'app.cadastroService', 'app.historicoCtrl', 'app.financeiroCtrl', 'app.ajustesCtrl', 'app.aulasCtrl', 'app.transferCtrl', 'app.addressMineCtrl', 'app.boletoCtrl', 'app.codigoPromocionalCtrl', 'app.marcadasCtrl', 'app.calendarioPessoalCtrl', 'app.categoriasCtrl', 'app.transferFavCtrl'])


.config(function($ionicConfigProvider){

  $ionicConfigProvider.backButton.previousTitleText(false).text('');

})

.run(function($ionicPlatform, $state, categoryService, ConnectivityMonitor, $rootScope, $ImageCacheFactory, $cordovaDevice, favoritesService, $timeout, $cordovaAppVersion, dataService, agendamentoService, $http, paymentService) {
  $ionicPlatform.ready(function() {

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }

    window.plugins.OneSignal.getIds(function(ids) {
      $rootScope.pushID = ids.userId;
    });

    $ImageCacheFactory.Cache(["img/degrade_modular.jpg", "img/fundoapp_3.jpg", "img/fundo_2.jpg", "img/fundocalendar.jpg",
    "img/modular-home.jpg", "img/person-outline.png", "img/Modular_logo_branca.png", "img/modular_M.png"]).then(function(){
    }, function(error) {

    });

    ConnectivityMonitor.startWatching();

    var notificationOpenedCallback = function(jsonData) {

    };

    // Set your iOS Settings
    var iosSettings = {};
    iosSettings["kOSSettingsKeyAutoPrompt"] = true;
    iosSettings["kOSSettingsKeyInAppLaunchURL"] = false;

   window.plugins.OneSignal
     .startInit("c0b2933e-7141-492f-9a00-aa6b2a290b9a")
     .iOSSettings(iosSettings)
     .handleNotificationOpened(notificationOpenedCallback)
     .endInit();

   if(ConnectivityMonitor.isOnline()){
     const dbRefRoot = firebase.database().ref();
     const dbRefAlunos = dbRefRoot.child('alunos');
     const dbRefAlunosPrivate = dbRefRoot.child('alunos_private');
     const dbRefProfessores = dbRefRoot.child('professores');
     const dbRefContato = dbRefRoot.child('contato_modular');
     const dbRefApps = dbRefRoot.child('apps');

     /*dbRefRoot.child("categorias").child("CV").once('value', function(snap) {
       $rootScope.cursosnome = snap.val().Nome;
     });*/

     /*$cordovaAppVersion.getVersionNumber().then(function (version) {
       $rootScope.appVersion = version;
       var plat = $cordovaDevice.getPlatform();
       dbRefApps.child(plat).once('value', function(snap){
         if(version != snap.val()){
           window.plugins.OneSignal.sendTag("atualizados", "false");
           window.plugins.OneSignal.sendTag("desatualizados", "true");
         }
         else{
           window.plugins.OneSignal.sendTag("atualizados", "true");
           window.plugins.OneSignal.sendTag("desatualizados", "false");
         }
       });
     });*/

     var imgs = [];
     dbRefProfessores.once('value').then(function(snapshot) {
       var x = snapshot.val();
       for(var i in x){
         if(x[i].Avatar && x[i].Autorizado === true){
           imgs.push(x[i].Avatar);
         }
       }
       $ImageCacheFactory.Cache(imgs);
     });

     dbRefContato.once('value').then(function(snapshot) {
       var x = snapshot.val();
       var y = x.c1.Número;
       var z = x.c2.Número;
       var w = x.c1.Nome;
       var s = x.c2.Nome;
       $rootScope.modular1nome = w;
       $rootScope.modular2nome = s;
       $rootScope.modular1numero = y;
       $rootScope.modular2numero = z;
     });

     var user = firebase.auth().currentUser;
     if (user) {

      dataService.setProfessores();
      dataService.loadHorariosProfessores();

      firebase.auth().currentUser.getToken(/* forceRefresh */ true).then(function(idToken) {
        dataService.setAuthToken(idToken);
      }).catch(function(error) {
        
      });

       /*dbRefAlunosPrivate.child(user.uid).once('value', function(snap) {
         if(snap.val().Favoritos){
           favoritesService.setHasFavTrue();
         }
       });*/

       agendamentoService.setAlunoKey(user.uid);
       agendamentoService.setAlunoNome(user.displayName);

       var now = new Date();
       var dia = now.getDate();
       var mes = now.getMonth() + 1;
       var ano = now.getFullYear();
       var hora = now.getHours();
       var minutos = now.getMinutes();
       if(dia < 10) {
           dia = '0' + dia;
       }
       if(mes < 10) {
           mes = '0' + mes;
       }
       var lastLogin = dia + '/' + mes + '/' + ano + '-' + hora + ':' + minutos;
       dbRefAlunosPrivate.child(user.uid).update({"Login" : lastLogin});
       /*var model = $cordovaDevice.getModel();
       var platform = $cordovaDevice.getPlatform();
       var version = $cordovaDevice.getVersion();
       dbRefAlunosPrivate.child(user.uid).update({"Dispositivo" : model});
       dbRefAlunosPrivate.child(user.uid).update({"OS" : platform});
       dbRefAlunosPrivate.child(user.uid).update({"OS_Version" : version});*/
       if (user.emailVerified) {
         dbRefAlunosPrivate.child(user.uid).update({"Email_Verificado" : true});
       }
       var dbRefAlu = dbRefAlunosPrivate.child(user.uid);
       dbRefAlu.once('value', function(snap) {
         if(snap.val().customerId) {
            paymentService.setCusId(snap.val().customerId);
         }
         categoryService.setCat(snap.val().Categoria);
         $rootScope.alunoaulasid = snap.val().Nome;
         $state.go('tabsController.aulas');
       });
     } else {
       $state.go('inicio');
     }

   }
   else {
     $state.go('loginAluno');
   }

   // Call syncHashedEmail anywhere in your app if you have the user's email.
   // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
   // window.plugins.OneSignal.syncHashedEmail(userEmail);
  });
})

.filter('unique', function() {

   return function(collection, keyname) {

      var output = [],
          keys = [];

      angular.forEach(collection, function(item) {

          var key = item[keyname];

          if(keys.indexOf(key) === -1) {

              keys.push(key);

              output.push(item);
          }
      });

      return output;
   };
})
